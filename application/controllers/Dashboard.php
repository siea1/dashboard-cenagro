<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
  {
		parent::__construct();
		$this->load->model('dashboard_model');
		$this->load->helper(array('url', 'form'));
    $this->load->database();
  }

  public function index()
	{
		$this->load->view('header_view');
		$this->load->view('navbar_view');
		$this->load->view('dashboard_filter_view');
	}

	public function reporte($num, $filter = NULL)
	{
		$dep = $this->input->post('departamento');
		if (is_null($dep)) {
			$data['num'] = $num;
			$data['tit_rep'] = $this->getTituloReporte($num);
			$ls['data'] = null;
			$this->load->view('header_view');
			$this->load->view('navbar_view');
			$this->load->view('filter_view', $data);
			$this->load->view('footer_view', $ls);
		} else {
			$ubigeo = $this->input->post('distrito');
			$d = $this->getReport($num, $ubigeo);
			$d['num'] = $num;
			$ls['data'] = json_encode($d);
			$this->load->view('header_view');
			$this->load->view('navbar_view');
			$this->load->view('reporte'.$num.'_view', $d);
			$this->load->view('footer_view', $ls);
			
		}
	}

	public function diagrams($n, $u)
	{
		$d = $this->getReport($n, $u);
		echo json_encode($d);
	}

	public function getReport($n, $u)
	{
		$dep = substr($u, 0, 2);
		$prov = substr($u, 2, 2);
		$dist = substr($u, 4, 2);
		$def;

		switch ($n) {
			case 1:
				$data = $this->dashboard_model->getReport1($dep, $prov, $dist);
				$def = json_decode(JSON_R1_DEF, true);
				break;
			case 2:
				$data = $this->dashboard_model->getReport2($dep, $prov, $dist);
				$def = json_decode(JSON_R2_DEF, true);
				break;
			case 3:
				$data = $this->dashboard_model->getReport3($dep, $prov, $dist);
				$def = json_decode(JSON_R3_DEF, true);
				break;
			case 4:
				$data = $this->dashboard_model->getReport4($dep, $prov, $dist);
				$def = json_decode(JSON_R4_DEF, true);
				break;
			case 5:
				$data = $this->dashboard_model->getReport5($dep, $prov, $dist);
				$def = json_decode(JSON_R5_DEF, true);
				break;
			case 6:
				$data = $this->dashboard_model->getReport6($dep, $prov, $dist);
				$def = json_decode(JSON_R6_DEF, true);
				break;
			case 7:
				$data = $this->dashboard_model->getReport7($dep, $prov, $dist);
				$def = json_decode(JSON_R7_DEF, true);
				break;
			case 8:
				$data = $this->dashboard_model->getReport8($dep, $prov, $dist);
				$def = json_decode(JSON_R8_DEF, true);
				break;
			default:
				return NULL;
				break;
		}
		
		$d['m0_5'] = $this->getObjectRow($data, 'M', '0-5', $def);
		$d['f0_5'] = $this->getObjectRow($data, 'F', '0-5', $def);
		$d['m51_10'] = $this->getObjectRow($data, 'M', '51-10', $def);
		$d['f51_10'] = $this->getObjectRow($data, 'F', '51-10', $def);
		$d['m101_20'] = $this->getObjectRow($data, 'M', '101-20', $def);
		$d['f101_20'] = $this->getObjectRow($data, 'F', '101-20', $def);
		$d['m201_50'] = $this->getObjectRow($data, 'M', '201-50', $def);
		$d['f201_50'] = $this->getObjectRow($data, 'F', '201-50', $def);
		$d['m501_100'] = $this->getObjectRow($data, 'M', '501-100', $def);
		$d['f501_100'] = $this->getObjectRow($data, 'F', '501-100', $def);
		$d['m100'] = $this->getObjectRow($data, 'M', '100', $def);
		$d['f100'] = $this->getObjectRow($data, 'F', '100', $def);
		return $d;
	}

	public function provincias($dep)
	{
		$data = $this->dashboard_model->getProvincias($dep);
		echo json_encode($data);
	}

	public function distritos($dep, $prov)
	{
		$data = $this->dashboard_model->getDistritos($dep, $prov);
		echo json_encode($data);
	}

	public function getObjectRow($res, $sex, $hec, $def)
	{
		foreach ($res as $r) {
			if ($r['SEXO'] == $sex and $r['HECTAREA'] == $hec) {
				return $r;
			} 
		}
		return $def;
	}

	public function getTituloReporte($num) {
		switch ($num) {
			case 1:
				return 'Uso de la tierra';
				break;
			case 2:
				return 'Tenencia de la tierra';
				break;
			case 3:
				return 'Asociatividad';
				break;
			case 4:
				return 'Crédito';
				break;
			case 5:
				return 'Capacidades';
				break;
			case 6:
				return 'Prácticas pecuarias';
				break;
			case 7:
				return 'Riego';
				break;
			case 8:
				return 'Hogar del productor';
				break;
			default:
				# code...
				break;
		}
	}
}
