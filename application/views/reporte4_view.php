
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Crédito</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('Credito')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="pieChartR4"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="doughnutChartR4"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th colspan="13" class="table-info"> 
                            <center>Gestionó el crédito</center>
                          </th>
                          <th colspan="13" class="table-warning"> 
                            <center>Tipo de Beneficio o Servicios que les brindan</center>
                          </th>
                          <th colspan="5" class="table-primary"> 
                            <center>Para qué pidió el prestamo</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Obtuvo Créd.
                          </td>
                          <td>
                            No obt./Por falta Gar.
                          </td>
                          <td>
                            No obt./Incum. Pago Créd. Ant.
                          </td>
                          <td>
                            No obt./No tener Titulo Prop.
                          </td>
                          <td>
                            No obt./Otro
                          </td>
                          <td>
                            No Ges./No Necesita
                          </td>
                          <td>
                            No Ges./Tram. Eng.
                          </td>
                          <td>
                            No Ges./Int. Elevados
                          </td>
                          <td>
                            No Ges./No hay hab.
                          </td>
                          <td>
                            No Ges./Falta Gar.
                          </td>
                          <td>
                            No Ges./Por deudas pend.
                          </td>
                          <td>
                            No Ges./Cree que no le darian
                          </td>
                          <td>
                            No Ges./Otro
                          </td>
                          <td>
                            Comerciante
                          </td>
                          <td>
                            Habilitadora
                          </td>
                          <td>
                            Agrobanco
                          </td>
                          <td>
                            Bco. Múltiple
                          </td>
                          <td>
                            CMAC
                          </td>
                          <td>
                            CAC
                          </td>
                          <td>
                            CRAC
                          </td>
                          <td>
                            Molino/Desm.
                          </td>
                          <td>
                            ONG
                          </td>
                          <td>
                            Emp. Textil
                          </td>
                          <td>
                            Prestamista
                          </td>
                          <td>
                            Edpyme
                          </td>
                          <td>
                            Otro
                          </td>
                          <td>
                            Adq. Ins. Prod.
                          </td>
                          <td>
                            Compra Maq.
                          </td>
                          <td>
                            Compra Herr.
                          </td>
                          <td>
                            Comerc. Prod.
                          </td>
                          <td>
                            Otro
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SOC'] + $f0_5['GES_SOC']; ?></p>
                            <?php echo $m0_5['GES_SOC_S'] + $f0_5['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN1'] + $f0_5['GES_SN1']; ?></p>
                            <?php echo $m0_5['GES_SN1_S'] + $f0_5['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN2'] + $f0_5['GES_SN2']; ?></p>
                            <?php echo $m0_5['GES_SN2_S'] + $f0_5['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN3'] + $f0_5['GES_SN3']; ?></p>
                            <?php echo $m0_5['GES_SN3_S'] + $f0_5['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN4'] + $f0_5['GES_SN4']; ?></p>
                            <?php echo $m0_5['GES_SN4_S'] + $f0_5['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG1'] + $f0_5['GES_NG1']; ?></p>
                            <?php echo $m0_5['GES_NG1_S'] + $f0_5['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG2'] + $f0_5['GES_NG2']; ?></p>
                            <?php echo $m0_5['GES_NG2_S'] + $f0_5['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG3'] + $f0_5['GES_NG3']; ?></p>
                            <?php echo $m0_5['GES_NG3_S'] + $f0_5['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG4'] + $f0_5['GES_NG4']; ?></p>
                            <?php echo $m0_5['GES_NG4_S'] + $f0_5['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG5'] + $f0_5['GES_NG5']; ?></p>
                            <?php echo $m0_5['GES_NG5_S'] + $f0_5['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG6'] + $f0_5['GES_NG6']; ?></p>
                            <?php echo $m0_5['GES_NG6_S'] + $f0_5['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG7'] + $f0_5['GES_NG7']; ?></p>
                            <?php echo $m0_5['GES_NG7_S'] + $f0_5['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG8'] + $f0_5['GES_NG7']; ?></p>
                            <?php echo $m0_5['GES_NG8_S'] + $f0_5['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_1'] + $f0_5['INS_1']; ?></p>
                            <?php echo $m0_5['INS_1_S'] + $f0_5['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_2'] + $f0_5['INS_2']; ?></p>
                            <?php echo $m0_5['INS_2_S'] + $f0_5['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_3'] + $f0_5['INS_3']; ?></p>
                            <?php echo $m0_5['INS_3_S'] + $f0_5['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_4'] + $f0_5['INS_4']; ?></p>
                            <?php echo $m0_5['INS_4_S'] + $f0_5['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_5'] + $f0_5['INS_5']; ?></p>
                            <?php echo $m0_5['INS_5_S'] + $f0_5['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_6'] + $f0_5['INS_6']; ?></p>
                            <?php echo $m0_5['INS_6_S'] + $f0_5['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_7'] + $f0_5['INS_7']; ?></p>
                            <?php echo $m0_5['INS_7_S'] + $f0_5['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_8'] + $f0_5['INS_8']; ?></p>
                            <?php echo $m0_5['INS_8_S'] + $f0_5['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_9'] + $f0_5['INS_9']; ?></p>
                            <?php echo $m0_5['INS_9_S'] + $f0_5['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_10'] + $f0_5['INS_10']; ?></p>
                            <?php echo $m0_5['INS_10_S'] + $f0_5['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_11'] + $f0_5['INS_11']; ?></p>
                            <?php echo $m0_5['INS_11_S'] + $f0_5['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_12'] + $f0_5['INS_12']; ?></p>
                            <?php echo $m0_5['INS_12_S'] + $f0_5['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_13'] + $f0_5['INS_13']; ?></p>
                            <?php echo $m0_5['INS_13_S'] + $f0_5['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_1'] + $f0_5['PRES_1']; ?></p>
                            <?php echo $m0_5['PRES_1_S'] + $f0_5['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_2'] + $f0_5['PRES_2']; ?></p>
                            <?php echo $m0_5['PRES_2_S'] + $f0_5['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_3'] + $f0_5['PRES_3']; ?></p>
                            <?php echo $m0_5['PRES_3_S'] + $f0_5['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_4'] + $f0_5['PRES_4']; ?></p>
                            <?php echo $m0_5['PRES_4_S'] + $f0_5['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_5'] + $f0_5['PRES_5']; ?></p>
                            <?php echo $m0_5['PRES_5_S'] + $f0_5['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SOC'] + $f51_10['GES_SOC']; ?></p>
                            <?php echo $m51_10['GES_SOC_S'] + $f51_10['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN1'] + $f51_10['GES_SN1']; ?></p>
                            <?php echo $m51_10['GES_SN1_S'] + $f51_10['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN2'] + $f51_10['GES_SN2']; ?></p>
                            <?php echo $m51_10['GES_SN2_S'] + $f51_10['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN3'] + $f51_10['GES_SN3']; ?></p>
                            <?php echo $m51_10['GES_SN3_S'] + $f51_10['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN4'] + $f51_10['GES_SN4']; ?></p>
                            <?php echo $m51_10['GES_SN4_S'] + $f51_10['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG1'] + $f51_10['GES_NG1']; ?></p>
                            <?php echo $m51_10['GES_NG1_S'] + $f51_10['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG2'] + $f51_10['GES_NG2']; ?></p>
                            <?php echo $m51_10['GES_NG2_S'] + $f51_10['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG3'] + $f51_10['GES_NG3']; ?></p>
                            <?php echo $m51_10['GES_NG3_S'] + $f51_10['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG4'] + $f51_10['GES_NG4']; ?></p>
                            <?php echo $m51_10['GES_NG4_S'] + $f51_10['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG5'] + $f51_10['GES_NG5']; ?></p>
                            <?php echo $m51_10['GES_NG5_S'] + $f51_10['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG6'] + $f51_10['GES_NG6']; ?></p>
                            <?php echo $m51_10['GES_NG6_S'] + $f51_10['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG7'] + $f51_10['GES_NG7']; ?></p>
                            <?php echo $m51_10['GES_NG7_S'] + $f51_10['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG8'] + $f51_10['GES_NG7']; ?></p>
                            <?php echo $m51_10['GES_NG8_S'] + $f51_10['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_1'] + $f51_10['INS_1']; ?></p>
                            <?php echo $m51_10['INS_1_S'] + $f51_10['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_2'] + $f51_10['INS_2']; ?></p>
                            <?php echo $m51_10['INS_2_S'] + $f51_10['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_3'] + $f51_10['INS_3']; ?></p>
                            <?php echo $m51_10['INS_3_S'] + $f51_10['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_4'] + $f51_10['INS_4']; ?></p>
                            <?php echo $m51_10['INS_4_S'] + $f51_10['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_5'] + $f51_10['INS_5']; ?></p>
                            <?php echo $m51_10['INS_5_S'] + $f51_10['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_6'] + $f51_10['INS_6']; ?></p>
                            <?php echo $m51_10['INS_6_S'] + $f51_10['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_7'] + $f51_10['INS_7']; ?></p>
                            <?php echo $m51_10['INS_7_S'] + $f51_10['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_8'] + $f51_10['INS_8']; ?></p>
                            <?php echo $m51_10['INS_8_S'] + $f51_10['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_9'] + $f51_10['INS_9']; ?></p>
                            <?php echo $m51_10['INS_9_S'] + $f51_10['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_10'] + $f51_10['INS_10']; ?></p>
                            <?php echo $m51_10['INS_10_S'] + $f51_10['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_11'] + $f51_10['INS_11']; ?></p>
                            <?php echo $m51_10['INS_11_S'] + $f51_10['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_12'] + $f51_10['INS_12']; ?></p>
                            <?php echo $m51_10['INS_12_S'] + $f51_10['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_13'] + $f51_10['INS_13']; ?></p>
                            <?php echo $m51_10['INS_13_S'] + $f51_10['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_1'] + $f51_10['PRES_1']; ?></p>
                            <?php echo $m51_10['PRES_1_S'] + $f51_10['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_2'] + $f51_10['PRES_2']; ?></p>
                            <?php echo $m51_10['PRES_2_S'] + $f51_10['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_3'] + $f51_10['PRES_3']; ?></p>
                            <?php echo $m51_10['PRES_3_S'] + $f51_10['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_4'] + $f51_10['PRES_4']; ?></p>
                            <?php echo $m51_10['PRES_4_S'] + $f51_10['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_5'] + $f51_10['PRES_5']; ?></p>
                            <?php echo $m51_10['PRES_5_S'] + $f51_10['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SOC'] + $f101_20['GES_SOC']; ?></p>
                            <?php echo $m101_20['GES_SOC_S'] + $f101_20['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN1'] + $f101_20['GES_SN1']; ?></p>
                            <?php echo $m101_20['GES_SN1_S'] + $f101_20['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN2'] + $f101_20['GES_SN2']; ?></p>
                            <?php echo $m101_20['GES_SN2_S'] + $f101_20['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN3'] + $f101_20['GES_SN3']; ?></p>
                            <?php echo $m101_20['GES_SN3_S'] + $f101_20['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN4'] + $f101_20['GES_SN4']; ?></p>
                            <?php echo $m101_20['GES_SN4_S'] + $f101_20['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG1'] + $f101_20['GES_NG1']; ?></p>
                            <?php echo $m101_20['GES_NG1_S'] + $f101_20['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG2'] + $f101_20['GES_NG2']; ?></p>
                            <?php echo $m101_20['GES_NG2_S'] + $f101_20['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG3'] + $f101_20['GES_NG3']; ?></p>
                            <?php echo $m101_20['GES_NG3_S'] + $f101_20['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG4'] + $f101_20['GES_NG4']; ?></p>
                            <?php echo $m101_20['GES_NG4_S'] + $f101_20['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG5'] + $f101_20['GES_NG5']; ?></p>
                            <?php echo $m101_20['GES_NG5_S'] + $f101_20['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG6'] + $f101_20['GES_NG6']; ?></p>
                            <?php echo $m101_20['GES_NG6_S'] + $f101_20['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG7'] + $f101_20['GES_NG7']; ?></p>
                            <?php echo $m101_20['GES_NG7_S'] + $f101_20['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG8'] + $f101_20['GES_NG7']; ?></p>
                            <?php echo $m101_20['GES_NG8_S'] + $f101_20['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_1'] + $f101_20['INS_1']; ?></p>
                            <?php echo $m101_20['INS_1_S'] + $f101_20['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_2'] + $f101_20['INS_2']; ?></p>
                            <?php echo $m101_20['INS_2_S'] + $f101_20['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_3'] + $f101_20['INS_3']; ?></p>
                            <?php echo $m101_20['INS_3_S'] + $f101_20['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_4'] + $f101_20['INS_4']; ?></p>
                            <?php echo $m101_20['INS_4_S'] + $f101_20['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_5'] + $f101_20['INS_5']; ?></p>
                            <?php echo $m101_20['INS_5_S'] + $f101_20['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_6'] + $f101_20['INS_6']; ?></p>
                            <?php echo $m101_20['INS_6_S'] + $f101_20['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_7'] + $f101_20['INS_7']; ?></p>
                            <?php echo $m101_20['INS_7_S'] + $f101_20['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_8'] + $f101_20['INS_8']; ?></p>
                            <?php echo $m101_20['INS_8_S'] + $f101_20['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_9'] + $f101_20['INS_9']; ?></p>
                            <?php echo $m101_20['INS_9_S'] + $f101_20['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_10'] + $f101_20['INS_10']; ?></p>
                            <?php echo $m101_20['INS_10_S'] + $f101_20['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_11'] + $f101_20['INS_11']; ?></p>
                            <?php echo $m101_20['INS_11_S'] + $f101_20['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_12'] + $f101_20['INS_12']; ?></p>
                            <?php echo $m101_20['INS_12_S'] + $f101_20['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_13'] + $f101_20['INS_13']; ?></p>
                            <?php echo $m101_20['INS_13_S'] + $f101_20['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_1'] + $f101_20['PRES_1']; ?></p>
                            <?php echo $m101_20['PRES_1_S'] + $f101_20['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_2'] + $f101_20['PRES_2']; ?></p>
                            <?php echo $m101_20['PRES_2_S'] + $f101_20['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_3'] + $f101_20['PRES_3']; ?></p>
                            <?php echo $m101_20['PRES_3_S'] + $f101_20['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_4'] + $f101_20['PRES_4']; ?></p>
                            <?php echo $m101_20['PRES_4_S'] + $f101_20['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_5'] + $f101_20['PRES_5']; ?></p>
                            <?php echo $m101_20['PRES_5_S'] + $f101_20['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SOC'] + $f201_50['GES_SOC']; ?></p>
                            <?php echo $m201_50['GES_SOC_S'] + $f201_50['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN1'] + $f201_50['GES_SN1']; ?></p>
                            <?php echo $m201_50['GES_SN1_S'] + $f201_50['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN2'] + $f201_50['GES_SN2']; ?></p>
                            <?php echo $m201_50['GES_SN2_S'] + $f201_50['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN3'] + $f201_50['GES_SN3']; ?></p>
                            <?php echo $m201_50['GES_SN3_S'] + $f201_50['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN4'] + $f201_50['GES_SN4']; ?></p>
                            <?php echo $m201_50['GES_SN4_S'] + $f201_50['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG1'] + $f201_50['GES_NG1']; ?></p>
                            <?php echo $m201_50['GES_NG1_S'] + $f201_50['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG2'] + $f201_50['GES_NG2']; ?></p>
                            <?php echo $m201_50['GES_NG2_S'] + $f201_50['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG3'] + $f201_50['GES_NG3']; ?></p>
                            <?php echo $m201_50['GES_NG3_S'] + $f201_50['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG4'] + $f201_50['GES_NG4']; ?></p>
                            <?php echo $m201_50['GES_NG4_S'] + $f201_50['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG5'] + $f201_50['GES_NG5']; ?></p>
                            <?php echo $m201_50['GES_NG5_S'] + $f201_50['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG6'] + $f201_50['GES_NG6']; ?></p>
                            <?php echo $m201_50['GES_NG6_S'] + $f201_50['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG7'] + $f201_50['GES_NG7']; ?></p>
                            <?php echo $m201_50['GES_NG7_S'] + $f201_50['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG8'] + $f201_50['GES_NG7']; ?></p>
                            <?php echo $m201_50['GES_NG8_S'] + $f201_50['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_1'] + $f201_50['INS_1']; ?></p>
                            <?php echo $m201_50['INS_1_S'] + $f201_50['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_2'] + $f201_50['INS_2']; ?></p>
                            <?php echo $m201_50['INS_2_S'] + $f201_50['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_3'] + $f201_50['INS_3']; ?></p>
                            <?php echo $m201_50['INS_3_S'] + $f201_50['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_4'] + $f201_50['INS_4']; ?></p>
                            <?php echo $m201_50['INS_4_S'] + $f201_50['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_5'] + $f201_50['INS_5']; ?></p>
                            <?php echo $m201_50['INS_5_S'] + $f201_50['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_6'] + $f201_50['INS_6']; ?></p>
                            <?php echo $m201_50['INS_6_S'] + $f201_50['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_7'] + $f201_50['INS_7']; ?></p>
                            <?php echo $m201_50['INS_7_S'] + $f201_50['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_8'] + $f201_50['INS_8']; ?></p>
                            <?php echo $m201_50['INS_8_S'] + $f201_50['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_9'] + $f201_50['INS_9']; ?></p>
                            <?php echo $m201_50['INS_9_S'] + $f201_50['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_10'] + $f201_50['INS_10']; ?></p>
                            <?php echo $m201_50['INS_10_S'] + $f201_50['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_11'] + $f201_50['INS_11']; ?></p>
                            <?php echo $m201_50['INS_11_S'] + $f201_50['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_12'] + $f201_50['INS_12']; ?></p>
                            <?php echo $m201_50['INS_12_S'] + $f201_50['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_13'] + $f201_50['INS_13']; ?></p>
                            <?php echo $m201_50['INS_13_S'] + $f201_50['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_1'] + $f201_50['PRES_1']; ?></p>
                            <?php echo $m201_50['PRES_1_S'] + $f201_50['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_2'] + $f201_50['PRES_2']; ?></p>
                            <?php echo $m201_50['PRES_2_S'] + $f201_50['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_3'] + $f201_50['PRES_3']; ?></p>
                            <?php echo $m201_50['PRES_3_S'] + $f201_50['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_4'] + $f201_50['PRES_4']; ?></p>
                            <?php echo $m201_50['PRES_4_S'] + $f201_50['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_5'] + $f201_50['PRES_5']; ?></p>
                            <?php echo $m201_50['PRES_5_S'] + $f201_50['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SOC'] + $f501_100['GES_SOC']; ?></p>
                            <?php echo $m501_100['GES_SOC_S'] + $f501_100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN1'] + $f501_100['GES_SN1']; ?></p>
                            <?php echo $m501_100['GES_SN1_S'] + $f501_100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN2'] + $f501_100['GES_SN2']; ?></p>
                            <?php echo $m501_100['GES_SN2_S'] + $f501_100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN3'] + $f501_100['GES_SN3']; ?></p>
                            <?php echo $m501_100['GES_SN3_S'] + $f501_100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN4'] + $f501_100['GES_SN4']; ?></p>
                            <?php echo $m501_100['GES_SN4_S'] + $f501_100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG1'] + $f501_100['GES_NG1']; ?></p>
                            <?php echo $m501_100['GES_NG1_S'] + $f501_100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG2'] + $f501_100['GES_NG2']; ?></p>
                            <?php echo $m501_100['GES_NG2_S'] + $f501_100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG3'] + $f501_100['GES_NG3']; ?></p>
                            <?php echo $m501_100['GES_NG3_S'] + $f501_100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG4'] + $f501_100['GES_NG4']; ?></p>
                            <?php echo $m501_100['GES_NG4_S'] + $f501_100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG5'] + $f501_100['GES_NG5']; ?></p>
                            <?php echo $m501_100['GES_NG5_S'] + $f501_100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG6'] + $f501_100['GES_NG6']; ?></p>
                            <?php echo $m501_100['GES_NG6_S'] + $f501_100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG7'] + $f501_100['GES_NG7']; ?></p>
                            <?php echo $m501_100['GES_NG7_S'] + $f501_100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG8'] + $f501_100['GES_NG7']; ?></p>
                            <?php echo $m501_100['GES_NG8_S'] + $f501_100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_1'] + $f501_100['INS_1']; ?></p>
                            <?php echo $m501_100['INS_1_S'] + $f501_100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_2'] + $f501_100['INS_2']; ?></p>
                            <?php echo $m501_100['INS_2_S'] + $f501_100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_3'] + $f501_100['INS_3']; ?></p>
                            <?php echo $m501_100['INS_3_S'] + $f501_100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_4'] + $f501_100['INS_4']; ?></p>
                            <?php echo $m501_100['INS_4_S'] + $f501_100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_5'] + $f501_100['INS_5']; ?></p>
                            <?php echo $m501_100['INS_5_S'] + $f501_100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_6'] + $f501_100['INS_6']; ?></p>
                            <?php echo $m501_100['INS_6_S'] + $f501_100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_7'] + $f501_100['INS_7']; ?></p>
                            <?php echo $m501_100['INS_7_S'] + $f501_100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_8'] + $f501_100['INS_8']; ?></p>
                            <?php echo $m501_100['INS_8_S'] + $f501_100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_9'] + $f501_100['INS_9']; ?></p>
                            <?php echo $m501_100['INS_9_S'] + $f501_100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_10'] + $f501_100['INS_10']; ?></p>
                            <?php echo $m501_100['INS_10_S'] + $f501_100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_11'] + $f501_100['INS_11']; ?></p>
                            <?php echo $m501_100['INS_11_S'] + $f501_100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_12'] + $f501_100['INS_12']; ?></p>
                            <?php echo $m501_100['INS_12_S'] + $f501_100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_13'] + $f501_100['INS_13']; ?></p>
                            <?php echo $m501_100['INS_13_S'] + $f501_100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_1'] + $f501_100['PRES_1']; ?></p>
                            <?php echo $m501_100['PRES_1_S'] + $f501_100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_2'] + $f501_100['PRES_2']; ?></p>
                            <?php echo $m501_100['PRES_2_S'] + $f501_100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_3'] + $f501_100['PRES_3']; ?></p>
                            <?php echo $m501_100['PRES_3_S'] + $f501_100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_4'] + $f501_100['PRES_4']; ?></p>
                            <?php echo $m501_100['PRES_4_S'] + $f501_100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_5'] + $f501_100['PRES_5']; ?></p>
                            <?php echo $m501_100['PRES_5_S'] + $f501_100['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SOC'] + $f100['GES_SOC']; ?></p>
                            <?php echo $m100['GES_SOC_S'] + $f100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN1'] + $f100['GES_SN1']; ?></p>
                            <?php echo $m100['GES_SN1_S'] + $f100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN2'] + $f100['GES_SN2']; ?></p>
                            <?php echo $m100['GES_SN2_S'] + $f100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN3'] + $f100['GES_SN3']; ?></p>
                            <?php echo $m100['GES_SN3_S'] + $f100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN4'] + $f100['GES_SN4']; ?></p>
                            <?php echo $m100['GES_SN4_S'] + $f100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG1'] + $f100['GES_NG1']; ?></p>
                            <?php echo $m100['GES_NG1_S'] + $f100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG2'] + $f100['GES_NG2']; ?></p>
                            <?php echo $m100['GES_NG2_S'] + $f100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG3'] + $f100['GES_NG3']; ?></p>
                            <?php echo $m100['GES_NG3_S'] + $f100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG4'] + $f100['GES_NG4']; ?></p>
                            <?php echo $m100['GES_NG4_S'] + $f100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG5'] + $f100['GES_NG5']; ?></p>
                            <?php echo $m100['GES_NG5_S'] + $f100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG6'] + $f100['GES_NG6']; ?></p>
                            <?php echo $m100['GES_NG6_S'] + $f100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG7'] + $f100['GES_NG7']; ?></p>
                            <?php echo $m100['GES_NG7_S'] + $f100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG8'] + $f100['GES_NG7']; ?></p>
                            <?php echo $m100['GES_NG8_S'] + $f100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_1'] + $f100['INS_1']; ?></p>
                            <?php echo $m100['INS_1_S'] + $f100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_2'] + $f100['INS_2']; ?></p>
                            <?php echo $m100['INS_2_S'] + $f100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_3'] + $f100['INS_3']; ?></p>
                            <?php echo $m100['INS_3_S'] + $f100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_4'] + $f100['INS_4']; ?></p>
                            <?php echo $m100['INS_4_S'] + $f100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_5'] + $f100['INS_5']; ?></p>
                            <?php echo $m100['INS_5_S'] + $f100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_6'] + $f100['INS_6']; ?></p>
                            <?php echo $m100['INS_6_S'] + $f100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_7'] + $f100['INS_7']; ?></p>
                            <?php echo $m100['INS_7_S'] + $f100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_8'] + $f100['INS_8']; ?></p>
                            <?php echo $m100['INS_8_S'] + $f100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_9'] + $f100['INS_9']; ?></p>
                            <?php echo $m100['INS_9_S'] + $f100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_10'] + $f100['INS_10']; ?></p>
                            <?php echo $m100['INS_10_S'] + $f100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_11'] + $f100['INS_11']; ?></p>
                            <?php echo $m100['INS_11_S'] + $f100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_12'] + $f100['INS_12']; ?></p>
                            <?php echo $m100['INS_12_S'] + $f100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_13'] + $f100['INS_13']; ?></p>
                            <?php echo $m100['INS_13_S'] + $f100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_1'] + $f100['PRES_1']; ?></p>
                            <?php echo $m100['PRES_1_S'] + $f100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_2'] + $f100['PRES_2']; ?></p>
                            <?php echo $m100['PRES_2_S'] + $f100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_3'] + $f100['PRES_3']; ?></p>
                            <?php echo $m100['PRES_3_S'] + $f100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_4'] + $f100['PRES_4']; ?></p>
                            <?php echo $m100['PRES_4_S'] + $f100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_5'] + $f100['PRES_5']; ?></p>
                            <?php echo $m100['PRES_5_S'] + $f100['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SOC']; ?></p>
                            <?php echo $m0_5['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN1']; ?></p>
                            <?php echo $m0_5['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN2']; ?></p>
                            <?php echo $m0_5['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN3']; ?></p>
                            <?php echo $m0_5['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_SN4']; ?></p>
                            <?php echo $m0_5['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG1']; ?></p>
                            <?php echo $m0_5['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG2']; ?></p>
                            <?php echo $m0_5['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG3']; ?></p>
                            <?php echo $m0_5['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG4']; ?></p>
                            <?php echo $m0_5['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG5']; ?></p>
                            <?php echo $m0_5['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG6']; ?></p>
                            <?php echo $m0_5['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG7']; ?></p>
                            <?php echo $m0_5['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GES_NG8']; ?></p>
                            <?php echo $m0_5['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_1']; ?></p>
                            <?php echo $m0_5['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_2']; ?></p>
                            <?php echo $m0_5['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_3']; ?></p>
                            <?php echo $m0_5['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_4']; ?></p>
                            <?php echo $m0_5['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_5']; ?></p>
                            <?php echo $m0_5['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_6']; ?></p>
                            <?php echo $m0_5['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_7']; ?></p>
                            <?php echo $m0_5['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_8']; ?></p>
                            <?php echo $m0_5['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_9']; ?></p>
                            <?php echo $m0_5['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_10']; ?></p>
                            <?php echo $m0_5['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_11']; ?></p>
                            <?php echo $m0_5['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_12']; ?></p>
                            <?php echo $m0_5['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['INS_13']; ?></p>
                            <?php echo $m0_5['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_1']; ?></p>
                            <?php echo $m0_5['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_2']; ?></p>
                            <?php echo $m0_5['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_3']; ?></p>
                            <?php echo $m0_5['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_4']; ?></p>
                            <?php echo $m0_5['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PRES_5']; ?></p>
                            <?php echo $m0_5['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SOC']; ?></p>
                            <?php echo $m51_10['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN1']; ?></p>
                            <?php echo $m51_10['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN2']; ?></p>
                            <?php echo $m51_10['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN3']; ?></p>
                            <?php echo $m51_10['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_SN4']; ?></p>
                            <?php echo $m51_10['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG1']; ?></p>
                            <?php echo $m51_10['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG2']; ?></p>
                            <?php echo $m51_10['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG3']; ?></p>
                            <?php echo $m51_10['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG4']; ?></p>
                            <?php echo $m51_10['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG5']; ?></p>
                            <?php echo $m51_10['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG6']; ?></p>
                            <?php echo $m51_10['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG7']; ?></p>
                            <?php echo $m51_10['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GES_NG8']; ?></p>
                            <?php echo $m51_10['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_1']; ?></p>
                            <?php echo $m51_10['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_2']; ?></p>
                            <?php echo $m51_10['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_3']; ?></p>
                            <?php echo $m51_10['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_4']; ?></p>
                            <?php echo $m51_10['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_5']; ?></p>
                            <?php echo $m51_10['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_6']; ?></p>
                            <?php echo $m51_10['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_7']; ?></p>
                            <?php echo $m51_10['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_8']; ?></p>
                            <?php echo $m51_10['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_9']; ?></p>
                            <?php echo $m51_10['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_10']; ?></p>
                            <?php echo $m51_10['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_11']; ?></p>
                            <?php echo $m51_10['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_12']; ?></p>
                            <?php echo $m51_10['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['INS_13']; ?></p>
                            <?php echo $m51_10['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_1']; ?></p>
                            <?php echo $m51_10['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_2']; ?></p>
                            <?php echo $m51_10['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_3']; ?></p>
                            <?php echo $m51_10['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_4']; ?></p>
                            <?php echo $m51_10['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PRES_5']; ?></p>
                            <?php echo $m51_10['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SOC']; ?></p>
                            <?php echo $m101_20['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN1']; ?></p>
                            <?php echo $m101_20['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN2']; ?></p>
                            <?php echo $m101_20['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN3']; ?></p>
                            <?php echo $m101_20['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_SN4']; ?></p>
                            <?php echo $m101_20['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG1']; ?></p>
                            <?php echo $m101_20['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG2']; ?></p>
                            <?php echo $m101_20['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG3']; ?></p>
                            <?php echo $m101_20['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG4']; ?></p>
                            <?php echo $m101_20['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG5']; ?></p>
                            <?php echo $m101_20['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG6']; ?></p>
                            <?php echo $m101_20['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG7']; ?></p>
                            <?php echo $m101_20['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GES_NG8']; ?></p>
                            <?php echo $m101_20['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_1']; ?></p>
                            <?php echo $m101_20['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_2']; ?></p>
                            <?php echo $m101_20['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_3']; ?></p>
                            <?php echo $m101_20['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_4']; ?></p>
                            <?php echo $m101_20['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_5']; ?></p>
                            <?php echo $m101_20['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_6']; ?></p>
                            <?php echo $m101_20['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_7']; ?></p>
                            <?php echo $m101_20['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_8']; ?></p>
                            <?php echo $m101_20['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_9']; ?></p>
                            <?php echo $m101_20['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_10']; ?></p>
                            <?php echo $m101_20['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_11']; ?></p>
                            <?php echo $m101_20['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_12']; ?></p>
                            <?php echo $m101_20['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['INS_13']; ?></p>
                            <?php echo $m101_20['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_1']; ?></p>
                            <?php echo $m101_20['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_2']; ?></p>
                            <?php echo $m101_20['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_3']; ?></p>
                            <?php echo $m101_20['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_4']; ?></p>
                            <?php echo $m101_20['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PRES_5']; ?></p>
                            <?php echo $m101_20['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SOC']; ?></p>
                            <?php echo $m201_50['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN1']; ?></p>
                            <?php echo $m201_50['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN2']; ?></p>
                            <?php echo $m201_50['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN3']; ?></p>
                            <?php echo $m201_50['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_SN4']; ?></p>
                            <?php echo $m201_50['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG1']; ?></p>
                            <?php echo $m201_50['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG2']; ?></p>
                            <?php echo $m201_50['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG3']; ?></p>
                            <?php echo $m201_50['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG4']; ?></p>
                            <?php echo $m201_50['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG5']; ?></p>
                            <?php echo $m201_50['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG6']; ?></p>
                            <?php echo $m201_50['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG7']; ?></p>
                            <?php echo $m201_50['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GES_NG8']; ?></p>
                            <?php echo $m201_50['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_1']; ?></p>
                            <?php echo $m201_50['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_2']; ?></p>
                            <?php echo $m201_50['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_3']; ?></p>
                            <?php echo $m201_50['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_4']; ?></p>
                            <?php echo $m201_50['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_5']; ?></p>
                            <?php echo $m201_50['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_6']; ?></p>
                            <?php echo $m201_50['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_7']; ?></p>
                            <?php echo $m201_50['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_8']; ?></p>
                            <?php echo $m201_50['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_9']; ?></p>
                            <?php echo $m201_50['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_10']; ?></p>
                            <?php echo $m201_50['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_11']; ?></p>
                            <?php echo $m201_50['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_12']; ?></p>
                            <?php echo $m201_50['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['INS_13']; ?></p>
                            <?php echo $m201_50['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_1']; ?></p>
                            <?php echo $m201_50['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_2']; ?></p>
                            <?php echo $m201_50['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_3']; ?></p>
                            <?php echo $m201_50['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_4']; ?></p>
                            <?php echo $m201_50['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PRES_5']; ?></p>
                            <?php echo $m201_50['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SOC']; ?></p>
                            <?php echo $m501_100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN1']; ?></p>
                            <?php echo $m501_100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN2']; ?></p>
                            <?php echo $m501_100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN3']; ?></p>
                            <?php echo $m501_100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_SN4']; ?></p>
                            <?php echo $m501_100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG1']; ?></p>
                            <?php echo $m501_100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG2']; ?></p>
                            <?php echo $m501_100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG3']; ?></p>
                            <?php echo $m501_100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG4']; ?></p>
                            <?php echo $m501_100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG5']; ?></p>
                            <?php echo $m501_100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG6']; ?></p>
                            <?php echo $m501_100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG7']; ?></p>
                            <?php echo $m501_100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GES_NG8']; ?></p>
                            <?php echo $m501_100['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_1']; ?></p>
                            <?php echo $m501_100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_2']; ?></p>
                            <?php echo $m501_100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_3']; ?></p>
                            <?php echo $m501_100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_4']; ?></p>
                            <?php echo $m501_100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_5']; ?></p>
                            <?php echo $m501_100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_6']; ?></p>
                            <?php echo $m501_100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_7']; ?></p>
                            <?php echo $m501_100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_8']; ?></p>
                            <?php echo $m501_100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_9']; ?></p>
                            <?php echo $m501_100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_10']; ?></p>
                            <?php echo $m501_100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_11']; ?></p>
                            <?php echo $m501_100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_12']; ?></p>
                            <?php echo $m501_100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['INS_13']; ?></p>
                            <?php echo $m501_100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_1']; ?></p>
                            <?php echo $m501_100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_2']; ?></p>
                            <?php echo $m501_100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_3']; ?></p>
                            <?php echo $m501_100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_4']; ?></p>
                            <?php echo $m501_100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PRES_5']; ?></p>
                            <?php echo $m501_100['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SOC']; ?></p>
                            <?php echo $m100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN1']; ?></p>
                            <?php echo $m100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN2']; ?></p>
                            <?php echo $m100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN3']; ?></p>
                            <?php echo $m100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_SN4']; ?></p>
                            <?php echo $m100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG1']; ?></p>
                            <?php echo $m100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG2']; ?></p>
                            <?php echo $m100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG3']; ?></p>
                            <?php echo $m100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG4']; ?></p>
                            <?php echo $m100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG5']; ?></p>
                            <?php echo $m100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG6']; ?></p>
                            <?php echo $m100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG7']; ?></p>
                            <?php echo $m100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GES_NG8']; ?></p>
                            <?php echo $m100['GES_NG8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_1']; ?></p>
                            <?php echo $m100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_2']; ?></p>
                            <?php echo $m100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_3']; ?></p>
                            <?php echo $m100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_4']; ?></p>
                            <?php echo $m100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_5']; ?></p>
                            <?php echo $m100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_6']; ?></p>
                            <?php echo $m100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_7']; ?></p>
                            <?php echo $m100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_8']; ?></p>
                            <?php echo $m100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_9']; ?></p>
                            <?php echo $m100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_10']; ?></p>
                            <?php echo $m100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_11']; ?></p>
                            <?php echo $m100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_12']; ?></p>
                            <?php echo $m100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['INS_13']; ?></p>
                            <?php echo $m100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_1']; ?></p>
                            <?php echo $m100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_2']; ?></p>
                            <?php echo $m100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_3']; ?></p>
                            <?php echo $m100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_4']; ?></p>
                            <?php echo $m100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PRES_5']; ?></p>
                            <?php echo $m100['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo  $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_SOC']; ?></p>
                            <?php echo $f0_5['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_SN1']; ?></p>
                            <?php echo $f0_5['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_SN2']; ?></p>
                            <?php echo $f0_5['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_SN3']; ?></p>
                            <?php echo $f0_5['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_SN4']; ?></p>
                            <?php echo $f0_5['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG1']; ?></p>
                            <?php echo $f0_5['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG2']; ?></p>
                            <?php echo $f0_5['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG3']; ?></p>
                            <?php echo $f0_5['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG4']; ?></p>
                            <?php echo $f0_5['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG5']; ?></p>
                            <?php echo $f0_5['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG6']; ?></p>
                            <?php echo $f0_5['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG7']; ?></p>
                            <?php echo $f0_5['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GES_NG7']; ?></p>
                            <?php echo $f0_5['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_1']; ?></p>
                            <?php echo $f0_5['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_2']; ?></p>
                            <?php echo $f0_5['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_3']; ?></p>
                            <?php echo $f0_5['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_4']; ?></p>
                            <?php echo $f0_5['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_5']; ?></p>
                            <?php echo $f0_5['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_6']; ?></p>
                            <?php echo $f0_5['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_7']; ?></p>
                            <?php echo $f0_5['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_8']; ?></p>
                            <?php echo $f0_5['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_9']; ?></p>
                            <?php echo $f0_5['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_10']; ?></p>
                            <?php echo $f0_5['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_11']; ?></p>
                            <?php echo $f0_5['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_12']; ?></p>
                            <?php echo $f0_5['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['INS_13']; ?></p>
                            <?php echo $f0_5['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PRES_1']; ?></p>
                            <?php echo $f0_5['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PRES_2']; ?></p>
                            <?php echo $f0_5['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PRES_3']; ?></p>
                            <?php echo $f0_5['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PRES_4']; ?></p>
                            <?php echo $f0_5['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PRES_5']; ?></p>
                            <?php echo $f0_5['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_SOC']; ?></p>
                            <?php echo $f51_10['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_SN1']; ?></p>
                            <?php echo $f51_10['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_SN2']; ?></p>
                            <?php echo $f51_10['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_SN3']; ?></p>
                            <?php echo $f51_10['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_SN4']; ?></p>
                            <?php echo $f51_10['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG1']; ?></p>
                            <?php echo $f51_10['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG2']; ?></p>
                            <?php echo $f51_10['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG3']; ?></p>
                            <?php echo $f51_10['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG4']; ?></p>
                            <?php echo $f51_10['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG5']; ?></p>
                            <?php echo $f51_10['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG6']; ?></p>
                            <?php echo $f51_10['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG7']; ?></p>
                            <?php echo $f51_10['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GES_NG7']; ?></p>
                            <?php echo $f51_10['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_1']; ?></p>
                            <?php echo $f51_10['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_2']; ?></p>
                            <?php echo $f51_10['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_3']; ?></p>
                            <?php echo $f51_10['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_4']; ?></p>
                            <?php echo $f51_10['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_5']; ?></p>
                            <?php echo $f51_10['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_6']; ?></p>
                            <?php echo $f51_10['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_7']; ?></p>
                            <?php echo $f51_10['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_8']; ?></p>
                            <?php echo $f51_10['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_9']; ?></p>
                            <?php echo $f51_10['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_10']; ?></p>
                            <?php echo $f51_10['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_11']; ?></p>
                            <?php echo $f51_10['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_12']; ?></p>
                            <?php echo $f51_10['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['INS_13']; ?></p>
                            <?php echo $f51_10['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PRES_1']; ?></p>
                            <?php echo $f51_10['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PRES_2']; ?></p>
                            <?php echo $f51_10['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PRES_3']; ?></p>
                            <?php echo $f51_10['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PRES_4']; ?></p>
                            <?php echo $f51_10['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PRES_5']; ?></p>
                            <?php echo $f51_10['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_SOC']; ?></p>
                            <?php echo $f101_20['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_SN1']; ?></p>
                            <?php echo $f101_20['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_SN2']; ?></p>
                            <?php echo $f101_20['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_SN3']; ?></p>
                            <?php echo $f101_20['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_SN4']; ?></p>
                            <?php echo $f101_20['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG1']; ?></p>
                            <?php echo $f101_20['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG2']; ?></p>
                            <?php echo $f101_20['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG3']; ?></p>
                            <?php echo $f101_20['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG4']; ?></p>
                            <?php echo $f101_20['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG5']; ?></p>
                            <?php echo $f101_20['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG6']; ?></p>
                            <?php echo $f101_20['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG7']; ?></p>
                            <?php echo $f101_20['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GES_NG7']; ?></p>
                            <?php echo $f101_20['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_1']; ?></p>
                            <?php echo $f101_20['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_2']; ?></p>
                            <?php echo $f101_20['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_3']; ?></p>
                            <?php echo $f101_20['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_4']; ?></p>
                            <?php echo $f101_20['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_5']; ?></p>
                            <?php echo $f101_20['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_6']; ?></p>
                            <?php echo $f101_20['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_7']; ?></p>
                            <?php echo $f101_20['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_8']; ?></p>
                            <?php echo $f101_20['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_9']; ?></p>
                            <?php echo $f101_20['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_10']; ?></p>
                            <?php echo $f101_20['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_11']; ?></p>
                            <?php echo $f101_20['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_12']; ?></p>
                            <?php echo $f101_20['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['INS_13']; ?></p>
                            <?php echo $f101_20['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PRES_1']; ?></p>
                            <?php echo $f101_20['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PRES_2']; ?></p>
                            <?php echo $f101_20['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PRES_3']; ?></p>
                            <?php echo $f101_20['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PRES_4']; ?></p>
                            <?php echo $f101_20['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PRES_5']; ?></p>
                            <?php echo $f101_20['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_SOC']; ?></p>
                            <?php echo $f201_50['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_SN1']; ?></p>
                            <?php echo $f201_50['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_SN2']; ?></p>
                            <?php echo $f201_50['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_SN3']; ?></p>
                            <?php echo $f201_50['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_SN4']; ?></p>
                            <?php echo $f201_50['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG1']; ?></p>
                            <?php echo $f201_50['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG2']; ?></p>
                            <?php echo $f201_50['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG3']; ?></p>
                            <?php echo $f201_50['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG4']; ?></p>
                            <?php echo $f201_50['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG5']; ?></p>
                            <?php echo $f201_50['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG6']; ?></p>
                            <?php echo $f201_50['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG7']; ?></p>
                            <?php echo $f201_50['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GES_NG7']; ?></p>
                            <?php echo $f201_50['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_1']; ?></p>
                            <?php echo $f201_50['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_2']; ?></p>
                            <?php echo $f201_50['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_3']; ?></p>
                            <?php echo $f201_50['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_4']; ?></p>
                            <?php echo $f201_50['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_5']; ?></p>
                            <?php echo $f201_50['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_6']; ?></p>
                            <?php echo $f201_50['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_7']; ?></p>
                            <?php echo $f201_50['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_8']; ?></p>
                            <?php echo $f201_50['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_9']; ?></p>
                            <?php echo $f201_50['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_10']; ?></p>
                            <?php echo $f201_50['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_11']; ?></p>
                            <?php echo $f201_50['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_12']; ?></p>
                            <?php echo $f201_50['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['INS_13']; ?></p>
                            <?php echo $f201_50['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PRES_1']; ?></p>
                            <?php echo $f201_50['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PRES_2']; ?></p>
                            <?php echo $f201_50['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PRES_3']; ?></p>
                            <?php echo $f201_50['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PRES_4']; ?></p>
                            <?php echo $f201_50['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PRES_5']; ?></p>
                            <?php echo $f201_50['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_SOC']; ?></p>
                            <?php echo $f501_100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_SN1']; ?></p>
                            <?php echo $f501_100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_SN2']; ?></p>
                            <?php echo $f501_100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_SN3']; ?></p>
                            <?php echo $f501_100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_SN4']; ?></p>
                            <?php echo $f501_100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG1']; ?></p>
                            <?php echo $f501_100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG2']; ?></p>
                            <?php echo $f501_100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG3']; ?></p>
                            <?php echo $f501_100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG4']; ?></p>
                            <?php echo $f501_100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG5']; ?></p>
                            <?php echo $f501_100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG6']; ?></p>
                            <?php echo $f501_100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG7']; ?></p>
                            <?php echo $f501_100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GES_NG7']; ?></p>
                            <?php echo $f501_100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_1']; ?></p>
                            <?php echo $f501_100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_2']; ?></p>
                            <?php echo $f501_100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_3']; ?></p>
                            <?php echo $f501_100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_4']; ?></p>
                            <?php echo $f501_100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_5']; ?></p>
                            <?php echo $f501_100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_6']; ?></p>
                            <?php echo $f501_100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_7']; ?></p>
                            <?php echo $f501_100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_8']; ?></p>
                            <?php echo $f501_100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_9']; ?></p>
                            <?php echo $f501_100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_10']; ?></p>
                            <?php echo $f501_100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_11']; ?></p>
                            <?php echo $f501_100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_12']; ?></p>
                            <?php echo $f501_100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['INS_13']; ?></p>
                            <?php echo $f501_100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PRES_1']; ?></p>
                            <?php echo $f501_100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PRES_2']; ?></p>
                            <?php echo $f501_100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PRES_3']; ?></p>
                            <?php echo $f501_100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PRES_4']; ?></p>
                            <?php echo $f501_100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PRES_5']; ?></p>
                            <?php echo $f501_100['PRES_5_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_SOC']; ?></p>
                            <?php echo $f100['GES_SOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_SN1']; ?></p>
                            <?php echo $f100['GES_SN1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_SN2']; ?></p>
                            <?php echo $f100['GES_SN2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_SN3']; ?></p>
                            <?php echo $f100['GES_SN3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_SN4']; ?></p>
                            <?php echo $f100['GES_SN4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG1']; ?></p>
                            <?php echo $f100['GES_NG1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG2']; ?></p>
                            <?php echo $f100['GES_NG2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG3']; ?></p>
                            <?php echo $f100['GES_NG3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG4']; ?></p>
                            <?php echo $f100['GES_NG4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG5']; ?></p>
                            <?php echo $f100['GES_NG5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG6']; ?></p>
                            <?php echo $f100['GES_NG6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG7']; ?></p>
                            <?php echo $f100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GES_NG7']; ?></p>
                            <?php echo $f100['GES_NG7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_1']; ?></p>
                            <?php echo $f100['INS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_2']; ?></p>
                            <?php echo $f100['INS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_3']; ?></p>
                            <?php echo $f100['INS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_4']; ?></p>
                            <?php echo $f100['INS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_5']; ?></p>
                            <?php echo $f100['INS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_6']; ?></p>
                            <?php echo $f100['INS_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_7']; ?></p>
                            <?php echo $f100['INS_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_8']; ?></p>
                            <?php echo $f100['INS_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_9']; ?></p>
                            <?php echo $f100['INS_9_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_10']; ?></p>
                            <?php echo $f100['INS_10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_11']; ?></p>
                            <?php echo $f100['INS_11_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_12']; ?></p>
                            <?php echo $f100['INS_12_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['INS_13']; ?></p>
                            <?php echo $f100['INS_13_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PRES_1']; ?></p>
                            <?php echo $f100['PRES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PRES_2']; ?></p>
                            <?php echo $f100['PRES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PRES_3']; ?></p>
                            <?php echo $f100['PRES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PRES_4']; ?></p>
                            <?php echo $f100['PRES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PRES_5']; ?></p>
                            <?php echo $f100['PRES_5_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       