
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Tenencia de la tierra</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('TenenciaTierra')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Régimen tenencia de parcela</h4>
                          <canvas id="pieChartR2"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Formas de adquisición de las parcelas</h4>
                          <canvas id="doughnutChartR2"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th colspan="9" class="table-info"> 
                            <center>Régimen de Tenencia de las Parcelas</center>
                          </th>
                          <th colspan="5" class="table-warning"> 
                            <center>Formas de Adquisición de las Parcelas</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Total
                          </td>
                          <td>
                            C/Tít. Reg.
                          </td>
                          <td>
                            C/Tít. no Reg.
                          </td>
                          <td>
                            S/Tít. en Tram.
                          </td>
                          <td>
                            S/Tít. ni en Tram.
                          </td>
                          <td>
                            Comunero
                          </td>
                          <td>
                            Arrendatario
                          </td>
                          <td>
                            Posesionario
                          </td>
                          <td>
                            Otro
                          </td>
                          <td>
                            Total
                          </td>
                          <td>
                            Herencia/Suc.
                          </td>
                          <td>
                            Compra/Venta
                          </td>
                          <td>
                            Adjudic.
                          </td>
                          <td>
                            Otra
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTR'] + $f0_5['RT_CTR'] + $m0_5['RT_CTNR'] + $f0_5['RT_CTNR'] + $m0_5['RT_STET'] + $f0_5['RT_STET'] + $m0_5['RT_STNET'] + $f0_5['RT_STNET'] ; ?></p>
                            <?php echo $m0_5['RT_CTR_S'] + $f0_5['RT_CTR_S'] + $m0_5['RT_CTNR_S'] + $f0_5['RT_CTNR_S'] + $m0_5['RT_STET_S'] + $f0_5['RT_STET_S'] + $m0_5['RT_STNET_S'] + $f0_5['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTR'] + $f0_5['RT_CTR']; ?></p>
                            <?php echo $m0_5['RT_CTR_S'] + $f0_5['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTNR'] + $f0_5['RT_CTNR']; ?></p>
                            <?php echo $m0_5['RT_CTNR_S'] + $f0_5['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_STET'] + $f0_5['RT_STET']; ?></p>
                            <?php echo $m0_5['RT_STET_S'] + $f0_5['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_STNET'] + $f0_5['RT_STNET']; ?></p>
                            <?php echo $m0_5['RT_STNET_S'] + $f0_5['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_COM'] + $f0_5['RT_COM']; ?></p>
                            <?php echo $m0_5['RT_COM_S'] + $f0_5['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_ARR'] + $f0_5['RT_ARR']; ?></p>
                            <?php echo $m0_5['RT_ARR_S'] + $f0_5['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_POS'] + $f0_5['RT_POS']; ?></p>
                            <?php echo $m0_5['RT_POS_S'] + $f0_5['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_OTR'] + $f0_5['RT_OTR']; ?></p>
                            <?php echo $m0_5['RT_OTR_S'] + $f0_5['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_HER'] + $f0_5['FA_HER'] + $m0_5['FA_CV'] + $f0_5['FA_CV'] + $m0_5['FA_ADJ'] + $f0_5['FA_ADJ'] + $m0_5['FA_OTRA'] + $f0_5['FA_OTRA'] ; ?></p>
                            <?php echo $m0_5['FA_HER_S'] + $f0_5['FA_HER_S'] + $m0_5['FA_CV_S'] + $f0_5['FA_CV_S'] + $m0_5['FA_ADJ_S'] + $f0_5['FA_ADJ_S'] + $m0_5['FA_OTRA_S'] + $f0_5['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_HER'] + $f0_5['FA_HER']; ?></p>
                            <?php echo $m0_5['FA_HER_S'] + $f0_5['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_CV'] + $f0_5['FA_CV']; ?></p>
                            <?php echo $m0_5['FA_CV_S'] + $f0_5['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_ADJ'] + $f0_5['FA_ADJ']; ?></p>
                            <?php echo $m0_5['FA_ADJ_S'] + $f0_5['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_OTRA'] + $f0_5['FA_OTRA']; ?></p>
                            <?php echo $m0_5['FA_OTRA_S'] + $f0_5['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTR'] + $f51_10['RT_CTR'] + $m51_10['RT_CTNR'] + $f51_10['RT_CTNR'] + $m51_10['RT_STET'] + $f51_10['RT_STET'] + $m51_10['RT_STNET'] + $f51_10['RT_STNET'] ; ?></p>
                            <?php echo $m51_10['RT_CTR_S'] + $f51_10['RT_CTR_S'] + $m51_10['RT_CTNR_S'] + $f51_10['RT_CTNR_S'] + $m51_10['RT_STET_S'] + $f51_10['RT_STET_S'] + $m51_10['RT_STNET_S'] + $f51_10['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTR'] + $f51_10['RT_CTR']; ?></p>
                            <?php echo $m51_10['RT_CTR_S'] + $f51_10['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTNR'] + $f51_10['RT_CTNR']; ?></p>
                            <?php echo $m51_10['RT_CTNR_S'] + $f51_10['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_STET'] + $f51_10['RT_STET']; ?></p>
                            <?php echo $m51_10['RT_STET_S'] + $f51_10['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_STNET'] + $f51_10['RT_STNET']; ?></p>
                            <?php echo $m51_10['RT_STNET_S'] + $f51_10['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_COM'] + $f51_10['RT_COM']; ?></p>
                            <?php echo $m51_10['RT_COM_S'] + $f51_10['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_ARR'] + $f51_10['RT_ARR']; ?></p>
                            <?php echo $m51_10['RT_ARR_S'] + $f51_10['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_POS'] + $f51_10['RT_POS']; ?></p>
                            <?php echo $m51_10['RT_POS_S'] + $f51_10['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_OTR'] + $f51_10['RT_OTR']; ?></p>
                            <?php echo $m51_10['RT_OTR_S'] + $f51_10['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_HER'] + $f51_10['FA_HER'] + $m51_10['FA_CV'] + $f51_10['FA_CV'] + $m51_10['FA_ADJ'] + $f51_10['FA_ADJ'] + $m51_10['FA_OTRA'] + $f51_10['FA_OTRA'] ; ?></p>
                            <?php echo $m51_10['FA_HER_S'] + $f51_10['FA_HER_S'] + $m51_10['FA_CV_S'] + $f51_10['FA_CV_S'] + $m51_10['FA_ADJ_S'] + $f51_10['FA_ADJ_S'] + $m51_10['FA_OTRA_S'] + $f51_10['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_HER'] + $f51_10['FA_HER']; ?></p>
                            <?php echo $m51_10['FA_HER_S'] + $f51_10['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_CV'] + $f51_10['FA_CV']; ?></p>
                            <?php echo $m51_10['FA_CV_S'] + $f51_10['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_ADJ'] + $f51_10['FA_ADJ']; ?></p>
                            <?php echo $m51_10['FA_ADJ_S'] + $f51_10['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_OTRA'] + $f51_10['FA_OTRA']; ?></p>
                            <?php echo $m51_10['FA_OTRA_S'] + $f51_10['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTR'] + $f101_20['RT_CTR'] + $m101_20['RT_CTNR'] + $f101_20['RT_CTNR'] + $m101_20['RT_STET'] + $f101_20['RT_STET'] + $m101_20['RT_STNET'] + $f101_20['RT_STNET'] ; ?></p>
                            <?php echo $m101_20['RT_CTR_S'] + $f101_20['RT_CTR_S'] + $m101_20['RT_CTNR_S'] + $f101_20['RT_CTNR_S'] + $m101_20['RT_STET_S'] + $f101_20['RT_STET_S'] + $m101_20['RT_STNET_S'] + $f101_20['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTR'] + $f101_20['RT_CTR']; ?></p>
                            <?php echo $m101_20['RT_CTR_S'] + $f101_20['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTNR'] + $f101_20['RT_CTNR']; ?></p>
                            <?php echo $m101_20['RT_CTNR_S'] + $f101_20['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_STET'] + $f101_20['RT_STET']; ?></p>
                            <?php echo $m101_20['RT_STET_S'] + $f101_20['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_STNET'] + $f101_20['RT_STNET']; ?></p>
                            <?php echo $m101_20['RT_STNET_S'] + $f101_20['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_COM'] + $f101_20['RT_COM']; ?></p>
                            <?php echo $m101_20['RT_COM_S'] + $f101_20['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_ARR'] + $f101_20['RT_ARR']; ?></p>
                            <?php echo $m101_20['RT_ARR_S'] + $f101_20['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_POS'] + $f101_20['RT_POS']; ?></p>
                            <?php echo $m101_20['RT_POS_S'] + $f101_20['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_OTR'] + $f101_20['RT_OTR']; ?></p>
                            <?php echo $m101_20['RT_OTR_S'] + $f101_20['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_HER'] + $f101_20['FA_HER'] + $m101_20['FA_CV'] + $f101_20['FA_CV'] + $m101_20['FA_ADJ'] + $f101_20['FA_ADJ'] + $m101_20['FA_OTRA'] + $f101_20['FA_OTRA'] ; ?></p>
                            <?php echo $m101_20['FA_HER_S'] + $f101_20['FA_HER_S'] + $m101_20['FA_CV_S'] + $f101_20['FA_CV_S'] + $m101_20['FA_ADJ_S'] + $f101_20['FA_ADJ_S'] + $m101_20['FA_OTRA_S'] + $f101_20['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_HER'] + $f101_20['FA_HER']; ?></p>
                            <?php echo $m101_20['FA_HER_S'] + $f101_20['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_CV'] + $f101_20['FA_CV']; ?></p>
                            <?php echo $m101_20['FA_CV_S'] + $f101_20['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_ADJ'] + $f101_20['FA_ADJ']; ?></p>
                            <?php echo $m101_20['FA_ADJ_S'] + $f101_20['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_OTRA'] + $f101_20['FA_OTRA']; ?></p>
                            <?php echo $m101_20['FA_OTRA_S'] + $f101_20['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTR'] + $f201_50['RT_CTR'] + $m201_50['RT_CTNR'] + $f201_50['RT_CTNR'] + $m201_50['RT_STET'] + $f201_50['RT_STET'] + $m201_50['RT_STNET'] + $f201_50['RT_STNET'] ; ?></p>
                            <?php echo $m201_50['RT_CTR_S'] + $f201_50['RT_CTR_S'] + $m201_50['RT_CTNR_S'] + $f201_50['RT_CTNR_S'] + $m201_50['RT_STET_S'] + $f201_50['RT_STET_S'] + $m201_50['RT_STNET_S'] + $f201_50['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTR'] + $f201_50['RT_CTR']; ?></p>
                            <?php echo $m201_50['RT_CTR_S'] + $f201_50['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTNR'] + $f201_50['RT_CTNR']; ?></p>
                            <?php echo $m201_50['RT_CTNR_S'] + $f201_50['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_STET'] + $f201_50['RT_STET']; ?></p>
                            <?php echo $m201_50['RT_STET_S'] + $f201_50['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_STNET'] + $f201_50['RT_STNET']; ?></p>
                            <?php echo $m201_50['RT_STNET_S'] + $f201_50['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_COM'] + $f201_50['RT_COM']; ?></p>
                            <?php echo $m201_50['RT_COM_S'] + $f201_50['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_ARR'] + $f201_50['RT_ARR']; ?></p>
                            <?php echo $m201_50['RT_ARR_S'] + $f201_50['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_POS'] + $f201_50['RT_POS']; ?></p>
                            <?php echo $m201_50['RT_POS_S'] + $f201_50['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_OTR'] + $f201_50['RT_OTR']; ?></p>
                            <?php echo $m201_50['RT_OTR_S'] + $f201_50['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_HER'] + $f201_50['FA_HER'] + $m201_50['FA_CV'] + $f201_50['FA_CV'] + $m201_50['FA_ADJ'] + $f201_50['FA_ADJ'] + $m201_50['FA_OTRA'] + $f201_50['FA_OTRA'] ; ?></p>
                            <?php echo $m201_50['FA_HER_S'] + $f201_50['FA_HER_S'] + $m201_50['FA_CV_S'] + $f201_50['FA_CV_S'] + $m201_50['FA_ADJ_S'] + $f201_50['FA_ADJ_S'] + $m201_50['FA_OTRA_S'] + $f201_50['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_HER'] + $f201_50['FA_HER']; ?></p>
                            <?php echo $m201_50['FA_HER_S'] + $f201_50['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_CV'] + $f201_50['FA_CV']; ?></p>
                            <?php echo $m201_50['FA_CV_S'] + $f201_50['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_ADJ'] + $f201_50['FA_ADJ']; ?></p>
                            <?php echo $m201_50['FA_ADJ_S'] + $f201_50['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_OTRA'] + $f201_50['FA_OTRA']; ?></p>
                            <?php echo $m201_50['FA_OTRA_S'] + $f201_50['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTR'] + $f501_100['RT_CTR'] + $m501_100['RT_CTNR'] + $f501_100['RT_CTNR'] + $m501_100['RT_STET'] + $f501_100['RT_STET'] + $m501_100['RT_STNET'] + $f501_100['RT_STNET'] ; ?></p>
                            <?php echo $m501_100['RT_CTR_S'] + $f501_100['RT_CTR_S'] + $m501_100['RT_CTNR_S'] + $f501_100['RT_CTNR_S'] + $m501_100['RT_STET_S'] + $f501_100['RT_STET_S'] + $m501_100['RT_STNET_S'] + $f501_100['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTR'] + $f501_100['RT_CTR']; ?></p>
                            <?php echo $m501_100['RT_CTR_S'] + $f501_100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTNR'] + $f501_100['RT_CTNR']; ?></p>
                            <?php echo $m501_100['RT_CTNR_S'] + $f501_100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_STET'] + $f501_100['RT_STET']; ?></p>
                            <?php echo $m501_100['RT_STET_S'] + $f501_100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_STNET'] + $f501_100['RT_STNET']; ?></p>
                            <?php echo $m501_100['RT_STNET_S'] + $f501_100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_COM'] + $f501_100['RT_COM']; ?></p>
                            <?php echo $m501_100['RT_COM_S'] + $f501_100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_ARR'] + $f501_100['RT_ARR']; ?></p>
                            <?php echo $m501_100['RT_ARR_S'] + $f501_100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_POS'] + $f501_100['RT_POS']; ?></p>
                            <?php echo $m501_100['RT_POS_S'] + $f501_100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_OTR'] + $f501_100['RT_OTR']; ?></p>
                            <?php echo $m501_100['RT_OTR_S'] + $f501_100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_HER'] + $f501_100['FA_HER'] + $m501_100['FA_CV'] + $f501_100['FA_CV'] + $m501_100['FA_ADJ'] + $f501_100['FA_ADJ'] + $m501_100['FA_OTRA'] + $f501_100['FA_OTRA'] ; ?></p>
                            <?php echo $m501_100['FA_HER_S'] + $f501_100['FA_HER_S'] + $m501_100['FA_CV_S'] + $f501_100['FA_CV_S'] + $m501_100['FA_ADJ_S'] + $f501_100['FA_ADJ_S'] + $m501_100['FA_OTRA_S'] + $f501_100['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_HER'] + $f501_100['FA_HER']; ?></p>
                            <?php echo $m501_100['FA_HER_S'] + $f501_100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_CV'] + $f501_100['FA_CV']; ?></p>
                            <?php echo $m501_100['FA_CV_S'] + $f501_100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_ADJ'] + $f501_100['FA_ADJ']; ?></p>
                            <?php echo $m501_100['FA_ADJ_S'] + $f501_100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_OTRA'] + $f501_100['FA_OTRA']; ?></p>
                            <?php echo $m501_100['FA_OTRA_S'] + $f501_100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTR'] + $f100['RT_CTR'] + $m100['RT_CTNR'] + $f100['RT_CTNR'] + $m100['RT_STET'] + $f100['RT_STET'] + $m100['RT_STNET'] + $f100['RT_STNET'] ; ?></p>
                            <?php echo $m100['RT_CTR_S'] + $f100['RT_CTR_S'] + $m100['RT_CTNR_S'] + $f100['RT_CTNR_S'] + $m100['RT_STET_S'] + $f100['RT_STET_S'] + $m100['RT_STNET_S'] + $f100['RT_STNET_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTR'] + $f100['RT_CTR']; ?></p>
                            <?php echo $m100['RT_CTR_S'] + $f100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTNR'] + $f100['RT_CTNR']; ?></p>
                            <?php echo $m100['RT_CTNR_S'] + $f100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_STET'] + $f100['RT_STET']; ?></p>
                            <?php echo $m100['RT_STET_S'] + $f100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_STNET'] + $f100['RT_STNET']; ?></p>
                            <?php echo $m100['RT_STNET_S'] + $f100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_COM'] + $f100['RT_COM']; ?></p>
                            <?php echo $m100['RT_COM_S'] + $f100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_ARR'] + $f100['RT_ARR']; ?></p>
                            <?php echo $m100['RT_ARR_S'] + $f100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_POS'] + $f100['RT_POS']; ?></p>
                            <?php echo $m100['RT_POS_S'] + $f100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_OTR'] + $f100['RT_OTR']; ?></p>
                            <?php echo $m100['RT_OTR_S'] + $f100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_HER'] + $f100['FA_HER'] + $m100['FA_CV'] + $f100['FA_CV'] + $m100['FA_ADJ'] + $f100['FA_ADJ'] + $m100['FA_OTRA'] + $f100['FA_OTRA'] ; ?></p>
                            <?php echo $m100['FA_HER_S'] + $f100['FA_HER_S'] + $m100['FA_CV_S'] + $f100['FA_CV_S'] + $m100['FA_ADJ_S'] + $f100['FA_ADJ_S'] + $m100['FA_OTRA_S'] + $f100['FA_OTRA_S'] ; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_HER'] + $f100['FA_HER']; ?></p>
                            <?php echo $m100['FA_HER_S'] + $f100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_CV'] + $f100['FA_CV']; ?></p>
                            <?php echo $m100['FA_CV_S'] + $f100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_ADJ'] + $f100['FA_ADJ']; ?></p>
                            <?php echo $m100['FA_ADJ_S'] + $f100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_OTRA'] + $f100['FA_OTRA']; ?></p>
                            <?php echo $m100['FA_OTRA_S'] + $f100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTR'] + $m0_5['RT_CTNR'] + $m0_5['RT_STET'] + $m0_5['RT_STNET'] ; ?></p>
                            <?php echo $m0_5['RT_CTR_S'] + $m0_5['RT_CTNR_S'] + $m0_5['RT_STET_S'] + $m0_5['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTR']; ?></p>
                            <?php echo $m0_5['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_CTNR']; ?></p>
                            <?php echo $m0_5['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_STET']; ?></p>
                            <?php echo $m0_5['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_STNET']; ?></p>
                            <?php echo $m0_5['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_COM']; ?></p>
                            <?php echo $m0_5['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_ARR']; ?></p>
                            <?php echo $m0_5['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_POS']; ?></p>
                            <?php echo $m0_5['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RT_OTR']; ?></p>
                            <?php echo $m0_5['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_HER'] + $m0_5['FA_CV'] + $m0_5['FA_ADJ'] + $m0_5['FA_OTRA']; ?></p>
                            <?php echo $m0_5['FA_HER_S'] + $m0_5['FA_CV_S'] + $m0_5['FA_ADJ_S'] + $m0_5['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_HER']; ?></p>
                            <?php echo $m0_5['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_CV']; ?></p>
                            <?php echo $m0_5['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_ADJ']; ?></p>
                            <?php echo $m0_5['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FA_OTRA']; ?></p>
                            <?php echo $m0_5['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTR'] + $m51_10['RT_CTNR'] + $m51_10['RT_STET'] + $m51_10['RT_STNET'] ; ?></p>
                            <?php echo $m51_10['RT_CTR_S'] + $m51_10['RT_CTNR_S'] + $m51_10['RT_STET_S'] + $m51_10['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTR']; ?></p>
                            <?php echo $m51_10['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_CTNR']; ?></p>
                            <?php echo $m51_10['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_STET']; ?></p>
                            <?php echo $m51_10['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_STNET']; ?></p>
                            <?php echo $m51_10['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_COM']; ?></p>
                            <?php echo $m51_10['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_ARR']; ?></p>
                            <?php echo $m51_10['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_POS']; ?></p>
                            <?php echo $m51_10['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RT_OTR']; ?></p>
                            <?php echo $m51_10['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_HER'] + $m51_10['FA_CV'] + $m51_10['FA_ADJ'] + $m51_10['FA_OTRA']; ?></p>
                            <?php echo $m51_10['FA_HER_S'] + $m51_10['FA_CV_S'] + $m51_10['FA_ADJ_S'] + $m51_10['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_HER']; ?></p>
                            <?php echo $m51_10['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_CV']; ?></p>
                            <?php echo $m51_10['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_ADJ']; ?></p>
                            <?php echo $m51_10['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FA_OTRA']; ?></p>
                            <?php echo $m51_10['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTR'] + $m101_20['RT_CTNR'] + $m101_20['RT_STET'] + $m101_20['RT_STNET'] ; ?></p>
                            <?php echo $m101_20['RT_CTR_S'] + $m101_20['RT_CTNR_S'] + $m101_20['RT_STET_S'] + $m101_20['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTR']; ?></p>
                            <?php echo $m101_20['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_CTNR']; ?></p>
                            <?php echo $m101_20['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_STET']; ?></p>
                            <?php echo $m101_20['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_STNET']; ?></p>
                            <?php echo $m101_20['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_COM']; ?></p>
                            <?php echo $m101_20['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_ARR']; ?></p>
                            <?php echo $m101_20['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_POS']; ?></p>
                            <?php echo $m101_20['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RT_OTR']; ?></p>
                            <?php echo $m101_20['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_HER'] + $m101_20['FA_CV'] + $m101_20['FA_ADJ'] + $m101_20['FA_OTRA']; ?></p>
                            <?php echo $m101_20['FA_HER_S'] + $m101_20['FA_CV_S'] + $m101_20['FA_ADJ_S'] + $m101_20['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_HER']; ?></p>
                            <?php echo $m101_20['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_CV']; ?></p>
                            <?php echo $m101_20['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_ADJ']; ?></p>
                            <?php echo $m101_20['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FA_OTRA']; ?></p>
                            <?php echo $m101_20['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTR'] + $m201_50['RT_CTNR'] + $m201_50['RT_STET'] + $m201_50['RT_STNET'] ; ?></p>
                            <?php echo $m201_50['RT_CTR_S'] + $m201_50['RT_CTNR_S'] + $m201_50['RT_STET_S'] + $m201_50['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTR']; ?></p>
                            <?php echo $m201_50['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_CTNR']; ?></p>
                            <?php echo $m201_50['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_STET']; ?></p>
                            <?php echo $m201_50['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_STNET']; ?></p>
                            <?php echo $m201_50['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_COM']; ?></p>
                            <?php echo $m201_50['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_ARR']; ?></p>
                            <?php echo $m201_50['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_POS']; ?></p>
                            <?php echo $m201_50['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RT_OTR']; ?></p>
                            <?php echo $m201_50['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_HER'] + $m201_50['FA_CV'] + $m201_50['FA_ADJ'] + $m201_50['FA_OTRA']; ?></p>
                            <?php echo $m201_50['FA_HER_S'] + $m201_50['FA_CV_S'] + $m201_50['FA_ADJ_S'] + $m201_50['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_HER']; ?></p>
                            <?php echo $m201_50['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_CV']; ?></p>
                            <?php echo $m201_50['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_ADJ']; ?></p>
                            <?php echo $m201_50['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FA_OTRA']; ?></p>
                            <?php echo $m201_50['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTR'] + $m501_100['RT_CTNR'] + $m501_100['RT_STET'] + $m501_100['RT_STNET'] ; ?></p>
                            <?php echo $m501_100['RT_CTR_S'] + $m501_100['RT_CTNR_S'] + $m501_100['RT_STET_S'] + $m501_100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTR']; ?></p>
                            <?php echo $m501_100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_CTNR']; ?></p>
                            <?php echo $m501_100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_STET']; ?></p>
                            <?php echo $m501_100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_STNET']; ?></p>
                            <?php echo $m501_100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_COM']; ?></p>
                            <?php echo $m501_100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_ARR']; ?></p>
                            <?php echo $m501_100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_POS']; ?></p>
                            <?php echo $m501_100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RT_OTR']; ?></p>
                            <?php echo $m501_100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_HER'] + $m501_100['FA_CV'] + $m501_100['FA_ADJ'] + $m501_100['FA_OTRA']; ?></p>
                            <?php echo $m501_100['FA_HER_S'] + $m501_100['FA_CV_S'] + $m501_100['FA_ADJ_S'] + $m501_100['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_HER']; ?></p>
                            <?php echo $m501_100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_CV']; ?></p>
                            <?php echo $m501_100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_ADJ']; ?></p>
                            <?php echo $m501_100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FA_OTRA']; ?></p>
                            <?php echo $m501_100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTR'] + $m100['RT_CTNR'] + $m100['RT_STET'] + $m100['RT_STNET'] ; ?></p>
                            <?php echo $m100['RT_CTR_S'] + $m100['RT_CTNR_S'] + $m100['RT_STET_S'] + $m100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTR']; ?></p>
                            <?php echo $m100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_CTNR']; ?></p>
                            <?php echo $m100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_STET']; ?></p>
                            <?php echo $m100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_STNET']; ?></p>
                            <?php echo $m100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_COM']; ?></p>
                            <?php echo $m100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_ARR']; ?></p>
                            <?php echo $m100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_POS']; ?></p>
                            <?php echo $m100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RT_OTR']; ?></p>
                            <?php echo $m100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_HER'] + $m100['FA_CV'] + $m100['FA_ADJ'] + $m100['FA_OTRA']; ?></p>
                            <?php echo $m100['FA_HER_S'] + $m100['FA_CV_S'] + $m100['FA_ADJ_S'] + $m100['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_HER']; ?></p>
                            <?php echo $m100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_CV']; ?></p>
                            <?php echo $m100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_ADJ']; ?></p>
                            <?php echo $m100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FA_OTRA']; ?></p>
                            <?php echo $m100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_CTR'] + $f0_5['RT_CTNR'] + $f0_5['RT_STET'] + $f0_5['RT_STNET'] ; ?></p>
                            <?php echo $f0_5['RT_CTR_S'] + $f0_5['RT_CTNR_S'] + $f0_5['RT_STET_S'] + $f0_5['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_CTR']; ?></p>
                            <?php echo $f0_5['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_CTNR']; ?></p>
                            <?php echo $f0_5['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_STET']; ?></p>
                            <?php echo $f0_5['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_STNET']; ?></p>
                            <?php echo $f0_5['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_COM']; ?></p>
                            <?php echo $f0_5['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_ARR']; ?></p>
                            <?php echo $f0_5['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_POS']; ?></p>
                            <?php echo $f0_5['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RT_OTR']; ?></p>
                            <?php echo $f0_5['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FA_HER'] + $f0_5['FA_CV'] + $f0_5['FA_ADJ'] + $f0_5['FA_OTRA']; ?></p>
                            <?php echo $f0_5['FA_HER_S'] + $f0_5['FA_CV_S'] + $f0_5['FA_ADJ_S'] + $f0_5['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FA_HER']; ?></p>
                            <?php echo $f0_5['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FA_CV']; ?></p>
                            <?php echo $f0_5['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FA_ADJ']; ?></p>
                            <?php echo $f0_5['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FA_OTRA']; ?></p>
                            <?php echo $f0_5['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_CTR'] + $f51_10['RT_CTNR'] + $f51_10['RT_STET'] + $f51_10['RT_STNET'] ; ?></p>
                            <?php echo $f51_10['RT_CTR_S'] + $f51_10['RT_CTNR_S'] + $f51_10['RT_STET_S'] + $f51_10['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_CTR']; ?></p>
                            <?php echo $f51_10['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_CTNR']; ?></p>
                            <?php echo $f51_10['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_STET']; ?></p>
                            <?php echo $f51_10['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_STNET']; ?></p>
                            <?php echo $f51_10['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_COM']; ?></p>
                            <?php echo $f51_10['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_ARR']; ?></p>
                            <?php echo $f51_10['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_POS']; ?></p>
                            <?php echo $f51_10['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RT_OTR']; ?></p>
                            <?php echo $f51_10['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FA_HER'] + $f51_10['FA_CV'] + $f51_10['FA_ADJ'] + $f51_10['FA_OTRA']; ?></p>
                            <?php echo $f51_10['FA_HER_S'] + $f51_10['FA_CV_S'] + $f51_10['FA_ADJ_S'] + $f51_10['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FA_HER']; ?></p>
                            <?php echo $f51_10['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FA_CV']; ?></p>
                            <?php echo $f51_10['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FA_ADJ']; ?></p>
                            <?php echo $f51_10['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FA_OTRA']; ?></p>
                            <?php echo $f51_10['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_CTR'] + $f101_20['RT_CTNR'] + $f101_20['RT_STET'] + $f101_20['RT_STNET'] ; ?></p>
                            <?php echo $f101_20['RT_CTR_S'] + $f101_20['RT_CTNR_S'] + $f101_20['RT_STET_S'] + $f101_20['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_CTR']; ?></p>
                            <?php echo $f101_20['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_CTNR']; ?></p>
                            <?php echo $f101_20['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_STET']; ?></p>
                            <?php echo $f101_20['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_STNET']; ?></p>
                            <?php echo $f101_20['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_COM']; ?></p>
                            <?php echo $f101_20['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_ARR']; ?></p>
                            <?php echo $f101_20['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_POS']; ?></p>
                            <?php echo $f101_20['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RT_OTR']; ?></p>
                            <?php echo $f101_20['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FA_HER'] + $f101_20['FA_CV'] + $f101_20['FA_ADJ'] + $f101_20['FA_OTRA']; ?></p>
                            <?php echo $f101_20['FA_HER_S'] + $f101_20['FA_CV_S'] + $f101_20['FA_ADJ_S'] + $f101_20['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FA_HER']; ?></p>
                            <?php echo $f101_20['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FA_CV']; ?></p>
                            <?php echo $f101_20['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FA_ADJ']; ?></p>
                            <?php echo $f101_20['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FA_OTRA']; ?></p>
                            <?php echo $f101_20['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_CTR'] + $f201_50['RT_CTNR'] + $f201_50['RT_STET'] + $f201_50['RT_STNET'] ; ?></p>
                            <?php echo $f201_50['RT_CTR_S'] + $f201_50['RT_CTNR_S'] + $f201_50['RT_STET_S'] + $f201_50['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_CTR']; ?></p>
                            <?php echo $f201_50['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_CTNR']; ?></p>
                            <?php echo $f201_50['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_STET']; ?></p>
                            <?php echo $f201_50['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_STNET']; ?></p>
                            <?php echo $f201_50['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_COM']; ?></p>
                            <?php echo $f201_50['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_ARR']; ?></p>
                            <?php echo $f201_50['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_POS']; ?></p>
                            <?php echo $f201_50['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RT_OTR']; ?></p>
                            <?php echo $f201_50['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FA_HER'] + $f201_50['FA_CV'] + $f201_50['FA_ADJ'] + $f201_50['FA_OTRA']; ?></p>
                            <?php echo $f201_50['FA_HER_S'] + $f201_50['FA_CV_S'] + $f201_50['FA_ADJ_S'] + $f201_50['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FA_HER']; ?></p>
                            <?php echo $f201_50['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FA_CV']; ?></p>
                            <?php echo $f201_50['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FA_ADJ']; ?></p>
                            <?php echo $f201_50['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FA_OTRA']; ?></p>
                            <?php echo $f201_50['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_CTR'] + $f501_100['RT_CTNR'] + $f501_100['RT_STET'] + $f501_100['RT_STNET'] ; ?></p>
                            <?php echo $f501_100['RT_CTR_S'] + $f501_100['RT_CTNR_S'] + $f501_100['RT_STET_S'] + $f501_100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_CTR']; ?></p>
                            <?php echo $f501_100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_CTNR']; ?></p>
                            <?php echo $f501_100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_STET']; ?></p>
                            <?php echo $f501_100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_STNET']; ?></p>
                            <?php echo $f501_100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_COM']; ?></p>
                            <?php echo $f501_100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_ARR']; ?></p>
                            <?php echo $f501_100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_POS']; ?></p>
                            <?php echo $f501_100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RT_OTR']; ?></p>
                            <?php echo $f501_100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FA_HER'] + $f501_100['FA_CV'] + $f501_100['FA_ADJ'] + $f501_100['FA_OTRA']; ?></p>
                            <?php echo $f501_100['FA_HER_S'] + $f501_100['FA_CV_S'] + $f501_100['FA_ADJ_S'] + $f501_100['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FA_HER']; ?></p>
                            <?php echo $f501_100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FA_CV']; ?></p>
                            <?php echo $f501_100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FA_ADJ']; ?></p>
                            <?php echo $f501_100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FA_OTRA']; ?></p>
                            <?php echo $f501_100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_CTR'] + $f100['RT_CTNR'] + $f100['RT_STET'] + $f100['RT_STNET'] ; ?></p>
                            <?php echo $f100['RT_CTR_S'] + $f100['RT_CTNR_S'] + $f100['RT_STET_S'] + $f100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_CTR']; ?></p>
                            <?php echo $f100['RT_CTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_CTNR']; ?></p>
                            <?php echo $f100['RT_CTNR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_STET']; ?></p>
                            <?php echo $f100['RT_STET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_STNET']; ?></p>
                            <?php echo $f100['RT_STNET_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_COM']; ?></p>
                            <?php echo $f100['RT_COM_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_ARR']; ?></p>
                            <?php echo $f100['RT_ARR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_POS']; ?></p>
                            <?php echo $f100['RT_POS_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RT_OTR']; ?></p>
                            <?php echo $f100['RT_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FA_HER'] + $f100['FA_CV'] + $f100['FA_ADJ'] + $f100['FA_OTRA']; ?></p>
                            <?php echo $f100['FA_HER_S'] + $f100['FA_CV_S'] + $f100['FA_ADJ_S'] + $f100['FA_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FA_HER']; ?></p>
                            <?php echo $f100['FA_HER_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FA_CV']; ?></p>
                            <?php echo $f100['FA_CV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FA_ADJ']; ?></p>
                            <?php echo $f100['FA_ADJ_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FA_OTRA']; ?></p>
                            <?php echo $f100['FA_OTRA_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       