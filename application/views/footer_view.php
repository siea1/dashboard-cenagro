 <footer class="footer">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022 <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">| Ministerio de Agricultura y Riego</a>.</span>
                    </div>
                </div>    
            </div>        
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script>
    localStorage.setItem('data', JSON.stringify(<?php echo $data;?>));
  </script>
  <script src="<?php echo base_url(); ?>resources/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url(); ?>resources/vendors/chart.js/Chart.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script src="<?php echo base_url(); ?>resources/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/template.js"></script>
  <script src="<?php echo base_url(); ?>resources/vendors/select2/select2.min.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/todolist.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/select2.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/jquery.table2excel.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/scripts.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>

</html>