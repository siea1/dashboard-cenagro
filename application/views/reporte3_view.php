
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Asociatividad</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('Asociatividad')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Pertenecen a alguna asociación</h4>
                          <canvas id="pieChartR3"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Beneficios que brinda la asociatividad</h4>
                          <canvas id="doughnutChartR3"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th colspan="2" class="table-info"> 
                            <center>Pertenecen a alguna Asociación, Comité o Cooperativa</center>
                          </th>
                          <th colspan="7" class="table-warning"> 
                            <center>Tipo de Beneficio o Servicios que les brindan</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Total/ Si pertenecen
                          </td>
                          <td>
                            Total/ No pertenecen
                          </td>
                          <td>
                            Abas. Ins. Agric./Pec.
                          </td>
                          <td>
                            Acc. Merc. Loc. Nac.
                          </td>
                          <td>
                            Acc. Merc. Ext.
                          </td>
                          <td>
                            Asist. Tec. y/o Capac.
                          </td>
                          <td>
                            Acc. Serv. Finan./Cred.
                          </td>
                          <td>
                            Otros
                          </td>
                          <td>
                            No Reciben Benef. o Serv.
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PAC_SI'] + $f0_5['PAC_SI']; ?></p>
                            <?php echo $m0_5['PAC_SI_S'] + $f0_5['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PAC_NO'] + $f0_5['PAC_NO']; ?></p>
                            <?php echo $m0_5['PAC_NO_S'] + $f0_5['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AIA'] + $f0_5['TB_AIA']; ?></p>
                            <?php echo $m0_5['TB_AIA_S'] + $f0_5['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AML'] + $f0_5['TB_AML']; ?></p>
                            <?php echo $m0_5['TB_AML_S'] + $f0_5['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AME'] + $f0_5['TB_AME']; ?></p>
                            <?php echo $m0_5['TB_AME_S'] + $f0_5['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_OAT'] + $f0_5['TB_OAT']; ?></p>
                            <?php echo $m0_5['TB_OAT_S'] + $f0_5['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_ASF'] + $f0_5['TB_ASF']; ?></p>
                            <?php echo $m0_5['TB_ASF_S'] + $f0_5['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_OTR'] + $f0_5['TB_OTR']; ?></p>
                            <?php echo $m0_5['TB_OTR_S'] + $f0_5['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_NO'] + $f0_5['TB_NO']; ?></p>
                            <?php echo $m0_5['TB_NO_S'] + $f0_5['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PAC_SI'] + $f51_10['PAC_SI']; ?></p>
                            <?php echo $m51_10['PAC_SI_S'] + $f51_10['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PAC_NO'] + $f51_10['PAC_NO']; ?></p>
                            <?php echo $m51_10['PAC_NO_S'] + $f51_10['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AIA'] + $f51_10['TB_AIA']; ?></p>
                            <?php echo $m51_10['TB_AIA_S'] + $f51_10['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AML'] + $f51_10['TB_AML']; ?></p>
                            <?php echo $m51_10['TB_AML_S'] + $f51_10['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AME'] + $f51_10['TB_AME']; ?></p>
                            <?php echo $m51_10['TB_AME_S'] + $f51_10['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_OAT'] + $f51_10['TB_OAT']; ?></p>
                            <?php echo $m51_10['TB_OAT_S'] + $f51_10['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_ASF'] + $f51_10['TB_ASF']; ?></p>
                            <?php echo $m51_10['TB_ASF_S'] + $f51_10['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_OTR'] + $f51_10['TB_OTR']; ?></p>
                            <?php echo $m51_10['TB_OTR_S'] + $f51_10['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_NO'] + $f51_10['TB_NO']; ?></p>
                            <?php echo $m51_10['TB_NO_S'] + $f51_10['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PAC_SI'] + $f101_20['PAC_SI']; ?></p>
                            <?php echo $m101_20['PAC_SI_S'] + $f101_20['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PAC_NO'] + $f101_20['PAC_NO']; ?></p>
                            <?php echo $m101_20['PAC_NO_S'] + $f101_20['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AIA'] + $f101_20['TB_AIA']; ?></p>
                            <?php echo $m101_20['TB_AIA_S'] + $f101_20['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AML'] + $f101_20['TB_AML']; ?></p>
                            <?php echo $m101_20['TB_AML_S'] + $f101_20['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AME'] + $f101_20['TB_AME']; ?></p>
                            <?php echo $m101_20['TB_AME_S'] + $f101_20['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_OAT'] + $f101_20['TB_OAT']; ?></p>
                            <?php echo $m101_20['TB_OAT_S'] + $f101_20['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_ASF'] + $f101_20['TB_ASF']; ?></p>
                            <?php echo $m101_20['TB_ASF_S'] + $f101_20['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_OTR'] + $f101_20['TB_OTR']; ?></p>
                            <?php echo $m101_20['TB_OTR_S'] + $f101_20['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_NO'] + $f101_20['TB_NO']; ?></p>
                            <?php echo $m101_20['TB_NO_S'] + $f101_20['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PAC_SI'] + $f201_50['PAC_SI']; ?></p>
                            <?php echo $m201_50['PAC_SI_S'] + $f201_50['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PAC_NO'] + $f201_50['PAC_NO']; ?></p>
                            <?php echo $m201_50['PAC_NO_S'] + $f201_50['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AIA'] + $f201_50['TB_AIA']; ?></p>
                            <?php echo $m201_50['TB_AIA_S'] + $f201_50['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AML'] + $f201_50['TB_AML']; ?></p>
                            <?php echo $m201_50['TB_AML_S'] + $f201_50['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AME'] + $f201_50['TB_AME']; ?></p>
                            <?php echo $m201_50['TB_AME_S'] + $f201_50['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_OAT'] + $f201_50['TB_OAT']; ?></p>
                            <?php echo $m201_50['TB_OAT_S'] + $f201_50['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_ASF'] + $f201_50['TB_ASF']; ?></p>
                            <?php echo $m201_50['TB_ASF_S'] + $f201_50['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_OTR'] + $f201_50['TB_OTR']; ?></p>
                            <?php echo $m201_50['TB_OTR_S'] + $f201_50['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_NO'] + $f201_50['TB_NO']; ?></p>
                            <?php echo $m201_50['TB_NO_S'] + $f201_50['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PAC_SI'] + $f501_100['PAC_SI']; ?></p>
                            <?php echo $m501_100['PAC_SI_S'] + $f501_100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PAC_NO'] + $f501_100['PAC_NO']; ?></p>
                            <?php echo $m501_100['PAC_NO_S'] + $f501_100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AIA'] + $f501_100['TB_AIA']; ?></p>
                            <?php echo $m501_100['TB_AIA_S'] + $f501_100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AML'] + $f501_100['TB_AML']; ?></p>
                            <?php echo $m501_100['TB_AML_S'] + $f501_100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AME'] + $f501_100['TB_AME']; ?></p>
                            <?php echo $m501_100['TB_AME_S'] + $f501_100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_OAT'] + $f501_100['TB_OAT']; ?></p>
                            <?php echo $m501_100['TB_OAT_S'] + $f501_100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_ASF'] + $f501_100['TB_ASF']; ?></p>
                            <?php echo $m501_100['TB_ASF_S'] + $f501_100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_OTR'] + $f501_100['TB_OTR']; ?></p>
                            <?php echo $m501_100['TB_OTR_S'] + $f501_100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_NO'] + $f501_100['TB_NO']; ?></p>
                            <?php echo $m501_100['TB_NO_S'] + $f501_100['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PAC_SI'] + $f100['PAC_SI']; ?></p>
                            <?php echo $m100['PAC_SI_S'] + $f100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PAC_NO'] + $f100['PAC_NO']; ?></p>
                            <?php echo $m100['PAC_NO_S'] + $f100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AIA'] + $f100['TB_AIA']; ?></p>
                            <?php echo $m100['TB_AIA_S'] + $f100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AML'] + $f100['TB_AML']; ?></p>
                            <?php echo $m100['TB_AML_S'] + $f100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AME'] + $f100['TB_AME']; ?></p>
                            <?php echo $m100['TB_AME_S'] + $f100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_OAT'] + $f100['TB_OAT']; ?></p>
                            <?php echo $m100['TB_OAT_S'] + $f100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_ASF'] + $f100['TB_ASF']; ?></p>
                            <?php echo $m100['TB_ASF_S'] + $f100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_OTR'] + $f100['TB_OTR']; ?></p>
                            <?php echo $m100['TB_OTR_S'] + $f100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_NO'] + $f100['TB_NO']; ?></p>
                            <?php echo $m100['TB_NO_S'] + $f100['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PAC_SI']; ?></p>
                            <?php echo $m0_5['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PAC_NO']; ?></p>
                            <?php echo $m0_5['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AIA']; ?></p>
                            <?php echo $m0_5['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AML']; ?></p>
                            <?php echo $m0_5['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_AME']; ?></p>
                            <?php echo $m0_5['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_OAT']; ?></p>
                            <?php echo $m0_5['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_ASF']; ?></p>
                            <?php echo $m0_5['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_OTR']; ?></p>
                            <?php echo $m0_5['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TB_NO']; ?></p>
                            <?php echo $m0_5['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PAC_SI']; ?></p>
                            <?php echo $m51_10['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PAC_NO']; ?></p>
                            <?php echo $m51_10['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AIA']; ?></p>
                            <?php echo $m51_10['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AML']; ?></p>
                            <?php echo $m51_10['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_AME']; ?></p>
                            <?php echo $m51_10['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_OAT']; ?></p>
                            <?php echo $m51_10['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_ASF']; ?></p>
                            <?php echo $m51_10['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_OTR']; ?></p>
                            <?php echo $m51_10['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TB_NO']; ?></p>
                            <?php echo $m51_10['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PAC_SI']; ?></p>
                            <?php echo $m101_20['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PAC_NO']; ?></p>
                            <?php echo $m101_20['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AIA']; ?></p>
                            <?php echo $m101_20['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AML']; ?></p>
                            <?php echo $m101_20['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_AME']; ?></p>
                            <?php echo $m101_20['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_OAT']; ?></p>
                            <?php echo $m101_20['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_ASF']; ?></p>
                            <?php echo $m101_20['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_OTR']; ?></p>
                            <?php echo $m101_20['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TB_NO']; ?></p>
                            <?php echo $m101_20['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PAC_SI']; ?></p>
                            <?php echo $m201_50['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PAC_NO']; ?></p>
                            <?php echo $m201_50['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AIA']; ?></p>
                            <?php echo $m201_50['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AML']; ?></p>
                            <?php echo $m201_50['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_AME']; ?></p>
                            <?php echo $m201_50['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_OAT']; ?></p>
                            <?php echo $m201_50['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_ASF']; ?></p>
                            <?php echo $m201_50['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_OTR']; ?></p>
                            <?php echo $m201_50['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TB_NO']; ?></p>
                            <?php echo $m201_50['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PAC_SI']; ?></p>
                            <?php echo $m501_100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PAC_NO']; ?></p>
                            <?php echo $m501_100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AIA']; ?></p>
                            <?php echo $m501_100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AML']; ?></p>
                            <?php echo $m501_100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_AME']; ?></p>
                            <?php echo $m501_100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_OAT']; ?></p>
                            <?php echo $m501_100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_ASF']; ?></p>
                            <?php echo $m501_100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_OTR']; ?></p>
                            <?php echo $m501_100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TB_NO']; ?></p>
                            <?php echo $m501_100['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PAC_SI']; ?></p>
                            <?php echo $m100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PAC_NO']; ?></p>
                            <?php echo $m100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AIA']; ?></p>
                            <?php echo $m100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AML']; ?></p>
                            <?php echo $m100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_AME']; ?></p>
                            <?php echo $m100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_OAT']; ?></p>
                            <?php echo $m100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_ASF']; ?></p>
                            <?php echo $m100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_OTR']; ?></p>
                            <?php echo $m100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TB_NO']; ?></p>
                            <?php echo $m100['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PAC_SI']; ?></p>
                            <?php echo $f0_5['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PAC_NO']; ?></p>
                            <?php echo $f0_5['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_AIA']; ?></p>
                            <?php echo $f0_5['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_AML']; ?></p>
                            <?php echo $f0_5['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_AME']; ?></p>
                            <?php echo $f0_5['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_OAT']; ?></p>
                            <?php echo $f0_5['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_ASF']; ?></p>
                            <?php echo $f0_5['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_OTR']; ?></p>
                            <?php echo $f0_5['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TB_NO']; ?></p>
                            <?php echo $f0_5['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PAC_SI']; ?></p>
                            <?php echo $f51_10['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PAC_NO']; ?></p>
                            <?php echo $f51_10['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_AIA']; ?></p>
                            <?php echo $f51_10['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_AML']; ?></p>
                            <?php echo $f51_10['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_AME']; ?></p>
                            <?php echo $f51_10['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_OAT']; ?></p>
                            <?php echo $f51_10['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_ASF']; ?></p>
                            <?php echo $f51_10['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_OTR']; ?></p>
                            <?php echo $f51_10['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TB_NO']; ?></p>
                            <?php echo $f51_10['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PAC_SI']; ?></p>
                            <?php echo $f101_20['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PAC_NO']; ?></p>
                            <?php echo $f101_20['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_AIA']; ?></p>
                            <?php echo $f101_20['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_AML']; ?></p>
                            <?php echo $f101_20['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_AME']; ?></p>
                            <?php echo $f101_20['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_OAT']; ?></p>
                            <?php echo $f101_20['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_ASF']; ?></p>
                            <?php echo $f101_20['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_OTR']; ?></p>
                            <?php echo $f101_20['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TB_NO']; ?></p>
                            <?php echo $f101_20['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PAC_SI']; ?></p>
                            <?php echo $f201_50['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PAC_NO']; ?></p>
                            <?php echo $f201_50['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_AIA']; ?></p>
                            <?php echo $f201_50['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_AML']; ?></p>
                            <?php echo $f201_50['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_AME']; ?></p>
                            <?php echo $f201_50['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_OAT']; ?></p>
                            <?php echo $f201_50['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_ASF']; ?></p>
                            <?php echo $f201_50['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_OTR']; ?></p>
                            <?php echo $f201_50['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TB_NO']; ?></p>
                            <?php echo $f201_50['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PAC_SI']; ?></p>
                            <?php echo $f501_100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PAC_NO']; ?></p>
                            <?php echo $f501_100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_AIA']; ?></p>
                            <?php echo $f501_100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_AML']; ?></p>
                            <?php echo $f501_100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_AME']; ?></p>
                            <?php echo $f501_100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_OAT']; ?></p>
                            <?php echo $f501_100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_ASF']; ?></p>
                            <?php echo $f501_100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_OTR']; ?></p>
                            <?php echo $f501_100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TB_NO']; ?></p>
                            <?php echo $f501_100['TB_NO_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PAC_SI']; ?></p>
                            <?php echo $f100['PAC_SI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PAC_NO']; ?></p>
                            <?php echo $f100['PAC_NO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_AIA']; ?></p>
                            <?php echo $f100['TB_AIA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_AML']; ?></p>
                            <?php echo $f100['TB_AML_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_AME']; ?></p>
                            <?php echo $f100['TB_AME_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_OAT']; ?></p>
                            <?php echo $f100['TB_OAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_ASF']; ?></p>
                            <?php echo $f100['TB_ASF_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_OTR']; ?></p>
                            <?php echo $f100['TB_OTR_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TB_NO']; ?></p>
                            <?php echo $f100['TB_NO_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       