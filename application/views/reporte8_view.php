
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Hogar del productor</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('HogarProductor')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="pieChartR8"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="doughnutChartR8"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th class="table-info"> 
                            <center>Productores Agrop. Individuales</center>
                          </th>
                          <th colspan="6" class="table-warning"> 
                            <center>Tamaño de Hogar</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Total
                          </td>
                          <td>
                            1 Miembro
                          </td>
                          <td>
                            2 a 3 Miembros
                          </td>
                          <td>
                            4 a 5 Miembros
                          </td>
                          <td>
                            6 a 8 Miembros
                          </td>
                          <td>
                            7 a 9 Miembros
                          </td>
                          <td>
                            Mas de 10 Miembros
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_0'] + $f0_5['TH_0']; ?></p>
                            <?php echo $m0_5['TH_0_S'] + $f0_5['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_1'] + $f0_5['TH_1']; ?></p>
                            <?php echo $m0_5['TH_1_S'] + $f0_5['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_2'] + $f0_5['TH_2']; ?></p>
                            <?php echo $m0_5['TH_2_S'] + $f0_5['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_3'] + $f0_5['TH_3']; ?></p>
                            <?php echo $m0_5['TH_3_S'] + $f0_5['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_4'] + $f0_5['TH_4']; ?></p>
                            <?php echo $m0_5['TH_4_S'] + $f0_5['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_5'] + $f0_5['TH_5']; ?></p>
                            <?php echo $m0_5['TH_5_S'] + $f0_5['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_6'] + $f0_5['TH_6']; ?></p>
                            <?php echo $m0_5['TH_6_S'] + $f0_5['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_0'] + $f51_10['TH_0']; ?></p>
                            <?php echo $m51_10['TH_0_S'] + $f51_10['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_1'] + $f51_10['TH_1']; ?></p>
                            <?php echo $m51_10['TH_1_S'] + $f51_10['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_2'] + $f51_10['TH_2']; ?></p>
                            <?php echo $m51_10['TH_2_S'] + $f51_10['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_3'] + $f51_10['TH_3']; ?></p>
                            <?php echo $m51_10['TH_3_S'] + $f51_10['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_4'] + $f51_10['TH_4']; ?></p>
                            <?php echo $m51_10['TH_4_S'] + $f51_10['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_5'] + $f51_10['TH_5']; ?></p>
                            <?php echo $m51_10['TH_5_S'] + $f51_10['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_6'] + $f51_10['TH_6']; ?></p>
                            <?php echo $m51_10['TH_6_S'] + $f51_10['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_0'] + $f101_20['TH_0']; ?></p>
                            <?php echo $m101_20['TH_0_S'] + $f101_20['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_1'] + $f101_20['TH_1']; ?></p>
                            <?php echo $m101_20['TH_1_S'] + $f101_20['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_2'] + $f101_20['TH_2']; ?></p>
                            <?php echo $m101_20['TH_2_S'] + $f101_20['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_3'] + $f101_20['TH_3']; ?></p>
                            <?php echo $m101_20['TH_3_S'] + $f101_20['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_4'] + $f101_20['TH_4']; ?></p>
                            <?php echo $m101_20['TH_4_S'] + $f101_20['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_5'] + $f101_20['TH_5']; ?></p>
                            <?php echo $m101_20['TH_5_S'] + $f101_20['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_6'] + $f101_20['TH_6']; ?></p>
                            <?php echo $m101_20['TH_6_S'] + $f101_20['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_0'] + $f201_50['TH_0']; ?></p>
                            <?php echo $m201_50['TH_0_S'] + $f201_50['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_1'] + $f201_50['TH_1']; ?></p>
                            <?php echo $m201_50['TH_1_S'] + $f201_50['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_2'] + $f201_50['TH_2']; ?></p>
                            <?php echo $m201_50['TH_2_S'] + $f201_50['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_3'] + $f201_50['TH_3']; ?></p>
                            <?php echo $m201_50['TH_3_S'] + $f201_50['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_4'] + $f201_50['TH_4']; ?></p>
                            <?php echo $m201_50['TH_4_S'] + $f201_50['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_5'] + $f201_50['TH_5']; ?></p>
                            <?php echo $m201_50['TH_5_S'] + $f201_50['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_6'] + $f201_50['TH_6']; ?></p>
                            <?php echo $m201_50['TH_6_S'] + $f201_50['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_0'] + $f501_100['TH_0']; ?></p>
                            <?php echo $m501_100['TH_0_S'] + $f501_100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_1'] + $f501_100['TH_1']; ?></p>
                            <?php echo $m501_100['TH_1_S'] + $f501_100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_2'] + $f501_100['TH_2']; ?></p>
                            <?php echo $m501_100['TH_2_S'] + $f501_100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_3'] + $f501_100['TH_3']; ?></p>
                            <?php echo $m501_100['TH_3_S'] + $f501_100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_4'] + $f501_100['TH_4']; ?></p>
                            <?php echo $m501_100['TH_4_S'] + $f501_100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_5'] + $f501_100['TH_5']; ?></p>
                            <?php echo $m501_100['TH_5_S'] + $f501_100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_6'] + $f501_100['TH_6']; ?></p>
                            <?php echo $m501_100['TH_6_S'] + $f501_100['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_0'] + $f100['TH_0']; ?></p>
                            <?php echo $m100['TH_0_S'] + $f100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_1'] + $f100['TH_1']; ?></p>
                            <?php echo $m100['TH_1_S'] + $f100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_2'] + $f100['TH_2']; ?></p>
                            <?php echo $m100['TH_2_S'] + $f100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_3'] + $f100['TH_3']; ?></p>
                            <?php echo $m100['TH_3_S'] + $f100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_4'] + $f100['TH_4']; ?></p>
                            <?php echo $m100['TH_4_S'] + $f100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_5'] + $f100['TH_5']; ?></p>
                            <?php echo $m100['TH_5_S'] + $f100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_6'] + $f100['TH_6']; ?></p>
                            <?php echo $m100['TH_6_S'] + $f100['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_0']; ?></p>
                            <?php echo $m0_5['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_1']; ?></p>
                            <?php echo $m0_5['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_2']; ?></p>
                            <?php echo $m0_5['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_3']; ?></p>
                            <?php echo $m0_5['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_4']; ?></p>
                            <?php echo $m0_5['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_5']; ?></p>
                            <?php echo $m0_5['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TH_6']; ?></p>
                            <?php echo $m0_5['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_0']; ?></p>
                            <?php echo $m51_10['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_1']; ?></p>
                            <?php echo $m51_10['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_2']; ?></p>
                            <?php echo $m51_10['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_3']; ?></p>
                            <?php echo $m51_10['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_4']; ?></p>
                            <?php echo $m51_10['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_5']; ?></p>
                            <?php echo $m51_10['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TH_6']; ?></p>
                            <?php echo $m51_10['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_0']; ?></p>
                            <?php echo $m101_20['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_1']; ?></p>
                            <?php echo $m101_20['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_2']; ?></p>
                            <?php echo $m101_20['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_3']; ?></p>
                            <?php echo $m101_20['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_4']; ?></p>
                            <?php echo $m101_20['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_5']; ?></p>
                            <?php echo $m101_20['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TH_6']; ?></p>
                            <?php echo $m101_20['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_0']; ?></p>
                            <?php echo $m201_50['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_1']; ?></p>
                            <?php echo $m201_50['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_2']; ?></p>
                            <?php echo $m201_50['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_3']; ?></p>
                            <?php echo $m201_50['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_4']; ?></p>
                            <?php echo $m201_50['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_5']; ?></p>
                            <?php echo $m201_50['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TH_6']; ?></p>
                            <?php echo $m201_50['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_0']; ?></p>
                            <?php echo $m501_100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_1']; ?></p>
                            <?php echo $m501_100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_2']; ?></p>
                            <?php echo $m501_100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_3']; ?></p>
                            <?php echo $m501_100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_4']; ?></p>
                            <?php echo $m501_100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_5']; ?></p>
                            <?php echo $m501_100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TH_6']; ?></p>
                            <?php echo $m501_100['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_0']; ?></p>
                            <?php echo $m100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_1']; ?></p>
                            <?php echo $m100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_2']; ?></p>
                            <?php echo $m100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_3']; ?></p>
                            <?php echo $m100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_4']; ?></p>
                            <?php echo $m100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_5']; ?></p>
                            <?php echo $m100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TH_6']; ?></p>
                            <?php echo $m100['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_0']; ?></p>
                            <?php echo $f0_5['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_1']; ?></p>
                            <?php echo $f0_5['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_2']; ?></p>
                            <?php echo $f0_5['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_3']; ?></p>
                            <?php echo $f0_5['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_4']; ?></p>
                            <?php echo $f0_5['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_5']; ?></p>
                            <?php echo $f0_5['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TH_6']; ?></p>
                            <?php echo $f0_5['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_0']; ?></p>
                            <?php echo $f51_10['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_1']; ?></p>
                            <?php echo $f51_10['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_2']; ?></p>
                            <?php echo $f51_10['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_3']; ?></p>
                            <?php echo $f51_10['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_4']; ?></p>
                            <?php echo $f51_10['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_5']; ?></p>
                            <?php echo $f51_10['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TH_6']; ?></p>
                            <?php echo $f51_10['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_0']; ?></p>
                            <?php echo $f101_20['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_1']; ?></p>
                            <?php echo $f101_20['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_2']; ?></p>
                            <?php echo $f101_20['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_3']; ?></p>
                            <?php echo $f101_20['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_4']; ?></p>
                            <?php echo $f101_20['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_5']; ?></p>
                            <?php echo $f101_20['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TH_6']; ?></p>
                            <?php echo $f101_20['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_0']; ?></p>
                            <?php echo $f201_50['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_1']; ?></p>
                            <?php echo $f201_50['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_2']; ?></p>
                            <?php echo $f201_50['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_3']; ?></p>
                            <?php echo $f201_50['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_4']; ?></p>
                            <?php echo $f201_50['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_5']; ?></p>
                            <?php echo $f201_50['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TH_6']; ?></p>
                            <?php echo $f201_50['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_0']; ?></p>
                            <?php echo $f501_100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_1']; ?></p>
                            <?php echo $f501_100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_2']; ?></p>
                            <?php echo $f501_100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_3']; ?></p>
                            <?php echo $f501_100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_4']; ?></p>
                            <?php echo $f501_100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_5']; ?></p>
                            <?php echo $f501_100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TH_6']; ?></p>
                            <?php echo $f501_100['TH_6_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_0']; ?></p>
                            <?php echo $f100['TH_0_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_1']; ?></p>
                            <?php echo $f100['TH_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_2']; ?></p>
                            <?php echo $f100['TH_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_3']; ?></p>
                            <?php echo $f100['TH_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_4']; ?></p>
                            <?php echo $f100['TH_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_5']; ?></p>
                            <?php echo $f100['TH_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TH_6']; ?></p>
                            <?php echo $f100['TH_6_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       