
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
              <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Dashboard</h4>
                  <div class="template-demo">
                    <h1 class="display-2">Midagri</h1>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- content-wrapper ends -->
        <footer class="footer">
            <div class="card">
                <div class="card-body">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022 <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">| Ministerio de Agricultura y Riego</a>.</span>
                    </div>
                </div>    
            </div>        
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="<?php echo base_url(); ?>resources/vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url(); ?>resources/vendors/chart.js/Chart.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="<?php echo base_url(); ?>resources/js/off-canvas.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/hoverable-collapse.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/template.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/todolist.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/scripts.js"></script>
  <script src="<?php echo base_url(); ?>resources/js/charts-minagri.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>

</html>