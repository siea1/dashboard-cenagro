
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Uso de la tierra</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option value="">Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>
                      
                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('UsoTierra')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Destino de la Producción</h4>
                          <canvas id="pieChartR1"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Situación</h4>
                          <canvas id="doughnutChartR1"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>
               
                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th colspan="7" class="table-info"> 
                            <center>Número de Parcelas</center>
                          </th>
                          <th colspan="9" class="table-warning"> 
                            <center>Condición Jurídica del Productor</center>
                          </th>
                          <th colspan="5" class="table-success"> 
                            <center>Grupos de Edad</center>
                          </th>
                          <th colspan="5" class="table-danger"> 
                            <center>Situación</center>
                          </th>
                          <th colspan="6" class="table-primary"> 
                            <center>Destino de la Producción</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            1
                          </td>
                          <td>
                            2
                          </td>
                          <td>
                            3
                          </td>
                          <td>
                            4
                          </td>
                          <td>
                            5
                          </td>
                          <td>
                            6-10
                          </td>
                          <td>
                            > 10
                          </td>
                          <td>
                            P.NAT
                          </td>
                          <td>
                            SAC
                          </td>
                          <td>
                            SAA
                          </td>
                          <td>
                            SRL
                          </td>
                          <td>
                            EIRL
                          </td>
                          <td>
                            COOP. AG.
                          </td>
                          <td>
                            COM. CAMP.
                          </td>
                          <td>
                            COM. NAT.
                          </td>
                          <td>
                            OTRA
                          </td>
                          <td>
                            < 18
                          </td>
                          <td>
                            19-30
                          </td>
                          <td>
                            31-45
                          </td>
                          <td>
                            45-64
                          </td>
                          <td>
                            > 64
                          </td>
                          <td>
                            Grav.
                          </td>
                          <td>
                            Asp.
                          </td>
                          <td>
                            Got.
                          </td>
                          <td>
                            Exu.
                          </td>
                          <td>
                            Secano
                          </td>
                          <td>
                            M. Nac
                          </td>
                          <td>
                            M. Ext
                          </td>
                          <td>
                            Agro
                          </td>
                          <td>
                            A/Cons.
                          </td>
                          <td>
                            A/Ins.
                          </td>
                          <td>
                            Al. Anim.
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P1'] + $f0_5['NP_P1']; ?></p>
                            <?php echo $m0_5['NP_P1_S'] + $f0_5['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P2'] + $f0_5['NP_P2']; ?></p>
                            <?php echo $m0_5['NP_P2_S'] + $f0_5['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P3'] + $f0_5['NP_P3']; ?></p>
                            <?php echo $m0_5['NP_P3_S'] + $f0_5['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P4'] + $f0_5['NP_P4']; ?></p>
                            <?php echo $m0_5['NP_P4_S'] + $f0_5['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P5'] + $f0_5['NP_P5']; ?></p>
                            <?php echo $m0_5['NP_P5_S'] + $f0_5['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P610'] + $f0_5['NP_P610']; ?></p>
                            <?php echo $m0_5['NP_P610_S'] + $f0_5['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P10'] + $f0_5['NP_P10']; ?></p>
                            <?php echo $m0_5['NP_P10_S'] + $f0_5['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_NAT'] + $f0_5['CJ_NAT']; ?></p>
                            <?php echo $m0_5['CJ_NAT_S'] + $f0_5['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SAC'] + $f0_5['CJ_SAC']; ?></p>
                            <?php echo $m0_5['CJ_SAC_S'] + $f0_5['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SAA'] + $f0_5['CJ_SAA']; ?></p>
                            <?php echo $m0_5['CJ_SAA_S'] + $f0_5['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SRL'] + $f0_5['CJ_SRL']; ?></p>
                            <?php echo $m0_5['CJ_SRL_S'] + $f0_5['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_EIRL'] + $f0_5['CJ_EIRL']; ?></p>
                            <?php echo $m0_5['CJ_EIRL_S'] + $f0_5['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COOPA'] + $f0_5['CJ_COOPA']; ?></p>
                            <?php echo $m0_5['CJ_COOPA_S'] + $f0_5['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COMC'] + $f0_5['CJ_COMC']; ?></p>
                            <?php echo $m0_5['CJ_COMC_S'] + $f0_5['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COMN'] + $f0_5['CJ_COMN']; ?></p>
                            <?php echo $m0_5['CJ_COMN_S'] + $f0_5['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_OTRA'] + $f0_5['CJ_OTRA']; ?></p>
                            <?php echo $m0_5['CJ_OTRA_S'] + $f0_5['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_18'] + $f0_5['GE_18']; ?></p>
                            <?php echo $m0_5['GE_18_S'] + $f0_5['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_1930'] + $f0_5['GE_1930']; ?></p>
                            <?php echo $m0_5['GE_1930_S'] + $f0_5['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_3145'] + $f0_5['GE_3145']; ?></p>
                            <?php echo $m0_5['GE_3145_S'] + $f0_5['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_4564'] + $f0_5['GE_4564']; ?></p>
                            <?php echo $m0_5['GE_4564_S'] + $f0_5['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_65'] + $f0_5['GE_65']; ?></p>
                            <?php echo $m0_5['GE_65_S'] + $f0_5['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_GRAV'] + $f0_5['SI_GRAV']; ?></p>
                            <?php echo $m0_5['SI_GRAV_S'] + $f0_5['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_ASP'] + $f0_5['SI_ASP']; ?></p>
                            <?php echo $m0_5['SI_ASP_S'] + $f0_5['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_GOT'] + $f0_5['SI_GOT']; ?></p>
                            <?php echo $m0_5['SI_GOT_S'] + $f0_5['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_EXU'] + $f0_5['SI_EXU']; ?></p>
                            <?php echo $m0_5['SI_EXU_S'] + $f0_5['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_SECANO'] + $f0_5['SI_SECANO']; ?></p>
                            <?php echo $m0_5['SI_SECANO_S'] + $f0_5['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_MNAC'] + $f0_5['DP_MNAC']; ?></p>
                            <?php echo $m0_5['DP_MNAC_S'] + $f0_5['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_MEXT'] + $f0_5['DP_MEXT']; ?></p>
                            <?php echo $m0_5['DP_MEXT_S'] + $f0_5['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AGRO'] + $f0_5['DP_AGRO']; ?></p>
                            <?php echo $m0_5['DP_AGRO_S'] + $f0_5['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AUTOC'] + $f0_5['DP_AUTOC']; ?></p>
                            <?php echo $m0_5['DP_AUTOC_S'] + $f0_5['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AUTOI'] + $f0_5['DP_AUTOI']; ?></p>
                            <?php echo $m0_5['DP_AUTOI_S'] + $f0_5['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AANIM'] + $f0_5['DP_AANIM']; ?></p>
                            <?php echo $m0_5['DP_AANIM_S'] + $f0_5['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P1'] + $f51_10['NP_P1']; ?></p>
                            <?php echo $m51_10['NP_P1_S'] + $f51_10['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P2'] + $f51_10['NP_P2']; ?></p>
                            <?php echo $m51_10['NP_P2_S'] + $f51_10['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P3'] + $f51_10['NP_P3']; ?></p>
                            <?php echo $m51_10['NP_P3_S'] + $f51_10['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P4'] + $f51_10['NP_P4']; ?></p>
                            <?php echo $m51_10['NP_P4_S'] + $f51_10['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P5'] + $f51_10['NP_P5']; ?></p>
                            <?php echo $m51_10['NP_P5_S'] + $f51_10['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P610'] + $f51_10['NP_P610']; ?></p>
                            <?php echo $m51_10['NP_P610_S'] + $f51_10['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P10'] + $f51_10['NP_P10']; ?></p>
                            <?php echo $m51_10['NP_P10_S'] + $f51_10['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_NAT'] + $f51_10['CJ_NAT']; ?></p>
                            <?php echo $m51_10['CJ_NAT_S'] + $f51_10['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SAC'] + $f51_10['CJ_SAC']; ?></p>
                            <?php echo $m51_10['CJ_SAC_S'] + $f51_10['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SAA'] + $f51_10['CJ_SAA']; ?></p>
                            <?php echo $m51_10['CJ_SAA_S'] + $f51_10['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SRL'] + $f51_10['CJ_SRL']; ?></p>
                            <?php echo $m51_10['CJ_SRL_S'] + $f51_10['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_EIRL'] + $f51_10['CJ_EIRL']; ?></p>
                            <?php echo $m51_10['CJ_EIRL_S'] + $f51_10['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COOPA'] + $f51_10['CJ_COOPA']; ?></p>
                            <?php echo $m51_10['CJ_COOPA_S'] + $f51_10['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COMC'] + $f51_10['CJ_COMC']; ?></p>
                            <?php echo $m51_10['CJ_COMC_S'] + $f51_10['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COMN'] + $f51_10['CJ_COMN']; ?></p>
                            <?php echo $m51_10['CJ_COMN_S'] + $f51_10['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_OTRA'] + $f51_10['CJ_OTRA']; ?></p>
                            <?php echo $m51_10['CJ_OTRA_S'] + $f51_10['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_18'] + $f51_10['GE_18']; ?></p>
                            <?php echo $m51_10['GE_18_S'] + $f51_10['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_1930'] + $f51_10['GE_1930']; ?></p>
                            <?php echo $m51_10['GE_1930_S'] + $f51_10['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_3145'] + $f51_10['GE_3145']; ?></p>
                            <?php echo $m51_10['GE_3145_S'] + $f51_10['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_4564'] + $f51_10['GE_4564']; ?></p>
                            <?php echo $m51_10['GE_4564_S'] + $f51_10['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_65'] + $f51_10['GE_65']; ?></p>
                            <?php echo $m51_10['GE_65_S'] + $f51_10['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_GRAV'] + $f51_10['SI_GRAV']; ?></p>
                            <?php echo $m51_10['SI_GRAV_S'] + $f51_10['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_ASP'] + $f51_10['SI_ASP']; ?></p>
                            <?php echo $m51_10['SI_ASP_S'] + $f51_10['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_GOT'] + $f51_10['SI_GOT']; ?></p>
                            <?php echo $m51_10['SI_GOT_S'] + $f51_10['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_EXU'] + $f51_10['SI_EXU']; ?></p>
                            <?php echo $m51_10['SI_EXU_S'] + $f51_10['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_SECANO'] + $f51_10['SI_SECANO']; ?></p>
                            <?php echo $m51_10['SI_SECANO_S'] + $f51_10['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_MNAC'] + $f51_10['DP_MNAC']; ?></p>
                            <?php echo $m51_10['DP_MNAC_S'] + $f51_10['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_MEXT'] + $f51_10['DP_MEXT']; ?></p>
                            <?php echo $m51_10['DP_MEXT_S'] + $f51_10['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AGRO'] + $f51_10['DP_AGRO']; ?></p>
                            <?php echo $m51_10['DP_AGRO_S'] + $f51_10['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AUTOC'] + $f51_10['DP_AUTOC']; ?></p>
                            <?php echo $m51_10['DP_AUTOC_S'] + $f51_10['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AUTOI'] + $f51_10['DP_AUTOI']; ?></p>
                            <?php echo $m51_10['DP_AUTOI_S'] + $f51_10['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AANIM'] + $f51_10['DP_AANIM']; ?></p>
                            <?php echo $m51_10['DP_AANIM_S'] + $f51_10['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P1'] + $f101_20['NP_P1']; ?></p>
                            <?php echo $m101_20['NP_P1_S'] + $f101_20['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P2'] + $f101_20['NP_P2']; ?></p>
                            <?php echo $m101_20['NP_P2_S'] + $f101_20['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P3'] + $f101_20['NP_P3']; ?></p>
                            <?php echo $m101_20['NP_P3_S'] + $f101_20['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P4'] + $f101_20['NP_P4']; ?></p>
                            <?php echo $m101_20['NP_P4_S'] + $f101_20['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P5'] + $f101_20['NP_P5']; ?></p>
                            <?php echo $m101_20['NP_P5_S'] + $f101_20['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P610'] + $f101_20['NP_P610']; ?></p>
                            <?php echo $m101_20['NP_P610_S'] + $f101_20['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P10'] + $f101_20['NP_P10']; ?></p>
                            <?php echo $m101_20['NP_P10_S'] + $f101_20['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_NAT'] + $f101_20['CJ_NAT']; ?></p>
                            <?php echo $m101_20['CJ_NAT_S'] + $f101_20['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SAC'] + $f101_20['CJ_SAC']; ?></p>
                            <?php echo $m101_20['CJ_SAC_S'] + $f101_20['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SAA'] + $f101_20['CJ_SAA']; ?></p>
                            <?php echo $m101_20['CJ_SAA_S'] + $f101_20['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SRL'] + $f101_20['CJ_SRL']; ?></p>
                            <?php echo $m101_20['CJ_SRL_S'] + $f101_20['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_EIRL'] + $f101_20['CJ_EIRL']; ?></p>
                            <?php echo $m101_20['CJ_EIRL_S'] + $f101_20['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COOPA'] + $f101_20['CJ_COOPA']; ?></p>
                            <?php echo $m101_20['CJ_COOPA_S'] + $f101_20['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COMC'] + $f101_20['CJ_COMC']; ?></p>
                            <?php echo $m101_20['CJ_COMC_S'] + $f101_20['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COMN'] + $f101_20['CJ_COMN']; ?></p>
                            <?php echo $m101_20['CJ_COMN_S'] + $f101_20['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_OTRA'] + $f101_20['CJ_OTRA']; ?></p>
                            <?php echo $m101_20['CJ_OTRA_S'] + $f101_20['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_18'] + $f101_20['GE_18']; ?></p>
                            <?php echo $m101_20['GE_18_S'] + $f101_20['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_1930'] + $f101_20['GE_1930']; ?></p>
                            <?php echo $m101_20['GE_1930_S'] + $f101_20['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_3145'] + $f101_20['GE_3145']; ?></p>
                            <?php echo $m101_20['GE_3145_S'] + $f101_20['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_4564'] + $f101_20['GE_4564']; ?></p>
                            <?php echo $m101_20['GE_4564_S'] + $f101_20['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_65'] + $f101_20['GE_65']; ?></p>
                            <?php echo $m101_20['GE_65_S'] + $f101_20['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_GRAV'] + $f101_20['SI_GRAV']; ?></p>
                            <?php echo $m101_20['SI_GRAV_S'] + $f101_20['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_ASP'] + $f101_20['SI_ASP']; ?></p>
                            <?php echo $m101_20['SI_ASP_S'] + $f101_20['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_GOT'] + $f101_20['SI_GOT']; ?></p>
                            <?php echo $m101_20['SI_GOT_S'] + $f101_20['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_EXU'] + $f101_20['SI_EXU']; ?></p>
                            <?php echo $m101_20['SI_EXU_S'] + $f101_20['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_SECANO'] + $f101_20['SI_SECANO']; ?></p>
                            <?php echo $m101_20['SI_SECANO_S'] + $f101_20['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_MNAC'] + $f101_20['DP_MNAC']; ?></p>
                            <?php echo $m101_20['DP_MNAC_S'] + $f101_20['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_MEXT'] + $f101_20['DP_MEXT']; ?></p>
                            <?php echo $m101_20['DP_MEXT_S'] + $f101_20['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AGRO'] + $f101_20['DP_AGRO']; ?></p>
                            <?php echo $m101_20['DP_AGRO_S'] + $f101_20['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AUTOC'] + $f101_20['DP_AUTOC']; ?></p>
                            <?php echo $m101_20['DP_AUTOC_S'] + $f101_20['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AUTOI'] + $f101_20['DP_AUTOI']; ?></p>
                            <?php echo $m101_20['DP_AUTOI_S'] + $f101_20['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AANIM'] + $f101_20['DP_AANIM']; ?></p>
                            <?php echo $m101_20['DP_AANIM_S'] + $f101_20['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P1'] + $f201_50['NP_P1']; ?></p>
                            <?php echo $m201_50['NP_P1_S'] + $f201_50['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P2'] + $f201_50['NP_P2']; ?></p>
                            <?php echo $m201_50['NP_P2_S'] + $f201_50['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P3'] + $f201_50['NP_P3']; ?></p>
                            <?php echo $m201_50['NP_P3_S'] + $f201_50['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P4'] + $f201_50['NP_P4']; ?></p>
                            <?php echo $m201_50['NP_P4_S'] + $f201_50['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P5'] + $f201_50['NP_P5']; ?></p>
                            <?php echo $m201_50['NP_P5_S'] + $f201_50['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P610'] + $f201_50['NP_P610']; ?></p>
                            <?php echo $m201_50['NP_P610_S'] + $f201_50['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P10'] + $f201_50['NP_P10']; ?></p>
                            <?php echo $m201_50['NP_P10_S'] + $f201_50['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_NAT'] + $f201_50['CJ_NAT']; ?></p>
                            <?php echo $m201_50['CJ_NAT_S'] + $f201_50['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SAC'] + $f201_50['CJ_SAC']; ?></p>
                            <?php echo $m201_50['CJ_SAC_S'] + $f201_50['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SAA'] + $f201_50['CJ_SAA']; ?></p>
                            <?php echo $m201_50['CJ_SAA_S'] + $f201_50['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SRL'] + $f201_50['CJ_SRL']; ?></p>
                            <?php echo $m201_50['CJ_SRL_S'] + $f201_50['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_EIRL'] + $f201_50['CJ_EIRL']; ?></p>
                            <?php echo $m201_50['CJ_EIRL_S'] + $f201_50['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COOPA'] + $f201_50['CJ_COOPA']; ?></p>
                            <?php echo $m201_50['CJ_COOPA_S'] + $f201_50['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COMC'] + $f201_50['CJ_COMC']; ?></p>
                            <?php echo $m201_50['CJ_COMC_S'] + $f201_50['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COMN'] + $f201_50['CJ_COMN']; ?></p>
                            <?php echo $m201_50['CJ_COMN_S'] + $f201_50['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_OTRA'] + $f201_50['CJ_OTRA']; ?></p>
                            <?php echo $m201_50['CJ_OTRA_S'] + $f201_50['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_18'] + $f201_50['GE_18']; ?></p>
                            <?php echo $m201_50['GE_18_S'] + $f201_50['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_1930'] + $f201_50['GE_1930']; ?></p>
                            <?php echo $m201_50['GE_1930_S'] + $f201_50['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_3145'] + $f201_50['GE_3145']; ?></p>
                            <?php echo $m201_50['GE_3145_S'] + $f201_50['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_4564'] + $f201_50['GE_4564']; ?></p>
                            <?php echo $m201_50['GE_4564_S'] + $f201_50['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_65'] + $f201_50['GE_65']; ?></p>
                            <?php echo $m201_50['GE_65_S'] + $f201_50['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_GRAV'] + $f201_50['SI_GRAV']; ?></p>
                            <?php echo $m201_50['SI_GRAV_S'] + $f201_50['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_ASP'] + $f201_50['SI_ASP']; ?></p>
                            <?php echo $m201_50['SI_ASP_S'] + $f201_50['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_GOT'] + $f201_50['SI_GOT']; ?></p>
                            <?php echo $m201_50['SI_GOT_S'] + $f201_50['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_EXU'] + $f201_50['SI_EXU']; ?></p>
                            <?php echo $m201_50['SI_EXU_S'] + $f201_50['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_SECANO'] + $f201_50['SI_SECANO']; ?></p>
                            <?php echo $m201_50['SI_SECANO_S'] + $f201_50['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_MNAC'] + $f201_50['DP_MNAC']; ?></p>
                            <?php echo $m201_50['DP_MNAC_S'] + $f201_50['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_MEXT'] + $f201_50['DP_MEXT']; ?></p>
                            <?php echo $m201_50['DP_MEXT_S'] + $f201_50['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AGRO'] + $f201_50['DP_AGRO']; ?></p>
                            <?php echo $m201_50['DP_AGRO_S'] + $f201_50['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AUTOC'] + $f201_50['DP_AUTOC']; ?></p>
                            <?php echo $m201_50['DP_AUTOC_S'] + $f201_50['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AUTOI'] + $f201_50['DP_AUTOI']; ?></p>
                            <?php echo $m201_50['DP_AUTOI_S'] + $f201_50['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AANIM'] + $f201_50['DP_AANIM']; ?></p>
                            <?php echo $m201_50['DP_AANIM_S'] + $f201_50['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P1'] + $f501_100['NP_P1']; ?></p>
                            <?php echo $m501_100['NP_P1_S'] + $f501_100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P2'] + $f501_100['NP_P2']; ?></p>
                            <?php echo $m501_100['NP_P2_S'] + $f501_100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P3'] + $f501_100['NP_P3']; ?></p>
                            <?php echo $m501_100['NP_P3_S'] + $f501_100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P4'] + $f501_100['NP_P4']; ?></p>
                            <?php echo $m501_100['NP_P4_S'] + $f501_100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P5'] + $f501_100['NP_P5']; ?></p>
                            <?php echo $m501_100['NP_P5_S'] + $f501_100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P610'] + $f501_100['NP_P610']; ?></p>
                            <?php echo $m501_100['NP_P610_S'] + $f501_100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P10'] + $f501_100['NP_P10']; ?></p>
                            <?php echo $m501_100['NP_P10_S'] + $f501_100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_NAT'] + $f501_100['CJ_NAT']; ?></p>
                            <?php echo $m501_100['CJ_NAT_S'] + $f501_100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SAC'] + $f501_100['CJ_SAC']; ?></p>
                            <?php echo $m501_100['CJ_SAC_S'] + $f501_100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SAA'] + $f501_100['CJ_SAA']; ?></p>
                            <?php echo $m501_100['CJ_SAA_S'] + $f501_100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SRL'] + $f501_100['CJ_SRL']; ?></p>
                            <?php echo $m501_100['CJ_SRL_S'] + $f501_100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_EIRL'] + $f501_100['CJ_EIRL']; ?></p>
                            <?php echo $m501_100['CJ_EIRL_S'] + $f501_100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COOPA'] + $f501_100['CJ_COOPA']; ?></p>
                            <?php echo $m501_100['CJ_COOPA_S'] + $f501_100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COMC'] + $f501_100['CJ_COMC']; ?></p>
                            <?php echo $m501_100['CJ_COMC_S'] + $f501_100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COMN'] + $f501_100['CJ_COMN']; ?></p>
                            <?php echo $m501_100['CJ_COMN_S'] + $f501_100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_OTRA'] + $f501_100['CJ_OTRA']; ?></p>
                            <?php echo $m501_100['CJ_OTRA_S'] + $f501_100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_18'] + $f501_100['GE_18']; ?></p>
                            <?php echo $m501_100['GE_18_S'] + $f501_100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_1930'] + $f501_100['GE_1930']; ?></p>
                            <?php echo $m501_100['GE_1930_S'] + $f501_100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_3145'] + $f501_100['GE_3145']; ?></p>
                            <?php echo $m501_100['GE_3145_S'] + $f501_100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_4564'] + $f501_100['GE_4564']; ?></p>
                            <?php echo $m501_100['GE_4564_S'] + $f501_100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_65'] + $f501_100['GE_65']; ?></p>
                            <?php echo $m501_100['GE_65_S'] + $f501_100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_GRAV'] + $f501_100['SI_GRAV']; ?></p>
                            <?php echo $m501_100['SI_GRAV_S'] + $f501_100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_ASP'] + $f501_100['SI_ASP']; ?></p>
                            <?php echo $m501_100['SI_ASP_S'] + $f501_100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_GOT'] + $f501_100['SI_GOT']; ?></p>
                            <?php echo $m501_100['SI_GOT_S'] + $f501_100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_EXU'] + $f501_100['SI_EXU']; ?></p>
                            <?php echo $m501_100['SI_EXU_S'] + $f501_100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_SECANO'] + $f501_100['SI_SECANO']; ?></p>
                            <?php echo $m501_100['SI_SECANO_S'] + $f501_100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_MNAC'] + $f501_100['DP_MNAC']; ?></p>
                            <?php echo $m501_100['DP_MNAC_S'] + $f501_100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_MEXT'] + $f501_100['DP_MEXT']; ?></p>
                            <?php echo $m501_100['DP_MEXT_S'] + $f501_100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AGRO'] + $f501_100['DP_AGRO']; ?></p>
                            <?php echo $m501_100['DP_AGRO_S'] + $f501_100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AUTOC'] + $f501_100['DP_AUTOC']; ?></p>
                            <?php echo $m501_100['DP_AUTOC_S'] + $f501_100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AUTOI'] + $f501_100['DP_AUTOI']; ?></p>
                            <?php echo $m501_100['DP_AUTOI_S'] + $f501_100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AANIM'] + $f501_100['DP_AANIM']; ?></p>
                            <?php echo $m501_100['DP_AANIM_S'] + $f501_100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P1'] + $f100['NP_P1']; ?></p>
                            <?php echo $m100['NP_P1_S'] + $f100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P2'] + $f100['NP_P2']; ?></p>
                            <?php echo $m100['NP_P2_S'] + $f100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P3'] + $f100['NP_P3']; ?></p>
                            <?php echo $m100['NP_P3_S'] + $f100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P4'] + $f100['NP_P4']; ?></p>
                            <?php echo $m100['NP_P4_S'] + $f100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P5'] + $f100['NP_P5']; ?></p>
                            <?php echo $m100['NP_P5_S'] + $f100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P610'] + $f100['NP_P610']; ?></p>
                            <?php echo $m100['NP_P610_S'] + $f100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P10'] + $f100['NP_P10']; ?></p>
                            <?php echo $m100['NP_P10_S'] + $f100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_NAT'] + $f100['CJ_NAT']; ?></p>
                            <?php echo $m100['CJ_NAT_S'] + $f100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SAC'] + $f100['CJ_SAC']; ?></p>
                            <?php echo $m100['CJ_SAC_S'] + $f100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SAA'] + $f100['CJ_SAA']; ?></p>
                            <?php echo $m100['CJ_SAA_S'] + $f100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SRL'] + $f100['CJ_SRL']; ?></p>
                            <?php echo $m100['CJ_SRL_S'] + $f100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_EIRL'] + $f100['CJ_EIRL']; ?></p>
                            <?php echo $m100['CJ_EIRL_S'] + $f100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COOPA'] + $f100['CJ_COOPA']; ?></p>
                            <?php echo $m100['CJ_COOPA_S'] + $f100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COMC'] + $f100['CJ_COMC']; ?></p>
                            <?php echo $m100['CJ_COMC_S'] + $f100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COMN'] + $f100['CJ_COMN']; ?></p>
                            <?php echo $m100['CJ_COMN_S'] + $f100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_OTRA'] + $f100['CJ_OTRA']; ?></p>
                            <?php echo $m100['CJ_OTRA_S'] + $f100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_18'] + $f100['GE_18']; ?></p>
                            <?php echo $m100['GE_18_S'] + $f100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_1930'] + $f100['GE_1930']; ?></p>
                            <?php echo $m100['GE_1930_S'] + $f100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_3145'] + $f100['GE_3145']; ?></p>
                            <?php echo $m100['GE_3145_S'] + $f100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_4564'] + $f100['GE_4564']; ?></p>
                            <?php echo $m100['GE_4564_S'] + $f100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_65'] + $f100['GE_65']; ?></p>
                            <?php echo $m100['GE_65_S'] + $f100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_GRAV'] + $f100['SI_GRAV']; ?></p>
                            <?php echo $m100['SI_GRAV_S'] + $f100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_ASP'] + $f100['SI_ASP']; ?></p>
                            <?php echo $m100['SI_ASP_S'] + $f100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_GOT'] + $f100['SI_GOT']; ?></p>
                            <?php echo $m100['SI_GOT_S'] + $f100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_EXU'] + $f100['SI_EXU']; ?></p>
                            <?php echo $m100['SI_EXU_S'] + $f100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_SECANO'] + $f100['SI_SECANO']; ?></p>
                            <?php echo $m100['SI_SECANO_S'] + $f100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_MNAC'] + $f100['DP_MNAC']; ?></p>
                            <?php echo $m100['DP_MNAC_S'] + $f100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_MEXT'] + $f100['DP_MEXT']; ?></p>
                            <?php echo $m100['DP_MEXT_S'] + $f100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AGRO'] + $f100['DP_AGRO']; ?></p>
                            <?php echo $m100['DP_AGRO_S'] + $f100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AUTOC'] + $f100['DP_AUTOC']; ?></p>
                            <?php echo $m100['DP_AUTOC_S'] + $f100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AUTOI'] + $f100['DP_AUTOI']; ?></p>
                            <?php echo $m100['DP_AUTOI_S'] + $f100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AANIM'] + $f100['DP_AANIM']; ?></p>
                            <?php echo $m100['DP_AANIM_S'] + $f100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P1']; ?></p>
                            <?php echo $m0_5['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P2']; ?></p>
                            <?php echo $m0_5['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P3']; ?></p>
                            <?php echo $m0_5['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P4']; ?></p>
                            <?php echo $m0_5['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P5']; ?></p>
                            <?php echo $m0_5['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P610']; ?></p>
                            <?php echo $m0_5['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['NP_P10']; ?></p>
                            <?php echo $m0_5['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_NAT']; ?></p>
                            <?php echo $m0_5['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SAC']; ?></p>
                            <?php echo $m0_5['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SAA']; ?></p>
                            <?php echo $m0_5['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_SRL']; ?></p>
                            <?php echo $m0_5['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_EIRL']; ?></p>
                            <?php echo $m0_5['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COOPA']; ?></p>
                            <?php echo $m0_5['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COMC']; ?></p>
                            <?php echo $m0_5['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_COMN']; ?></p>
                            <?php echo $m0_5['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['CJ_OTRA']; ?></p>
                            <?php echo $m0_5['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_18'];  ?></p>
                            <?php echo $m0_5['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_1930']; ?></p>
                            <?php echo $m0_5['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_3145']; ?></p>
                            <?php echo $m0_5['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_4564']; ?></p>
                            <?php echo $m0_5['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['GE_65']; ?></p>
                            <?php echo $m0_5['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_GRAV']; ?></p>
                            <?php echo $m0_5['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_ASP']; ?></p>
                            <?php echo $m0_5['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_GOT']; ?></p>
                            <?php echo $m0_5['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_EXU']; ?></p>
                            <?php echo $m0_5['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['SI_SECANO']; ?></p>
                            <?php echo $m0_5['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_MNAC']; ?></p>
                            <?php echo $m0_5['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_MEXT']; ?></p>
                            <?php echo $m0_5['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AGRO']; ?></p>
                            <?php echo $m0_5['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AUTOC']; ?></p>
                            <?php echo $m0_5['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AUTOI']; ?></p>
                            <?php echo $m0_5['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['DP_AANIM']; ?></p>
                            <?php echo $m0_5['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P1']; ?></p>
                            <?php echo $m51_10['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P2']; ?></p>
                            <?php echo $m51_10['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P3']; ?></p>
                            <?php echo $m51_10['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P4']; ?></p>
                            <?php echo $m51_10['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P5']; ?></p>
                            <?php echo $m51_10['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P610']; ?></p>
                            <?php echo $m51_10['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['NP_P10']; ?></p>
                            <?php echo $m51_10['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_NAT']; ?></p>
                            <?php echo $m51_10['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SAC']; ?></p>
                            <?php echo $m51_10['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SAA']; ?></p>
                            <?php echo $m51_10['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_SRL']; ?></p>
                            <?php echo $m51_10['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_EIRL']; ?></p>
                            <?php echo $m51_10['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COOPA']; ?></p>
                            <?php echo $m51_10['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COMC']; ?></p>
                            <?php echo $m51_10['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_COMN']; ?></p>
                            <?php echo $m51_10['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['CJ_OTRA']; ?></p>
                            <?php echo $m51_10['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_18'];  ?></p>
                            <?php echo $m51_10['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_1930']; ?></p>
                            <?php echo $m51_10['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_3145']; ?></p>
                            <?php echo $m51_10['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_4564']; ?></p>
                            <?php echo $m51_10['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['GE_65']; ?></p>
                            <?php echo $m51_10['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_GRAV']; ?></p>
                            <?php echo $m51_10['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_ASP']; ?></p>
                            <?php echo $m51_10['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_GOT']; ?></p>
                            <?php echo $m51_10['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_EXU']; ?></p>
                            <?php echo $m51_10['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['SI_SECANO']; ?></p>
                            <?php echo $m51_10['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_MNAC']; ?></p>
                            <?php echo $m51_10['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_MEXT']; ?></p>
                            <?php echo $m51_10['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AGRO']; ?></p>
                            <?php echo $m51_10['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AUTOC']; ?></p>
                            <?php echo $m51_10['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AUTOI']; ?></p>
                            <?php echo $m51_10['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['DP_AANIM']; ?></p>
                            <?php echo $m51_10['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P1']; ?></p>
                            <?php echo $m101_20['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P2']; ?></p>
                            <?php echo $m101_20['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P3']; ?></p>
                            <?php echo $m101_20['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P4']; ?></p>
                            <?php echo $m101_20['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P5']; ?></p>
                            <?php echo $m101_20['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P610']; ?></p>
                            <?php echo $m101_20['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['NP_P10']; ?></p>
                            <?php echo $m101_20['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_NAT']; ?></p>
                            <?php echo $m101_20['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SAC']; ?></p>
                            <?php echo $m101_20['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SAA']; ?></p>
                            <?php echo $m101_20['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_SRL']; ?></p>
                            <?php echo $m101_20['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_EIRL']; ?></p>
                            <?php echo $m101_20['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COOPA']; ?></p>
                            <?php echo $m101_20['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COMC']; ?></p>
                            <?php echo $m101_20['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_COMN']; ?></p>
                            <?php echo $m101_20['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['CJ_OTRA']; ?></p>
                            <?php echo $m101_20['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_18'];  ?></p>
                            <?php echo $m101_20['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_1930']; ?></p>
                            <?php echo $m101_20['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_3145']; ?></p>
                            <?php echo $m101_20['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_4564']; ?></p>
                            <?php echo $m101_20['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['GE_65']; ?></p>
                            <?php echo $m101_20['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_GRAV']; ?></p>
                            <?php echo $m101_20['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_ASP']; ?></p>
                            <?php echo $m101_20['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_GOT']; ?></p>
                            <?php echo $m101_20['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_EXU']; ?></p>
                            <?php echo $m101_20['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['SI_SECANO']; ?></p>
                            <?php echo $m101_20['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_MNAC']; ?></p>
                            <?php echo $m101_20['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_MEXT']; ?></p>
                            <?php echo $m101_20['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AGRO']; ?></p>
                            <?php echo $m101_20['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AUTOC']; ?></p>
                            <?php echo $m101_20['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AUTOI']; ?></p>
                            <?php echo $m101_20['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['DP_AANIM']; ?></p>
                            <?php echo $m101_20['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P1']; ?></p>
                            <?php echo $m201_50['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P2']; ?></p>
                            <?php echo $m201_50['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P3']; ?></p>
                            <?php echo $m201_50['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P4']; ?></p>
                            <?php echo $m201_50['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P5']; ?></p>
                            <?php echo $m201_50['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P610']; ?></p>
                            <?php echo $m201_50['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['NP_P10']; ?></p>
                            <?php echo $m201_50['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_NAT']; ?></p>
                            <?php echo $m201_50['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SAC']; ?></p>
                            <?php echo $m201_50['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SAA']; ?></p>
                            <?php echo $m201_50['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_SRL']; ?></p>
                            <?php echo $m201_50['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_EIRL']; ?></p>
                            <?php echo $m201_50['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COOPA']; ?></p>
                            <?php echo $m201_50['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COMC']; ?></p>
                            <?php echo $m201_50['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_COMN']; ?></p>
                            <?php echo $m201_50['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['CJ_OTRA']; ?></p>
                            <?php echo $m201_50['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_18'];  ?></p>
                            <?php echo $m201_50['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_1930']; ?></p>
                            <?php echo $m201_50['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_3145']; ?></p>
                            <?php echo $m201_50['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_4564']; ?></p>
                            <?php echo $m201_50['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['GE_65']; ?></p>
                            <?php echo $m201_50['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_GRAV']; ?></p>
                            <?php echo $m201_50['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_ASP']; ?></p>
                            <?php echo $m201_50['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_GOT']; ?></p>
                            <?php echo $m201_50['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_EXU']; ?></p>
                            <?php echo $m201_50['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['SI_SECANO']; ?></p>
                            <?php echo $m201_50['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_MNAC']; ?></p>
                            <?php echo $m201_50['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_MEXT']; ?></p>
                            <?php echo $m201_50['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AGRO']; ?></p>
                            <?php echo $m201_50['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AUTOC']; ?></p>
                            <?php echo $m201_50['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AUTOI']; ?></p>
                            <?php echo $m201_50['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['DP_AANIM']; ?></p>
                            <?php echo $m201_50['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P1']; ?></p>
                            <?php echo $m501_100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P2']; ?></p>
                            <?php echo $m501_100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P3']; ?></p>
                            <?php echo $m501_100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P4']; ?></p>
                            <?php echo $m501_100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P5']; ?></p>
                            <?php echo $m501_100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P610']; ?></p>
                            <?php echo $m501_100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['NP_P10']; ?></p>
                            <?php echo $m501_100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_NAT']; ?></p>
                            <?php echo $m501_100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SAC']; ?></p>
                            <?php echo $m501_100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SAA']; ?></p>
                            <?php echo $m501_100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_SRL']; ?></p>
                            <?php echo $m501_100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_EIRL']; ?></p>
                            <?php echo $m501_100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COOPA']; ?></p>
                            <?php echo $m501_100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COMC']; ?></p>
                            <?php echo $m501_100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_COMN']; ?></p>
                            <?php echo $m501_100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['CJ_OTRA']; ?></p>
                            <?php echo $m501_100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_18'];  ?></p>
                            <?php echo $m501_100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_1930']; ?></p>
                            <?php echo $m501_100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_3145']; ?></p>
                            <?php echo $m501_100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_4564']; ?></p>
                            <?php echo $m501_100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['GE_65']; ?></p>
                            <?php echo $m501_100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_GRAV']; ?></p>
                            <?php echo $m501_100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_ASP']; ?></p>
                            <?php echo $m501_100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_GOT']; ?></p>
                            <?php echo $m501_100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_EXU']; ?></p>
                            <?php echo $m501_100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['SI_SECANO']; ?></p>
                            <?php echo $m501_100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_MNAC']; ?></p>
                            <?php echo $m501_100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_MEXT']; ?></p>
                            <?php echo $m501_100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AGRO']; ?></p>
                            <?php echo $m501_100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AUTOC']; ?></p>
                            <?php echo $m501_100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AUTOI']; ?></p>
                            <?php echo $m501_100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['DP_AANIM']; ?></p>
                            <?php echo $m501_100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P1']; ?></p>
                            <?php echo $m100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P2']; ?></p>
                            <?php echo $m100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P3']; ?></p>
                            <?php echo $m100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P4']; ?></p>
                            <?php echo $m100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P5']; ?></p>
                            <?php echo $m100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P610']; ?></p>
                            <?php echo $m100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['NP_P10']; ?></p>
                            <?php echo $m100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_NAT']; ?></p>
                            <?php echo $m100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SAC']; ?></p>
                            <?php echo $m100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SAA']; ?></p>
                            <?php echo $m100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_SRL']; ?></p>
                            <?php echo $m100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_EIRL']; ?></p>
                            <?php echo $m100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COOPA']; ?></p>
                            <?php echo $m100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COMC']; ?></p>
                            <?php echo $m100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_COMN']; ?></p>
                            <?php echo $m100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['CJ_OTRA']; ?></p>
                            <?php echo $m100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_18'];  ?></p>
                            <?php echo $m100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_1930']; ?></p>
                            <?php echo $m100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_3145']; ?></p>
                            <?php echo $m100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_4564']; ?></p>
                            <?php echo $m100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['GE_65']; ?></p>
                            <?php echo $m100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_GRAV']; ?></p>
                            <?php echo $m100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_ASP']; ?></p>
                            <?php echo $m100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_GOT']; ?></p>
                            <?php echo $m100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_EXU']; ?></p>
                            <?php echo $m100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['SI_SECANO']; ?></p>
                            <?php echo $m100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_MNAC']; ?></p>
                            <?php echo $m100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_MEXT']; ?></p>
                            <?php echo $m100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AGRO']; ?></p>
                            <?php echo $m100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AUTOC']; ?></p>
                            <?php echo $m100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AUTOI']; ?></p>
                            <?php echo $m100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['DP_AANIM']; ?></p>
                            <?php echo $m100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P1']; ?></p>
                            <?php echo $f0_5['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P2']; ?></p>
                            <?php echo $f0_5['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P3']; ?></p>
                            <?php echo $f0_5['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P4']; ?></p>
                            <?php echo $f0_5['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P5']; ?></p>
                            <?php echo $f0_5['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P610']; ?></p>
                            <?php echo $f0_5['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['NP_P10']; ?></p>
                            <?php echo $f0_5['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_NAT']; ?></p>
                            <?php echo $f0_5['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_SAC']; ?></p>
                            <?php echo $f0_5['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_SAA']; ?></p>
                            <?php echo $f0_5['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_SRL']; ?></p>
                            <?php echo $f0_5['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_EIRL']; ?></p>
                            <?php echo $f0_5['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_COOPA']; ?></p>
                            <?php echo $f0_5['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_COMC']; ?></p>
                            <?php echo $f0_5['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_COMN']; ?></p>
                            <?php echo $f0_5['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['CJ_OTRA']; ?></p>
                            <?php echo $f0_5['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GE_18'];  ?></p>
                            <?php echo $f0_5['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GE_1930']; ?></p>
                            <?php echo $f0_5['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GE_3145']; ?></p>
                            <?php echo $f0_5['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GE_4564']; ?></p>
                            <?php echo $f0_5['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['GE_65']; ?></p>
                            <?php echo $f0_5['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['SI_GRAV']; ?></p>
                            <?php echo $f0_5['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['SI_ASP']; ?></p>
                            <?php echo $f0_5['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['SI_GOT']; ?></p>
                            <?php echo $f0_5['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['SI_EXU']; ?></p>
                            <?php echo $f0_5['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['SI_SECANO']; ?></p>
                            <?php echo $f0_5['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_MNAC']; ?></p>
                            <?php echo $f0_5['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_MEXT']; ?></p>
                            <?php echo $f0_5['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_AGRO']; ?></p>
                            <?php echo $f0_5['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_AUTOC']; ?></p>
                            <?php echo $f0_5['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_AUTOI']; ?></p>
                            <?php echo $f0_5['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['DP_AANIM']; ?></p>
                            <?php echo $f0_5['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P1']; ?></p>
                            <?php echo $f51_10['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P2']; ?></p>
                            <?php echo $f51_10['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P3']; ?></p>
                            <?php echo $f51_10['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P4']; ?></p>
                            <?php echo $f51_10['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P5']; ?></p>
                            <?php echo $f51_10['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P610']; ?></p>
                            <?php echo $f51_10['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['NP_P10']; ?></p>
                            <?php echo $f51_10['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_NAT']; ?></p>
                            <?php echo $f51_10['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_SAC']; ?></p>
                            <?php echo $f51_10['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_SAA']; ?></p>
                            <?php echo $f51_10['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_SRL']; ?></p>
                            <?php echo $f51_10['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_EIRL']; ?></p>
                            <?php echo $f51_10['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_COOPA']; ?></p>
                            <?php echo $f51_10['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_COMC']; ?></p>
                            <?php echo $f51_10['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_COMN']; ?></p>
                            <?php echo $f51_10['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['CJ_OTRA']; ?></p>
                            <?php echo $f51_10['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GE_18'];  ?></p>
                            <?php echo $f51_10['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GE_1930']; ?></p>
                            <?php echo $f51_10['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GE_3145']; ?></p>
                            <?php echo $f51_10['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GE_4564']; ?></p>
                            <?php echo $f51_10['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['GE_65']; ?></p>
                            <?php echo $f51_10['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['SI_GRAV']; ?></p>
                            <?php echo $f51_10['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['SI_ASP']; ?></p>
                            <?php echo $f51_10['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['SI_GOT']; ?></p>
                            <?php echo $f51_10['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['SI_EXU']; ?></p>
                            <?php echo $f51_10['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['SI_SECANO']; ?></p>
                            <?php echo $f51_10['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_MNAC']; ?></p>
                            <?php echo $f51_10['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_MEXT']; ?></p>
                            <?php echo $f51_10['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_AGRO']; ?></p>
                            <?php echo $f51_10['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_AUTOC']; ?></p>
                            <?php echo $f51_10['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_AUTOI']; ?></p>
                            <?php echo $f51_10['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['DP_AANIM']; ?></p>
                            <?php echo $f51_10['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P1']; ?></p>
                            <?php echo $f101_20['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P2']; ?></p>
                            <?php echo $f101_20['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P3']; ?></p>
                            <?php echo $f101_20['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P4']; ?></p>
                            <?php echo $f101_20['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P5']; ?></p>
                            <?php echo $f101_20['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P610']; ?></p>
                            <?php echo $f101_20['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['NP_P10']; ?></p>
                            <?php echo $f101_20['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_NAT']; ?></p>
                            <?php echo $f101_20['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_SAC']; ?></p>
                            <?php echo $f101_20['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_SAA']; ?></p>
                            <?php echo $f101_20['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_SRL']; ?></p>
                            <?php echo $f101_20['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_EIRL']; ?></p>
                            <?php echo $f101_20['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_COOPA']; ?></p>
                            <?php echo $f101_20['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_COMC']; ?></p>
                            <?php echo $f101_20['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_COMN']; ?></p>
                            <?php echo $f101_20['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['CJ_OTRA']; ?></p>
                            <?php echo $f101_20['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GE_18'];  ?></p>
                            <?php echo $f101_20['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GE_1930']; ?></p>
                            <?php echo $f101_20['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GE_3145']; ?></p>
                            <?php echo $f101_20['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GE_4564']; ?></p>
                            <?php echo $f101_20['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['GE_65']; ?></p>
                            <?php echo $f101_20['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['SI_GRAV']; ?></p>
                            <?php echo $f101_20['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['SI_ASP']; ?></p>
                            <?php echo $f101_20['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['SI_GOT']; ?></p>
                            <?php echo $f101_20['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['SI_EXU']; ?></p>
                            <?php echo $f101_20['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['SI_SECANO']; ?></p>
                            <?php echo $f101_20['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_MNAC']; ?></p>
                            <?php echo $f101_20['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_MEXT']; ?></p>
                            <?php echo $f101_20['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_AGRO']; ?></p>
                            <?php echo $f101_20['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_AUTOC']; ?></p>
                            <?php echo $f101_20['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_AUTOI']; ?></p>
                            <?php echo $f101_20['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['DP_AANIM']; ?></p>
                            <?php echo $f101_20['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P1']; ?></p>
                            <?php echo $f201_50['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P2']; ?></p>
                            <?php echo $f201_50['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P3']; ?></p>
                            <?php echo $f201_50['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P4']; ?></p>
                            <?php echo $f201_50['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P5']; ?></p>
                            <?php echo $f201_50['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P610']; ?></p>
                            <?php echo $f201_50['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['NP_P10']; ?></p>
                            <?php echo $f201_50['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_NAT']; ?></p>
                            <?php echo $f201_50['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_SAC']; ?></p>
                            <?php echo $f201_50['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_SAA']; ?></p>
                            <?php echo $f201_50['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_SRL']; ?></p>
                            <?php echo $f201_50['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_EIRL']; ?></p>
                            <?php echo $f201_50['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_COOPA']; ?></p>
                            <?php echo $f201_50['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_COMC']; ?></p>
                            <?php echo $f201_50['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_COMN']; ?></p>
                            <?php echo $f201_50['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['CJ_OTRA']; ?></p>
                            <?php echo $f201_50['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GE_18'];  ?></p>
                            <?php echo $f201_50['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GE_1930']; ?></p>
                            <?php echo $f201_50['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GE_3145']; ?></p>
                            <?php echo $f201_50['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GE_4564']; ?></p>
                            <?php echo $f201_50['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['GE_65']; ?></p>
                            <?php echo $f201_50['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['SI_GRAV']; ?></p>
                            <?php echo $f201_50['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['SI_ASP']; ?></p>
                            <?php echo $f201_50['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['SI_GOT']; ?></p>
                            <?php echo $f201_50['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['SI_EXU']; ?></p>
                            <?php echo $f201_50['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['SI_SECANO']; ?></p>
                            <?php echo $f201_50['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_MNAC']; ?></p>
                            <?php echo $f201_50['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_MEXT']; ?></p>
                            <?php echo $f201_50['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_AGRO']; ?></p>
                            <?php echo $f201_50['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_AUTOC']; ?></p>
                            <?php echo $f201_50['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_AUTOI']; ?></p>
                            <?php echo $f201_50['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['DP_AANIM']; ?></p>
                            <?php echo $f201_50['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P1']; ?></p>
                            <?php echo $f501_100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P2']; ?></p>
                            <?php echo $f501_100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P3']; ?></p>
                            <?php echo $f501_100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P4']; ?></p>
                            <?php echo $f501_100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P5']; ?></p>
                            <?php echo $f501_100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P610']; ?></p>
                            <?php echo $f501_100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['NP_P10']; ?></p>
                            <?php echo $f501_100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_NAT']; ?></p>
                            <?php echo $f501_100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_SAC']; ?></p>
                            <?php echo $f501_100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_SAA']; ?></p>
                            <?php echo $f501_100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_SRL']; ?></p>
                            <?php echo $f501_100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_EIRL']; ?></p>
                            <?php echo $f501_100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_COOPA']; ?></p>
                            <?php echo $f501_100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_COMC']; ?></p>
                            <?php echo $f501_100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_COMN']; ?></p>
                            <?php echo $f501_100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['CJ_OTRA']; ?></p>
                            <?php echo $f501_100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GE_18'];  ?></p>
                            <?php echo $f501_100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GE_1930']; ?></p>
                            <?php echo $f501_100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GE_3145']; ?></p>
                            <?php echo $f501_100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GE_4564']; ?></p>
                            <?php echo $f501_100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['GE_65']; ?></p>
                            <?php echo $f501_100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['SI_GRAV']; ?></p>
                            <?php echo $f501_100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['SI_ASP']; ?></p>
                            <?php echo $f501_100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['SI_GOT']; ?></p>
                            <?php echo $f501_100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['SI_EXU']; ?></p>
                            <?php echo $f501_100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['SI_SECANO']; ?></p>
                            <?php echo $f501_100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_MNAC']; ?></p>
                            <?php echo $f501_100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_MEXT']; ?></p>
                            <?php echo $f501_100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_AGRO']; ?></p>
                            <?php echo $f501_100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_AUTOC']; ?></p>
                            <?php echo $f501_100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_AUTOI']; ?></p>
                            <?php echo $f501_100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['DP_AANIM']; ?></p>
                            <?php echo $f501_100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P1']; ?></p>
                            <?php echo $f100['NP_P1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P2']; ?></p>
                            <?php echo $f100['NP_P2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P3']; ?></p>
                            <?php echo $f100['NP_P3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P4']; ?></p>
                            <?php echo $f100['NP_P4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P5']; ?></p>
                            <?php echo $f100['NP_P5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P610']; ?></p>
                            <?php echo $f100['NP_P610_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['NP_P10']; ?></p>
                            <?php echo $f100['NP_P10_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_NAT']; ?></p>
                            <?php echo $f100['CJ_NAT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_SAC']; ?></p>
                            <?php echo $f100['CJ_SAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_SAA']; ?></p>
                            <?php echo $f100['CJ_SAA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_SRL']; ?></p>
                            <?php echo $f100['CJ_SRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_EIRL']; ?></p>
                            <?php echo $f100['CJ_EIRL_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_COOPA']; ?></p>
                            <?php echo $f100['CJ_COOPA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_COMC']; ?></p>
                            <?php echo $f100['CJ_COMC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_COMN']; ?></p>
                            <?php echo $f100['CJ_COMN_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['CJ_OTRA']; ?></p>
                            <?php echo $f100['CJ_OTRA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GE_18'];  ?></p>
                            <?php echo $f100['GE_18_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GE_1930']; ?></p>
                            <?php echo $f100['GE_1930_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GE_3145']; ?></p>
                            <?php echo $f100['GE_3145_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GE_4564']; ?></p>
                            <?php echo $f100['GE_4564_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['GE_65']; ?></p>
                            <?php echo $f100['GE_65_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['SI_GRAV']; ?></p>
                            <?php echo $f100['SI_GRAV_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['SI_ASP']; ?></p>
                            <?php echo $f100['SI_ASP_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['SI_GOT']; ?></p>
                            <?php echo $f100['SI_GOT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['SI_EXU']; ?></p>
                            <?php echo $f100['SI_EXU_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['SI_SECANO']; ?></p>
                            <?php echo $f100['SI_SECANO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_MNAC']; ?></p>
                            <?php echo $f100['DP_MNAC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_MEXT']; ?></p>
                            <?php echo $f100['DP_MEXT_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_AGRO']; ?></p>
                            <?php echo $f100['DP_AGRO_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_AUTOC']; ?></p>
                            <?php echo $f100['DP_AUTOC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_AUTOI']; ?></p>
                            <?php echo $f100['DP_AUTOI_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['DP_AANIM']; ?></p>
                            <?php echo $f100['DP_AANIM_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        