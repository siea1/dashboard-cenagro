
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Capacidades</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('Capacidades')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="pieChartR5"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="doughnutChartR5"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th colspan="4" class="table-info"> 
                            <center>Recibió algún tipo de servicio de extensión</center>
                          </th>
                          <th colspan="5" class="table-warning"> 
                            <center>En que temáticas recibió  servicios de extensión</center>
                          </th>
                          <th colspan="7" class="table-danger"> 
                            <center>Medios de información</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Capacitación
                          </td>
                          <td>
                            Asist. Técnica
                          </td>
                          <td>
                            As. Empr.
                          </td>
                          <td>
                            No Recibió
                          </td>
                          <td>
                            En Cultivos
                          </td>
                          <td>
                            En Ganaderia
                          </td>
                          <td>
                            Manejo, Obs., Proc.
                          </td>
                          <td>
                            Asoc. Prod. y Comer.
                          </td>
                          <td>
                            En Neg. y Comer.
                          </td>
                          <td>
                            Teléfono
                          </td>
                          <td>
                            Radio
                          </td>
                          <td>
                            Televisión
                          </td>
                          <td>
                            Internet
                          </td>
                          <td>
                            Pub. Escritas
                          </td>
                          <td>
                            Otros
                          </td>
                          <td>
                            No Hacen de Ningun Medio
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_1'] + $f0_5['RES_1']; ?></p>
                            <?php echo $m0_5['RES_1_S'] + $f0_5['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_2'] + $f0_5['RES_2']; ?></p>
                            <?php echo $m0_5['RES_2_S'] + $f0_5['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_3'] + $f0_5['RES_3']; ?></p>
                            <?php echo $m0_5['RES_3_S'] + $f0_5['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_4'] + $f0_5['RES_4']; ?></p>
                            <?php echo $m0_5['RES_4_S'] + $f0_5['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_1'] + $f0_5['TRS_1']; ?></p>
                            <?php echo $m0_5['TRS_1_S'] + $f0_5['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_2'] + $f0_5['TRS_2']; ?></p>
                            <?php echo $m0_5['TRS_2_S'] + $f0_5['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_3'] + $f0_5['TRS_3']; ?></p>
                            <?php echo $m0_5['TRS_3_S'] + $f0_5['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_4'] + $f0_5['TRS_4']; ?></p>
                            <?php echo $m0_5['TRS_4_S'] + $f0_5['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_5'] + $f0_5['TRS_5']; ?></p>
                            <?php echo $m0_5['TRS_5_S'] + $f0_5['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_1'] + $f0_5['MI_1']; ?></p>
                            <?php echo $m0_5['MI_1_S'] + $f0_5['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_2'] + $f0_5['MI_2']; ?></p>
                            <?php echo $m0_5['MI_2_S'] + $f0_5['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_3'] + $f0_5['MI_3']; ?></p>
                            <?php echo $m0_5['MI_3_S'] + $f0_5['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_4'] + $f0_5['MI_4']; ?></p>
                            <?php echo $m0_5['MI_4_S'] + $f0_5['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_5'] + $f0_5['MI_5']; ?></p>
                            <?php echo $m0_5['MI_5_S'] + $f0_5['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_6'] + $f0_5['MI_6']; ?></p>
                            <?php echo $m0_5['MI_6_S'] + $f0_5['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_7'] + $f0_5['MI_7']; ?></p>
                            <?php echo $m0_5['MI_7_S'] + $f0_5['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_1'] + $f51_10['RES_1']; ?></p>
                            <?php echo $m51_10['RES_1_S'] + $f51_10['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_2'] + $f51_10['RES_2']; ?></p>
                            <?php echo $m51_10['RES_2_S'] + $f51_10['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_3'] + $f51_10['RES_3']; ?></p>
                            <?php echo $m51_10['RES_3_S'] + $f51_10['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_4'] + $f51_10['RES_4']; ?></p>
                            <?php echo $m51_10['RES_4_S'] + $f51_10['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_1'] + $f51_10['TRS_1']; ?></p>
                            <?php echo $m51_10['TRS_1_S'] + $f51_10['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_2'] + $f51_10['TRS_2']; ?></p>
                            <?php echo $m51_10['TRS_2_S'] + $f51_10['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_3'] + $f51_10['TRS_3']; ?></p>
                            <?php echo $m51_10['TRS_3_S'] + $f51_10['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_4'] + $f51_10['TRS_4']; ?></p>
                            <?php echo $m51_10['TRS_4_S'] + $f51_10['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_5'] + $f51_10['TRS_5']; ?></p>
                            <?php echo $m51_10['TRS_5_S'] + $f51_10['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_1'] + $f51_10['MI_1']; ?></p>
                            <?php echo $m51_10['MI_1_S'] + $f51_10['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_2'] + $f51_10['MI_2']; ?></p>
                            <?php echo $m51_10['MI_2_S'] + $f51_10['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_3'] + $f51_10['MI_3']; ?></p>
                            <?php echo $m51_10['MI_3_S'] + $f51_10['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_4'] + $f51_10['MI_4']; ?></p>
                            <?php echo $m51_10['MI_4_S'] + $f51_10['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_5'] + $f51_10['MI_5']; ?></p>
                            <?php echo $m51_10['MI_5_S'] + $f51_10['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_6'] + $f51_10['MI_6']; ?></p>
                            <?php echo $m51_10['MI_6_S'] + $f51_10['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_7'] + $f51_10['MI_7']; ?></p>
                            <?php echo $m51_10['MI_7_S'] + $f51_10['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_1'] + $f101_20['RES_1']; ?></p>
                            <?php echo $m101_20['RES_1_S'] + $f101_20['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_2'] + $f101_20['RES_2']; ?></p>
                            <?php echo $m101_20['RES_2_S'] + $f101_20['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_3'] + $f101_20['RES_3']; ?></p>
                            <?php echo $m101_20['RES_3_S'] + $f101_20['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_4'] + $f101_20['RES_4']; ?></p>
                            <?php echo $m101_20['RES_4_S'] + $f101_20['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_1'] + $f101_20['TRS_1']; ?></p>
                            <?php echo $m101_20['TRS_1_S'] + $f101_20['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_2'] + $f101_20['TRS_2']; ?></p>
                            <?php echo $m101_20['TRS_2_S'] + $f101_20['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_3'] + $f101_20['TRS_3']; ?></p>
                            <?php echo $m101_20['TRS_3_S'] + $f101_20['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_4'] + $f101_20['TRS_4']; ?></p>
                            <?php echo $m101_20['TRS_4_S'] + $f101_20['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_5'] + $f101_20['TRS_5']; ?></p>
                            <?php echo $m101_20['TRS_5_S'] + $f101_20['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_1'] + $f101_20['MI_1']; ?></p>
                            <?php echo $m101_20['MI_1_S'] + $f101_20['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_2'] + $f101_20['MI_2']; ?></p>
                            <?php echo $m101_20['MI_2_S'] + $f101_20['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_3'] + $f101_20['MI_3']; ?></p>
                            <?php echo $m101_20['MI_3_S'] + $f101_20['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_4'] + $f101_20['MI_4']; ?></p>
                            <?php echo $m101_20['MI_4_S'] + $f101_20['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_5'] + $f101_20['MI_5']; ?></p>
                            <?php echo $m101_20['MI_5_S'] + $f101_20['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_6'] + $f101_20['MI_6']; ?></p>
                            <?php echo $m101_20['MI_6_S'] + $f101_20['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_7'] + $f101_20['MI_7']; ?></p>
                            <?php echo $m101_20['MI_7_S'] + $f101_20['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_1'] + $f201_50['RES_1']; ?></p>
                            <?php echo $m201_50['RES_1_S'] + $f201_50['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_2'] + $f201_50['RES_2']; ?></p>
                            <?php echo $m201_50['RES_2_S'] + $f201_50['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_3'] + $f201_50['RES_3']; ?></p>
                            <?php echo $m201_50['RES_3_S'] + $f201_50['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_4'] + $f201_50['RES_4']; ?></p>
                            <?php echo $m201_50['RES_4_S'] + $f201_50['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_1'] + $f201_50['TRS_1']; ?></p>
                            <?php echo $m201_50['TRS_1_S'] + $f201_50['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_2'] + $f201_50['TRS_2']; ?></p>
                            <?php echo $m201_50['TRS_2_S'] + $f201_50['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_3'] + $f201_50['TRS_3']; ?></p>
                            <?php echo $m201_50['TRS_3_S'] + $f201_50['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_4'] + $f201_50['TRS_4']; ?></p>
                            <?php echo $m201_50['TRS_4_S'] + $f201_50['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_5'] + $f201_50['TRS_5']; ?></p>
                            <?php echo $m201_50['TRS_5_S'] + $f201_50['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_1'] + $f201_50['MI_1']; ?></p>
                            <?php echo $m201_50['MI_1_S'] + $f201_50['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_2'] + $f201_50['MI_2']; ?></p>
                            <?php echo $m201_50['MI_2_S'] + $f201_50['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_3'] + $f201_50['MI_3']; ?></p>
                            <?php echo $m201_50['MI_3_S'] + $f201_50['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_4'] + $f201_50['MI_4']; ?></p>
                            <?php echo $m201_50['MI_4_S'] + $f201_50['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_5'] + $f201_50['MI_5']; ?></p>
                            <?php echo $m201_50['MI_5_S'] + $f201_50['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_6'] + $f201_50['MI_6']; ?></p>
                            <?php echo $m201_50['MI_6_S'] + $f201_50['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_7'] + $f201_50['MI_7']; ?></p>
                            <?php echo $m201_50['MI_7_S'] + $f201_50['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_1'] + $f501_100['RES_1']; ?></p>
                            <?php echo $m501_100['RES_1_S'] + $f501_100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_2'] + $f501_100['RES_2']; ?></p>
                            <?php echo $m501_100['RES_2_S'] + $f501_100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_3'] + $f501_100['RES_3']; ?></p>
                            <?php echo $m501_100['RES_3_S'] + $f501_100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_4'] + $f501_100['RES_4']; ?></p>
                            <?php echo $m501_100['RES_4_S'] + $f501_100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_1'] + $f501_100['TRS_1']; ?></p>
                            <?php echo $m501_100['TRS_1_S'] + $f501_100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_2'] + $f501_100['TRS_2']; ?></p>
                            <?php echo $m501_100['TRS_2_S'] + $f501_100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_3'] + $f501_100['TRS_3']; ?></p>
                            <?php echo $m501_100['TRS_3_S'] + $f501_100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_4'] + $f501_100['TRS_4']; ?></p>
                            <?php echo $m501_100['TRS_4_S'] + $f501_100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_5'] + $f501_100['TRS_5']; ?></p>
                            <?php echo $m501_100['TRS_5_S'] + $f501_100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_1'] + $f501_100['MI_1']; ?></p>
                            <?php echo $m501_100['MI_1_S'] + $f501_100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_2'] + $f501_100['MI_2']; ?></p>
                            <?php echo $m501_100['MI_2_S'] + $f501_100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_3'] + $f501_100['MI_3']; ?></p>
                            <?php echo $m501_100['MI_3_S'] + $f501_100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_4'] + $f501_100['MI_4']; ?></p>
                            <?php echo $m501_100['MI_4_S'] + $f501_100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_5'] + $f501_100['MI_5']; ?></p>
                            <?php echo $m501_100['MI_5_S'] + $f501_100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_6'] + $f501_100['MI_6']; ?></p>
                            <?php echo $m501_100['MI_6_S'] + $f501_100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_7'] + $f501_100['MI_7']; ?></p>
                            <?php echo $m501_100['MI_7_S'] + $f501_100['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_1'] + $f100['RES_1']; ?></p>
                            <?php echo $m100['RES_1_S'] + $f100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_2'] + $f100['RES_2']; ?></p>
                            <?php echo $m100['RES_2_S'] + $f100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_3'] + $f100['RES_3']; ?></p>
                            <?php echo $m100['RES_3_S'] + $f100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_4'] + $f100['RES_4']; ?></p>
                            <?php echo $m100['RES_4_S'] + $f100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_1'] + $f100['TRS_1']; ?></p>
                            <?php echo $m100['TRS_1_S'] + $f100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_2'] + $f100['TRS_2']; ?></p>
                            <?php echo $m100['TRS_2_S'] + $f100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_3'] + $f100['TRS_3']; ?></p>
                            <?php echo $m100['TRS_3_S'] + $f100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_4'] + $f100['TRS_4']; ?></p>
                            <?php echo $m100['TRS_4_S'] + $f100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_5'] + $f100['TRS_5']; ?></p>
                            <?php echo $m100['TRS_5_S'] + $f100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_1'] + $f100['MI_1']; ?></p>
                            <?php echo $m100['MI_1_S'] + $f100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_2'] + $f100['MI_2']; ?></p>
                            <?php echo $m100['MI_2_S'] + $f100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_3'] + $f100['MI_3']; ?></p>
                            <?php echo $m100['MI_3_S'] + $f100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_4'] + $f100['MI_4']; ?></p>
                            <?php echo $m100['MI_4_S'] + $f100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_5'] + $f100['MI_5']; ?></p>
                            <?php echo $m100['MI_5_S'] + $f100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_6'] + $f100['MI_6']; ?></p>
                            <?php echo $m100['MI_6_S'] + $f100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_7'] + $f100['MI_7']; ?></p>
                            <?php echo $m100['MI_7_S'] + $f100['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_1']; ?></p>
                            <?php echo $m0_5['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_2']; ?></p>
                            <?php echo $m0_5['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_3']; ?></p>
                            <?php echo $m0_5['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['RES_4']; ?></p>
                            <?php echo $m0_5['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_1']; ?></p>
                            <?php echo $m0_5['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_2']; ?></p>
                            <?php echo $m0_5['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_3']; ?></p>
                            <?php echo $m0_5['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_4']; ?></p>
                            <?php echo $m0_5['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['TRS_5']; ?></p>
                            <?php echo $m0_5['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_1']; ?></p>
                            <?php echo $m0_5['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_2']; ?></p>
                            <?php echo $m0_5['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_3']; ?></p>
                            <?php echo $m0_5['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_4']; ?></p>
                            <?php echo $m0_5['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_5']; ?></p>
                            <?php echo $m0_5['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_6']; ?></p>
                            <?php echo $m0_5['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['MI_7']; ?></p>
                            <?php echo $m0_5['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_1']; ?></p>
                            <?php echo $m51_10['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_2']; ?></p>
                            <?php echo $m51_10['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_3']; ?></p>
                            <?php echo $m51_10['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['RES_4']; ?></p>
                            <?php echo $m51_10['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_1']; ?></p>
                            <?php echo $m51_10['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_2']; ?></p>
                            <?php echo $m51_10['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_3']; ?></p>
                            <?php echo $m51_10['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_4']; ?></p>
                            <?php echo $m51_10['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['TRS_5']; ?></p>
                            <?php echo $m51_10['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_1']; ?></p>
                            <?php echo $m51_10['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_2']; ?></p>
                            <?php echo $m51_10['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_3']; ?></p>
                            <?php echo $m51_10['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_4']; ?></p>
                            <?php echo $m51_10['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_5']; ?></p>
                            <?php echo $m51_10['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_6']; ?></p>
                            <?php echo $m51_10['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['MI_7']; ?></p>
                            <?php echo $m51_10['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_1']; ?></p>
                            <?php echo $m101_20['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_2']; ?></p>
                            <?php echo $m101_20['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_3']; ?></p>
                            <?php echo $m101_20['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['RES_4']; ?></p>
                            <?php echo $m101_20['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_1']; ?></p>
                            <?php echo $m101_20['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_2']; ?></p>
                            <?php echo $m101_20['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_3']; ?></p>
                            <?php echo $m101_20['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_4']; ?></p>
                            <?php echo $m101_20['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['TRS_5']; ?></p>
                            <?php echo $m101_20['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_1']; ?></p>
                            <?php echo $m101_20['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_2']; ?></p>
                            <?php echo $m101_20['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_3']; ?></p>
                            <?php echo $m101_20['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_4']; ?></p>
                            <?php echo $m101_20['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_5']; ?></p>
                            <?php echo $m101_20['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_6']; ?></p>
                            <?php echo $m101_20['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['MI_7']; ?></p>
                            <?php echo $m101_20['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_1']; ?></p>
                            <?php echo $m201_50['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_2']; ?></p>
                            <?php echo $m201_50['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_3']; ?></p>
                            <?php echo $m201_50['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['RES_4']; ?></p>
                            <?php echo $m201_50['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_1']; ?></p>
                            <?php echo $m201_50['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_2']; ?></p>
                            <?php echo $m201_50['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_3']; ?></p>
                            <?php echo $m201_50['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_4']; ?></p>
                            <?php echo $m201_50['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['TRS_5']; ?></p>
                            <?php echo $m201_50['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_1']; ?></p>
                            <?php echo $m201_50['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_2']; ?></p>
                            <?php echo $m201_50['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_3']; ?></p>
                            <?php echo $m201_50['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_4']; ?></p>
                            <?php echo $m201_50['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_5']; ?></p>
                            <?php echo $m201_50['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_6']; ?></p>
                            <?php echo $m201_50['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['MI_7']; ?></p>
                            <?php echo $m201_50['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_1']; ?></p>
                            <?php echo $m501_100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_2']; ?></p>
                            <?php echo $m501_100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_3']; ?></p>
                            <?php echo $m501_100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['RES_4']; ?></p>
                            <?php echo $m501_100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_1']; ?></p>
                            <?php echo $m501_100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_2']; ?></p>
                            <?php echo $m501_100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_3']; ?></p>
                            <?php echo $m501_100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_4']; ?></p>
                            <?php echo $m501_100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['TRS_5']; ?></p>
                            <?php echo $m501_100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_1']; ?></p>
                            <?php echo $m501_100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_2']; ?></p>
                            <?php echo $m501_100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_3']; ?></p>
                            <?php echo $m501_100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_4']; ?></p>
                            <?php echo $m501_100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_5']; ?></p>
                            <?php echo $m501_100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_6']; ?></p>
                            <?php echo $m501_100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['MI_7']; ?></p>
                            <?php echo $m501_100['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_1']; ?></p>
                            <?php echo $m100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_2']; ?></p>
                            <?php echo $m100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_3']; ?></p>
                            <?php echo $m100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['RES_4']; ?></p>
                            <?php echo $m100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_1']; ?></p>
                            <?php echo $m100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_2']; ?></p>
                            <?php echo $m100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_3']; ?></p>
                            <?php echo $m100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_4']; ?></p>
                            <?php echo $m100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['TRS_5']; ?></p>
                            <?php echo $m100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_1']; ?></p>
                            <?php echo $m100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_2']; ?></p>
                            <?php echo $m100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_3']; ?></p>
                            <?php echo $m100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_4']; ?></p>
                            <?php echo $m100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_5']; ?></p>
                            <?php echo $m100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_6']; ?></p>
                            <?php echo $m100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['MI_7']; ?></p>
                            <?php echo $m100['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RES_1']; ?></p>
                            <?php echo $f0_5['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RES_2']; ?></p>
                            <?php echo $f0_5['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RES_3']; ?></p>
                            <?php echo $f0_5['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['RES_4']; ?></p>
                            <?php echo $f0_5['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TRS_1']; ?></p>
                            <?php echo $f0_5['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TRS_2']; ?></p>
                            <?php echo $f0_5['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TRS_3']; ?></p>
                            <?php echo $f0_5['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TRS_4']; ?></p>
                            <?php echo $f0_5['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['TRS_5']; ?></p>
                            <?php echo $f0_5['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_1']; ?></p>
                            <?php echo $f0_5['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_2']; ?></p>
                            <?php echo $f0_5['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_3']; ?></p>
                            <?php echo $f0_5['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_4']; ?></p>
                            <?php echo $f0_5['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_5']; ?></p>
                            <?php echo $f0_5['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_6']; ?></p>
                            <?php echo $f0_5['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['MI_7']; ?></p>
                            <?php echo $f0_5['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RES_1']; ?></p>
                            <?php echo $f51_10['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RES_2']; ?></p>
                            <?php echo $f51_10['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RES_3']; ?></p>
                            <?php echo $f51_10['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['RES_4']; ?></p>
                            <?php echo $f51_10['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TRS_1']; ?></p>
                            <?php echo $f51_10['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TRS_2']; ?></p>
                            <?php echo $f51_10['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TRS_3']; ?></p>
                            <?php echo $f51_10['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TRS_4']; ?></p>
                            <?php echo $f51_10['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['TRS_5']; ?></p>
                            <?php echo $f51_10['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_1']; ?></p>
                            <?php echo $f51_10['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_2']; ?></p>
                            <?php echo $f51_10['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_3']; ?></p>
                            <?php echo $f51_10['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_4']; ?></p>
                            <?php echo $f51_10['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_5']; ?></p>
                            <?php echo $f51_10['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_6']; ?></p>
                            <?php echo $f51_10['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['MI_7']; ?></p>
                            <?php echo $f51_10['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RES_1']; ?></p>
                            <?php echo $f101_20['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RES_2']; ?></p>
                            <?php echo $f101_20['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RES_3']; ?></p>
                            <?php echo $f101_20['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['RES_4']; ?></p>
                            <?php echo $f101_20['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TRS_1']; ?></p>
                            <?php echo $f101_20['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TRS_2']; ?></p>
                            <?php echo $f101_20['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TRS_3']; ?></p>
                            <?php echo $f101_20['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TRS_4']; ?></p>
                            <?php echo $f101_20['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['TRS_5']; ?></p>
                            <?php echo $f101_20['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_1']; ?></p>
                            <?php echo $f101_20['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_2']; ?></p>
                            <?php echo $f101_20['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_3']; ?></p>
                            <?php echo $f101_20['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_4']; ?></p>
                            <?php echo $f101_20['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_5']; ?></p>
                            <?php echo $f101_20['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_6']; ?></p>
                            <?php echo $f101_20['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['MI_7']; ?></p>
                            <?php echo $f101_20['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RES_1']; ?></p>
                            <?php echo $f201_50['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RES_2']; ?></p>
                            <?php echo $f201_50['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RES_3']; ?></p>
                            <?php echo $f201_50['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['RES_4']; ?></p>
                            <?php echo $f201_50['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TRS_1']; ?></p>
                            <?php echo $f201_50['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TRS_2']; ?></p>
                            <?php echo $f201_50['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TRS_3']; ?></p>
                            <?php echo $f201_50['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TRS_4']; ?></p>
                            <?php echo $f201_50['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['TRS_5']; ?></p>
                            <?php echo $f201_50['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_1']; ?></p>
                            <?php echo $f201_50['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_2']; ?></p>
                            <?php echo $f201_50['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_3']; ?></p>
                            <?php echo $f201_50['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_4']; ?></p>
                            <?php echo $f201_50['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_5']; ?></p>
                            <?php echo $f201_50['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_6']; ?></p>
                            <?php echo $f201_50['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['MI_7']; ?></p>
                            <?php echo $f201_50['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RES_1']; ?></p>
                            <?php echo $f501_100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RES_2']; ?></p>
                            <?php echo $f501_100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RES_3']; ?></p>
                            <?php echo $f501_100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['RES_4']; ?></p>
                            <?php echo $f501_100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TRS_1']; ?></p>
                            <?php echo $f501_100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TRS_2']; ?></p>
                            <?php echo $f501_100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TRS_3']; ?></p>
                            <?php echo $f501_100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TRS_4']; ?></p>
                            <?php echo $f501_100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['TRS_5']; ?></p>
                            <?php echo $f501_100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_1']; ?></p>
                            <?php echo $f501_100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_2']; ?></p>
                            <?php echo $f501_100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_3']; ?></p>
                            <?php echo $f501_100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_4']; ?></p>
                            <?php echo $f501_100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_5']; ?></p>
                            <?php echo $f501_100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_6']; ?></p>
                            <?php echo $f501_100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['MI_7']; ?></p>
                            <?php echo $f501_100['MI_7_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RES_1']; ?></p>
                            <?php echo $f100['RES_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RES_2']; ?></p>
                            <?php echo $f100['RES_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RES_3']; ?></p>
                            <?php echo $f100['RES_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['RES_4']; ?></p>
                            <?php echo $f100['RES_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TRS_1']; ?></p>
                            <?php echo $f100['TRS_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TRS_2']; ?></p>
                            <?php echo $f100['TRS_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TRS_3']; ?></p>
                            <?php echo $f100['TRS_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TRS_4']; ?></p>
                            <?php echo $f100['TRS_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['TRS_5']; ?></p>
                            <?php echo $f100['TRS_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_1']; ?></p>
                            <?php echo $f100['MI_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_2']; ?></p>
                            <?php echo $f100['MI_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_3']; ?></p>
                            <?php echo $f100['MI_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_4']; ?></p>
                            <?php echo $f100['MI_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_5']; ?></p>
                            <?php echo $f100['MI_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_6']; ?></p>
                            <?php echo $f100['MI_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['MI_7']; ?></p>
                            <?php echo $f100['MI_7_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       