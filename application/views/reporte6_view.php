
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Prácticas pecuarias</h4>
                  <?php $atributos = array('class' => 'form-inline', 'id' => 'formFilter'); ?>
                  <?php echo form_open('dashboard/reporte/'.$num, $atributos); ?>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDepartamento" name="departamento">
                        <option>Seleccione departamento</option>
                        <option value='01'>Amazonas</option>
                        <option value='02'>Ancash</option>
                        <option value='03'>Apurimac</option>
                        <option value='04'>Arequipa</option>
                        <option value='05'>Ayacucho</option>
                        <option value='06'>Cajamarca</option>
                        <option value='07'>Callao</option>
                        <option value='08'>Cusco</option>
                        <option value='09'>Huancavelica</option>
                        <option value='10'>Huanuco</option>
                        <option value='11'>Ica</option>
                        <option value='12'>Junín</option>
                        <option value='13'>La Libertad</option>
                        <option value='14'>Lambayeque</option>
                        <option value='15'>Lima</option>
                        <option value='16'>Loreto</option>
                        <option value='17'>Madre de Dios</option>
                        <option value='18'>Moquegua</option>
                        <option value='19'>Pasco</option>
                        <option value='20'>Piura</option>
                        <option value='21'>Puno</option>
                        <option value='22'>San Martín</option>
                        <option value='23'>Tacna</option>
                        <option value='24'>Tumbes</option>
                        <option value='25'>Ucayali</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelProvincia" name="provincia">
                        <option>Seleccione Provincia</option>
                      </select>
                    </div>
                    <div class="input-group mb-2 mr-sm-2">
                      <select class="form-control" id="SelDistrito" name="distrito">
                        <option value="">Seleccione Distrito</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-danger mb-2">Buscar</button>
                  </form>

                  <div class="template-demo">
                    <button type="button" class="btn btn-outline-success btn-icon-text" onclick="exportReportToExcel('PracticasPecuarias')" id="btnExcel">
                    <i class="typcn typcn-download btn-icon-prepend"></i> Excel</button>
                    <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="viewGraphics()" id="btnGraphics">
                    <i class="typcn typcn-chart-bar btn-icon-prepend"></i> Gráficos</button>
                    <button type="button" class="btn btn-outline-warning btn-icon-text" onclick="viewTable()" id="btnTable" style="display: none;">
                    <i class="typcn typcn-th-small-outline btn-icon-prepend"></i> Tabla</button>
                  </div>

                  <div class="row mt-4" id="graphicsIdDiv" style="display: none">
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Unidades Agropecuarias</h4>
                          <canvas id="barChart"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="pieChartR6"></canvas>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 grid-margin stretch-card">
                      <div class="card">
                        <div class="card-body">
                          <h4 class="card-title"></h4>
                          <canvas id="doughnutChartR6"></canvas>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive pt-3" id="tableIdDiv">
                    <table class="table table-bordered" id="tableId">
                      <thead>
                        <tr >
                          <th>
                            #
                          </th>
                          <th>
                            #
                          </th>
                          <th>
                            UA
                          </th>
                          <th class="table-success">
                            Existe Ganado/Aves u Otros
                          </th>
                          <th colspan="6" class="table-info"> 
                            <center>Principales Prácticas Pecuarias</center>
                          </th>
                          <th colspan="9" class="table-warning"> 
                            <center>Formas de Eliminación de Residuos</center>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="background-color: #f5f9ff">
                          <td>
                            #
                          </td>
                          <td>
                            #
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Total UA
                          </td>
                          <td>
                            Vacunas a Anim.
                          </td>
                          <td>
                            Baña contra Parásito
                          </td>
                          <td>
                            Efectua Dosif.
                          </td>
                          <td>
                            Utiliza Alim. Bal.
                          </td>
                          <td>
                            Efec. Insem. Art.
                          </td>
                          <td>
                            Sementales de Raza para Mej.
                          </td>
                          <td>
                            Los deja en Calle/Via Pub.
                          </td>
                          <td>
                            Echa al Río/Aceq./Lag./Playa
                          </td>
                          <td>
                            Botadero a Cielo Ab.
                          </td>
                          <td>
                            En Contenedor Comunal
                          </td>
                          <td>
                            Los Quema
                          </td>
                          <td>
                            Los Entierra
                          </td>
                          <td>
                            Los Usa como Abono
                          </td>
                          <td>
                            Los Vende
                          </td>
                          <td>
                            Otros
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA'] + $f0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S'] + $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PEC'] + $f0_5['PEC']; ?></p>
                            <?php echo $m0_5['PEC_S'] + $f0_5['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_1'] + $f0_5['PPP_1']; ?></p>
                            <?php echo $m0_5['PPP_1_S'] + $f0_5['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_2'] + $f0_5['PPP_2']; ?></p>
                            <?php echo $m0_5['PPP_2_S'] + $f0_5['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_3'] + $f0_5['PPP_3']; ?></p>
                            <?php echo $m0_5['PPP_3_S'] + $f0_5['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_4'] + $f0_5['PPP_4']; ?></p>
                            <?php echo $m0_5['PPP_4_S'] + $f0_5['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_5'] + $f0_5['PPP_5']; ?></p>
                            <?php echo $m0_5['PPP_5_S'] + $f0_5['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_6'] + $f0_5['PPP_6']; ?></p>
                            <?php echo $m0_5['PPP_6_S'] + $f0_5['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_1'] + $f0_5['FER_1']; ?></p>
                            <?php echo $m0_5['FER_1_S'] + $f0_5['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_2'] + $f0_5['FER_2']; ?></p>
                            <?php echo $m0_5['FER_2_S'] + $f0_5['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_3'] + $f0_5['FER_3']; ?></p>
                            <?php echo $m0_5['FER_3_S'] + $f0_5['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_4'] + $f0_5['FER_4']; ?></p>
                            <?php echo $m0_5['FER_4_S'] + $f0_5['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_5'] + $f0_5['FER_5']; ?></p>
                            <?php echo $m0_5['FER_5_S'] + $f0_5['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_6'] + $f0_5['FER_6']; ?></p>
                            <?php echo $m0_5['FER_6_S'] + $f0_5['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_7'] + $f0_5['FER_7']; ?></p>
                            <?php echo $m0_5['FER_7_S'] + $f0_5['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_8'] + $f0_5['FER_8']; ?></p>
                            <?php echo $m0_5['FER_8_S'] + $f0_5['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_9'] + $f0_5['FER_9']; ?></p>
                            <?php echo $m0_5['FER_9_S'] + $f0_5['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA'] + $f51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S'] + $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PEC'] + $f51_10['PEC']; ?></p>
                            <?php echo $m51_10['PEC_S'] + $f51_10['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_1'] + $f51_10['PPP_1']; ?></p>
                            <?php echo $m51_10['PPP_1_S'] + $f51_10['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_2'] + $f51_10['PPP_2']; ?></p>
                            <?php echo $m51_10['PPP_2_S'] + $f51_10['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_3'] + $f51_10['PPP_3']; ?></p>
                            <?php echo $m51_10['PPP_3_S'] + $f51_10['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_4'] + $f51_10['PPP_4']; ?></p>
                            <?php echo $m51_10['PPP_4_S'] + $f51_10['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_5'] + $f51_10['PPP_5']; ?></p>
                            <?php echo $m51_10['PPP_5_S'] + $f51_10['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_6'] + $f51_10['PPP_6']; ?></p>
                            <?php echo $m51_10['PPP_6_S'] + $f51_10['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_1'] + $f51_10['FER_1']; ?></p>
                            <?php echo $m51_10['FER_1_S'] + $f51_10['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_2'] + $f51_10['FER_2']; ?></p>
                            <?php echo $m51_10['FER_2_S'] + $f51_10['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_3'] + $f51_10['FER_3']; ?></p>
                            <?php echo $m51_10['FER_3_S'] + $f51_10['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_4'] + $f51_10['FER_4']; ?></p>
                            <?php echo $m51_10['FER_4_S'] + $f51_10['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_5'] + $f51_10['FER_5']; ?></p>
                            <?php echo $m51_10['FER_5_S'] + $f51_10['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_6'] + $f51_10['FER_6']; ?></p>
                            <?php echo $m51_10['FER_6_S'] + $f51_10['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_7'] + $f51_10['FER_7']; ?></p>
                            <?php echo $m51_10['FER_7_S'] + $f51_10['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_8'] + $f51_10['FER_8']; ?></p>
                            <?php echo $m51_10['FER_8_S'] + $f51_10['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_9'] + $f51_10['FER_9']; ?></p>
                            <?php echo $m51_10['FER_9_S'] + $f51_10['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA'] + $f101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S'] + $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PEC'] + $f101_20['PEC']; ?></p>
                            <?php echo $m101_20['PEC_S'] + $f101_20['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_1'] + $f101_20['PPP_1']; ?></p>
                            <?php echo $m101_20['PPP_1_S'] + $f101_20['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_2'] + $f101_20['PPP_2']; ?></p>
                            <?php echo $m101_20['PPP_2_S'] + $f101_20['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_3'] + $f101_20['PPP_3']; ?></p>
                            <?php echo $m101_20['PPP_3_S'] + $f101_20['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_4'] + $f101_20['PPP_4']; ?></p>
                            <?php echo $m101_20['PPP_4_S'] + $f101_20['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_5'] + $f101_20['PPP_5']; ?></p>
                            <?php echo $m101_20['PPP_5_S'] + $f101_20['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_6'] + $f101_20['PPP_6']; ?></p>
                            <?php echo $m101_20['PPP_6_S'] + $f101_20['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_1'] + $f101_20['FER_1']; ?></p>
                            <?php echo $m101_20['FER_1_S'] + $f101_20['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_2'] + $f101_20['FER_2']; ?></p>
                            <?php echo $m101_20['FER_2_S'] + $f101_20['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_3'] + $f101_20['FER_3']; ?></p>
                            <?php echo $m101_20['FER_3_S'] + $f101_20['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_4'] + $f101_20['FER_4']; ?></p>
                            <?php echo $m101_20['FER_4_S'] + $f101_20['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_5'] + $f101_20['FER_5']; ?></p>
                            <?php echo $m101_20['FER_5_S'] + $f101_20['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_6'] + $f101_20['FER_6']; ?></p>
                            <?php echo $m101_20['FER_6_S'] + $f101_20['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_7'] + $f101_20['FER_7']; ?></p>
                            <?php echo $m101_20['FER_7_S'] + $f101_20['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_8'] + $f101_20['FER_8']; ?></p>
                            <?php echo $m101_20['FER_8_S'] + $f101_20['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_9'] + $f101_20['FER_9']; ?></p>
                            <?php echo $m101_20['FER_9_S'] + $f101_20['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA'] + $f201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S'] + $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PEC'] + $f201_50['PEC']; ?></p>
                            <?php echo $m201_50['PEC_S'] + $f201_50['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_1'] + $f201_50['PPP_1']; ?></p>
                            <?php echo $m201_50['PPP_1_S'] + $f201_50['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_2'] + $f201_50['PPP_2']; ?></p>
                            <?php echo $m201_50['PPP_2_S'] + $f201_50['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_3'] + $f201_50['PPP_3']; ?></p>
                            <?php echo $m201_50['PPP_3_S'] + $f201_50['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_4'] + $f201_50['PPP_4']; ?></p>
                            <?php echo $m201_50['PPP_4_S'] + $f201_50['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_5'] + $f201_50['PPP_5']; ?></p>
                            <?php echo $m201_50['PPP_5_S'] + $f201_50['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_6'] + $f201_50['PPP_6']; ?></p>
                            <?php echo $m201_50['PPP_6_S'] + $f201_50['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_1'] + $f201_50['FER_1']; ?></p>
                            <?php echo $m201_50['FER_1_S'] + $f201_50['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_2'] + $f201_50['FER_2']; ?></p>
                            <?php echo $m201_50['FER_2_S'] + $f201_50['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_3'] + $f201_50['FER_3']; ?></p>
                            <?php echo $m201_50['FER_3_S'] + $f201_50['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_4'] + $f201_50['FER_4']; ?></p>
                            <?php echo $m201_50['FER_4_S'] + $f201_50['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_5'] + $f201_50['FER_5']; ?></p>
                            <?php echo $m201_50['FER_5_S'] + $f201_50['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_6'] + $f201_50['FER_6']; ?></p>
                            <?php echo $m201_50['FER_6_S'] + $f201_50['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_7'] + $f201_50['FER_7']; ?></p>
                            <?php echo $m201_50['FER_7_S'] + $f201_50['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_8'] + $f201_50['FER_8']; ?></p>
                            <?php echo $m201_50['FER_8_S'] + $f201_50['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_9'] + $f201_50['FER_9']; ?></p>
                            <?php echo $m201_50['FER_9_S'] + $f201_50['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA'] + $f501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S'] + $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PEC'] + $f501_100['PEC']; ?></p>
                            <?php echo $m501_100['PEC_S'] + $f501_100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_1'] + $f501_100['PPP_1']; ?></p>
                            <?php echo $m501_100['PPP_1_S'] + $f501_100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_2'] + $f501_100['PPP_2']; ?></p>
                            <?php echo $m501_100['PPP_2_S'] + $f501_100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_3'] + $f501_100['PPP_3']; ?></p>
                            <?php echo $m501_100['PPP_3_S'] + $f501_100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_4'] + $f501_100['PPP_4']; ?></p>
                            <?php echo $m501_100['PPP_4_S'] + $f501_100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_5'] + $f501_100['PPP_5']; ?></p>
                            <?php echo $m501_100['PPP_5_S'] + $f501_100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_6'] + $f501_100['PPP_6']; ?></p>
                            <?php echo $m501_100['PPP_6_S'] + $f501_100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_1'] + $f501_100['FER_1']; ?></p>
                            <?php echo $m501_100['FER_1_S'] + $f501_100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_2'] + $f501_100['FER_2']; ?></p>
                            <?php echo $m501_100['FER_2_S'] + $f501_100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_3'] + $f501_100['FER_3']; ?></p>
                            <?php echo $m501_100['FER_3_S'] + $f501_100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_4'] + $f501_100['FER_4']; ?></p>
                            <?php echo $m501_100['FER_4_S'] + $f501_100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_5'] + $f501_100['FER_5']; ?></p>
                            <?php echo $m501_100['FER_5_S'] + $f501_100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_6'] + $f501_100['FER_6']; ?></p>
                            <?php echo $m501_100['FER_6_S'] + $f501_100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_7'] + $f501_100['FER_7']; ?></p>
                            <?php echo $m501_100['FER_7_S'] + $f501_100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_8'] + $f501_100['FER_8']; ?></p>
                            <?php echo $m501_100['FER_8_S'] + $f501_100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_9'] + $f501_100['FER_9']; ?></p>
                            <?php echo $m501_100['FER_9_S'] + $f501_100['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA'] + $f100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S'] + $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PEC'] + $f100['PEC']; ?></p>
                            <?php echo $m100['PEC_S'] + $f100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_1'] + $f100['PPP_1']; ?></p>
                            <?php echo $m100['PPP_1_S'] + $f100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_2'] + $f100['PPP_2']; ?></p>
                            <?php echo $m100['PPP_2_S'] + $f100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_3'] + $f100['PPP_3']; ?></p>
                            <?php echo $m100['PPP_3_S'] + $f100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_4'] + $f100['PPP_4']; ?></p>
                            <?php echo $m100['PPP_4_S'] + $f100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_5'] + $f100['PPP_5']; ?></p>
                            <?php echo $m100['PPP_5_S'] + $f100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_6'] + $f100['PPP_6']; ?></p>
                            <?php echo $m100['PPP_6_S'] + $f100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_1'] + $f100['FER_1']; ?></p>
                            <?php echo $m100['FER_1_S'] + $f100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_2'] + $f100['FER_2']; ?></p>
                            <?php echo $m100['FER_2_S'] + $f100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_3'] + $f100['FER_3']; ?></p>
                            <?php echo $m100['FER_3_S'] + $f100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_4'] + $f100['FER_4']; ?></p>
                            <?php echo $m100['FER_4_S'] + $f100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_5'] + $f100['FER_5']; ?></p>
                            <?php echo $m100['FER_5_S'] + $f100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_6'] + $f100['FER_6']; ?></p>
                            <?php echo $m100['FER_6_S'] + $f100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_7'] + $f100['FER_7']; ?></p>
                            <?php echo $m100['FER_7_S'] + $f100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_8'] + $f100['FER_8']; ?></p>
                            <?php echo $m100['FER_8_S'] + $f100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_9'] + $f100['FER_9']; ?></p>
                            <?php echo $m100['FER_9_S'] + $f100['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">HOMBRES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m0_5['CON_UA']; ?></p>
                            <?php echo $m0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PEC']; ?></p>
                            <?php echo $m0_5['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_1']; ?></p>
                            <?php echo $m0_5['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_2']; ?></p>
                            <?php echo $m0_5['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_3']; ?></p>
                            <?php echo $m0_5['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_4']; ?></p>
                            <?php echo $m0_5['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_5']; ?></p>
                            <?php echo $m0_5['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['PPP_6']; ?></p>
                            <?php echo $m0_5['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_1']; ?></p>
                            <?php echo $m0_5['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_2']; ?></p>
                            <?php echo $m0_5['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_3']; ?></p>
                            <?php echo $m0_5['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_4']; ?></p>
                            <?php echo $m0_5['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_5']; ?></p>
                            <?php echo $m0_5['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_6']; ?></p>
                            <?php echo $m0_5['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_7']; ?></p>
                            <?php echo $m0_5['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_8']; ?></p>
                            <?php echo $m0_5['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m0_5['FER_9']; ?></p>
                            <?php echo $m0_5['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m51_10['CON_UA']; ?></p>
                            <?php echo $m51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PEC']; ?></p>
                            <?php echo $m51_10['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_1']; ?></p>
                            <?php echo $m51_10['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_2']; ?></p>
                            <?php echo $m51_10['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_3']; ?></p>
                            <?php echo $m51_10['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_4']; ?></p>
                            <?php echo $m51_10['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_5']; ?></p>
                            <?php echo $m51_10['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['PPP_6']; ?></p>
                            <?php echo $m51_10['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_1']; ?></p>
                            <?php echo $m51_10['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_2']; ?></p>
                            <?php echo $m51_10['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_3']; ?></p>
                            <?php echo $m51_10['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_4']; ?></p>
                            <?php echo $m51_10['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_5']; ?></p>
                            <?php echo $m51_10['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_6']; ?></p>
                            <?php echo $m51_10['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_7']; ?></p>
                            <?php echo $m51_10['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_8']; ?></p>
                            <?php echo $m51_10['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m51_10['FER_9']; ?></p>
                            <?php echo $m51_10['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m101_20['CON_UA']; ?></p>
                            <?php echo $m101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PEC']; ?></p>
                            <?php echo $m101_20['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_1']; ?></p>
                            <?php echo $m101_20['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_2']; ?></p>
                            <?php echo $m101_20['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_3']; ?></p>
                            <?php echo $m101_20['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_4']; ?></p>
                            <?php echo $m101_20['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_5']; ?></p>
                            <?php echo $m101_20['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['PPP_6']; ?></p>
                            <?php echo $m101_20['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_1']; ?></p>
                            <?php echo $m101_20['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_2']; ?></p>
                            <?php echo $m101_20['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_3']; ?></p>
                            <?php echo $m101_20['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_4']; ?></p>
                            <?php echo $m101_20['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_5']; ?></p>
                            <?php echo $m101_20['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_6']; ?></p>
                            <?php echo $m101_20['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_7']; ?></p>
                            <?php echo $m101_20['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_8']; ?></p>
                            <?php echo $m101_20['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m101_20['FER_9']; ?></p>
                            <?php echo $m101_20['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m201_50['CON_UA']; ?></p>
                            <?php echo $m201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PEC']; ?></p>
                            <?php echo $m201_50['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_1']; ?></p>
                            <?php echo $m201_50['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_2']; ?></p>
                            <?php echo $m201_50['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_3']; ?></p>
                            <?php echo $m201_50['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_4']; ?></p>
                            <?php echo $m201_50['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_5']; ?></p>
                            <?php echo $m201_50['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['PPP_6']; ?></p>
                            <?php echo $m201_50['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_1']; ?></p>
                            <?php echo $m201_50['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_2']; ?></p>
                            <?php echo $m201_50['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_3']; ?></p>
                            <?php echo $m201_50['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_4']; ?></p>
                            <?php echo $m201_50['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_5']; ?></p>
                            <?php echo $m201_50['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_6']; ?></p>
                            <?php echo $m201_50['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_7']; ?></p>
                            <?php echo $m201_50['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_8']; ?></p>
                            <?php echo $m201_50['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m201_50['FER_9']; ?></p>
                            <?php echo $m201_50['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m501_100['CON_UA']; ?></p>
                            <?php echo $m501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PEC']; ?></p>
                            <?php echo $m501_100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_1']; ?></p>
                            <?php echo $m501_100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_2']; ?></p>
                            <?php echo $m501_100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_3']; ?></p>
                            <?php echo $m501_100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_4']; ?></p>
                            <?php echo $m501_100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_5']; ?></p>
                            <?php echo $m501_100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['PPP_6']; ?></p>
                            <?php echo $m501_100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_1']; ?></p>
                            <?php echo $m501_100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_2']; ?></p>
                            <?php echo $m501_100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_3']; ?></p>
                            <?php echo $m501_100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_4']; ?></p>
                            <?php echo $m501_100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_5']; ?></p>
                            <?php echo $m501_100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_6']; ?></p>
                            <?php echo $m501_100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_7']; ?></p>
                            <?php echo $m501_100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_8']; ?></p>
                            <?php echo $m501_100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m501_100['FER_9']; ?></p>
                            <?php echo $m501_100['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $m100['CON_UA']; ?></p>
                            <?php echo $m100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PEC']; ?></p>
                            <?php echo $m100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_1']; ?></p>
                            <?php echo $m100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_2']; ?></p>
                            <?php echo $m100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_3']; ?></p>
                            <?php echo $m100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_4']; ?></p>
                            <?php echo $m100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_5']; ?></p>
                            <?php echo $m100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['PPP_6']; ?></p>
                            <?php echo $m100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_1']; ?></p>
                            <?php echo $m100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_2']; ?></p>
                            <?php echo $m100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_3']; ?></p>
                            <?php echo $m100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_4']; ?></p>
                            <?php echo $m100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_5']; ?></p>
                            <?php echo $m100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_6']; ?></p>
                            <?php echo $m100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_7']; ?></p>
                            <?php echo $m100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_8']; ?></p>
                            <?php echo $m100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $m100['FER_9']; ?></p>
                            <?php echo $m100['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr style="background-color: #f5f9ff">
                          <td colspan="35" style="padding: 4px;"><label class="badge badge-danger">MUJERES</label></td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            Hasta 5.0 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f0_5['CON_UA']; ?></p>
                            <?php echo $f0_5['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PEC']; ?></p>
                            <?php echo $f0_5['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_1']; ?></p>
                            <?php echo $f0_5['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_2']; ?></p>
                            <?php echo $f0_5['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_3']; ?></p>
                            <?php echo $f0_5['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_4']; ?></p>
                            <?php echo $f0_5['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_5']; ?></p>
                            <?php echo $f0_5['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['PPP_6']; ?></p>
                            <?php echo $f0_5['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_1']; ?></p>
                            <?php echo $f0_5['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_2']; ?></p>
                            <?php echo $f0_5['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_3']; ?></p>
                            <?php echo $f0_5['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_4']; ?></p>
                            <?php echo $f0_5['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_5']; ?></p>
                            <?php echo $f0_5['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_6']; ?></p>
                            <?php echo $f0_5['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_7']; ?></p>
                            <?php echo $f0_5['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_8']; ?></p>
                            <?php echo $f0_5['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f0_5['FER_9']; ?></p>
                            <?php echo $f0_5['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 5.1 a 10 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f51_10['CON_UA']; ?></p>
                            <?php echo $f51_10['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PEC']; ?></p>
                            <?php echo $f51_10['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_1']; ?></p>
                            <?php echo $f51_10['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_2']; ?></p>
                            <?php echo $f51_10['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_3']; ?></p>
                            <?php echo $f51_10['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_4']; ?></p>
                            <?php echo $f51_10['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_5']; ?></p>
                            <?php echo $f51_10['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['PPP_6']; ?></p>
                            <?php echo $f51_10['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_1']; ?></p>
                            <?php echo $f51_10['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_2']; ?></p>
                            <?php echo $f51_10['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_3']; ?></p>
                            <?php echo $f51_10['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_4']; ?></p>
                            <?php echo $f51_10['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_5']; ?></p>
                            <?php echo $f51_10['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_6']; ?></p>
                            <?php echo $f51_10['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_7']; ?></p>
                            <?php echo $f51_10['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_8']; ?></p>
                            <?php echo $f51_10['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f51_10['FER_9']; ?></p>
                            <?php echo $f51_10['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 10.1 a 20 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f101_20['CON_UA']; ?></p>
                            <?php echo $f101_20['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PEC']; ?></p>
                            <?php echo $f101_20['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_1']; ?></p>
                            <?php echo $f101_20['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_2']; ?></p>
                            <?php echo $f101_20['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_3']; ?></p>
                            <?php echo $f101_20['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_4']; ?></p>
                            <?php echo $f101_20['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_5']; ?></p>
                            <?php echo $f101_20['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['PPP_6']; ?></p>
                            <?php echo $f101_20['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_1']; ?></p>
                            <?php echo $f101_20['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_2']; ?></p>
                            <?php echo $f101_20['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_3']; ?></p>
                            <?php echo $f101_20['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_4']; ?></p>
                            <?php echo $f101_20['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_5']; ?></p>
                            <?php echo $f101_20['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_6']; ?></p>
                            <?php echo $f101_20['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_7']; ?></p>
                            <?php echo $f101_20['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_8']; ?></p>
                            <?php echo $f101_20['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f101_20['FER_9']; ?></p>
                            <?php echo $f101_20['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 20.1 a 50 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f201_50['CON_UA']; ?></p>
                            <?php echo $f201_50['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PEC']; ?></p>
                            <?php echo $f201_50['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_1']; ?></p>
                            <?php echo $f201_50['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_2']; ?></p>
                            <?php echo $f201_50['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_3']; ?></p>
                            <?php echo $f201_50['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_4']; ?></p>
                            <?php echo $f201_50['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_5']; ?></p>
                            <?php echo $f201_50['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['PPP_6']; ?></p>
                            <?php echo $f201_50['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_1']; ?></p>
                            <?php echo $f201_50['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_2']; ?></p>
                            <?php echo $f201_50['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_3']; ?></p>
                            <?php echo $f201_50['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_4']; ?></p>
                            <?php echo $f201_50['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_5']; ?></p>
                            <?php echo $f201_50['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_6']; ?></p>
                            <?php echo $f201_50['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_7']; ?></p>
                            <?php echo $f201_50['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_8']; ?></p>
                            <?php echo $f201_50['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f201_50['FER_9']; ?></p>
                            <?php echo $f201_50['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 50.1 a 100 <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f501_100['CON_UA']; ?></p>
                            <?php echo $f501_100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PEC']; ?></p>
                            <?php echo $f501_100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_1']; ?></p>
                            <?php echo $f501_100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_2']; ?></p>
                            <?php echo $f501_100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_3']; ?></p>
                            <?php echo $f501_100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_4']; ?></p>
                            <?php echo $f501_100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_5']; ?></p>
                            <?php echo $f501_100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['PPP_6']; ?></p>
                            <?php echo $f501_100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_1']; ?></p>
                            <?php echo $f501_100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_2']; ?></p>
                            <?php echo $f501_100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_3']; ?></p>
                            <?php echo $f501_100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_4']; ?></p>
                            <?php echo $f501_100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_5']; ?></p>
                            <?php echo $f501_100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_6']; ?></p>
                            <?php echo $f501_100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_7']; ?></p>
                            <?php echo $f501_100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_8']; ?></p>
                            <?php echo $f501_100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f501_100['FER_9']; ?></p>
                            <?php echo $f501_100['FER_9_S']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td class="table-success">
                            De 100 a más <br>
                            Hectáreas
                          </td>
                          <td>
                            <p>Productores</p>
                            Superficie
                          </td>
                          <td>
                            <p><?php echo $f100['CON_UA']; ?></p>
                            <?php echo $f100['CON_UA_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PEC']; ?></p>
                            <?php echo $f100['PEC_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_1']; ?></p>
                            <?php echo $f100['PPP_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_2']; ?></p>
                            <?php echo $f100['PPP_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_3']; ?></p>
                            <?php echo $f100['PPP_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_4']; ?></p>
                            <?php echo $f100['PPP_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_5']; ?></p>
                            <?php echo $f100['PPP_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['PPP_6']; ?></p>
                            <?php echo $f100['PPP_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_1']; ?></p>
                            <?php echo $f100['FER_1_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_2']; ?></p>
                            <?php echo $f100['FER_2_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_3']; ?></p>
                            <?php echo $f100['FER_3_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_4']; ?></p>
                            <?php echo $f100['FER_4_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_5']; ?></p>
                            <?php echo $f100['FER_5_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_6']; ?></p>
                            <?php echo $f100['FER_6_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_7']; ?></p>
                            <?php echo $f100['FER_7_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_8']; ?></p>
                            <?php echo $f100['FER_8_S']; ?>
                          </td>
                          <td>
                            <p><?php echo $f100['FER_9']; ?></p>
                            <?php echo $f100['FER_9_S']; ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
       