<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-brand-wrapper d-flex justify-content-center">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
          <a class="navbar-brand brand-logo" href="<?php echo site_url('/');?>"><img src="<?php echo base_url(); ?>resources/images/minagri_logo.png" alt="logo Minagri"/></a>
          <a class="navbar-brand brand-logo-mini" href="<?php echo site_url('/');?>"><img src="<?php echo base_url(); ?>resources/images/logo_min.png" alt="logo"/></a>
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="typcn typcn-th-menu"></span>
          </button>
        </div>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav mr-lg-2">
          <li class="nav-item ml-0">
            <h4 class="mb-0">Dashboard</h4>
          </li>
          <li class="nav-item">
            <div class="d-flex align-items-baseline">
              <p class="mb-0">Inicio</p>
              <i class="typcn typcn-chevron-right"></i>
              <p class="mb-0"></p>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="typcn typcn-th-menu"></span>
        </button>
      </div>
    </nav>
    
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <!-- partial:../../partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="typcn typcn-document-text menu-icon"></i>
              <span class="menu-title">Reportes</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/1');?>">Uso de la Tierra</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/2');?>">Tenencia de la Tierra</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/3');?>">Asociatividad</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/4');?>">Crédito</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/5');?>">Capacidades</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/6');?>">Prácticas Pecuarias</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/7');?>">Riego</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('dashboard/reporte/8');?>">Hogar del Productor</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>