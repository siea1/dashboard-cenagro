<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



defined('JSON_R1_DEF') OR define('JSON_R1_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"NP_P1": "0",
		"NP_P1_S": "0",
		"NP_P2": "0",
		"NP_P2_S": "0",
		"NP_P3": "0",
		"NP_P3_S": "0",
		"NP_P4": "0",
		"NP_P4_S": "0",
		"NP_P5": "0",
		"NP_P5_S": "0",
		"NP_P610": "0",
		"NP_P610_S": "0",
		"NP_P10": "0",
		"NP_P10_S": "0",
		"CJ_NAT": "0",
		"CJ_NAT_S": "0",
		"CJ_SAC": "0",
		"CJ_SAC_S": "0",
		"CJ_SAA": "0",
		"CJ_SAA_S": "0",
		"CJ_SRL": "0",
		"CJ_SRL_S": "0",
		"CJ_EIRL": "0",
		"CJ_EIRL_S": "0",
		"CJ_COOPA": "0",
		"CJ_COOPA_S": "0",
		"CJ_COMC": "0",
		"CJ_COMC_S": "0",
		"CJ_COMN": "0",
		"CJ_COMN_S": "0",
		"CJ_OTRA": "0",
		"CJ_OTRA_S": "0",
		"GE_18": "0",
		"GE_18_S": "0",
		"GE_1930": "0",
		"GE_1930_S": "0",
		"GE_3145": "0",
		"GE_3145_S": "0",
		"GE_4564": "0",
		"GE_4564_S": "0",
		"GE_65": "0",
		"GE_65_S": "0",
		"SI_GRAV": "0",
		"SI_GRAV_S": "0",
		"SI_ASP": "0",
		"SI_ASP_S": "0",
		"SI_GOT": "0",
		"SI_GOT_S": "0",
		"SI_EXU": "0",
		"SI_EXU_S": "0",
		"SI_SECANO": "0",
		"SI_SECANO_S": "0",
		"DP_MNAC": "0",
		"DP_MNAC_S": "0",
		"DP_MEXT": "0",
		"DP_MEXT_S": "0",
		"DP_AGRO": "0",
		"DP_AGRO_S": "0",
		"DP_AUTOC": "0",
		"DP_AUTOC_S": "0",
		"DP_AUTOI": "0",
		"DP_AUTOI_S": "0",
		"DP_AANIM": "0",
		"DP_AANIM_S": "0"
	}'); 

defined('JSON_R2_DEF') OR define('JSON_R2_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"RT_CTR": "0",
		"RT_CTR_S": "0",
		"RT_CTNR": "0",
		"RT_CTNR_S": "0",
		"RT_STET": "0",
		"RT_STET_S": "0",
		"RT_STNET": "0",
		"RT_STNET_S": "0",
		"RT_COM": "0",
		"RT_COM_S": "0",
		"RT_ARR": "0",
		"RT_ARR_S": "0",
		"RT_POS": "0",
		"RT_POS_S": "0",
		"RT_OTR": "0",
		"RT_OTR_S": "0",
		"FA_HER": "0",
		"FA_HER_S": "0",
		"FA_CV": "0",
		"FA_CV_S": "0",
		"FA_ADJ": "0",
		"FA_ADJ_S": "0",
		"FA_OTRA": "0",
		"FA_OTRA_S": "0"
	}');

defined('JSON_R3_DEF') OR define('JSON_R3_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"PAC_SI": "0",
		"PAC_SI_S": "0",
		"PAC_NO": "0",
		"PAC_NO_S": "0",
		"TB_AIA": "0",
		"TB_AIA_S": "0",
		"TB_AML": "0",
		"TB_AML_S": "0",
		"TB_AME": "0",
		"TB_AME_S": "0",
		"TB_OAT": "0",
		"TB_OAT_S": "0",
		"TB_ASF": "0",
		"TB_ASF_S": "0",
		"TB_OTR": "0",
		"TB_OTR_S": "0",
		"TB_NO": "0",
		"TB_NO_S": "0"
	}');

defined('JSON_R4_DEF') OR define('JSON_R4_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"GES_SOC": "0",
		"GES_SOC_S": "0",
		"GES_SN1": "0",
		"GES_SN1_S": "0",
		"GES_SN2": "0",
		"GES_SN2_S": "0",
		"GES_SN3": "0",
		"GES_SN3_S": "0",
		"GES_SN4": "0",
		"GES_SN4_S": "0",
		"GES_NG1": "0",
		"GES_NG1_S": "0",
		"GES_NG2": "0",
		"GES_NG2_S": "0",
		"GES_NG3": "0",
		"GES_NG3_S": "0",
		"GES_NG4": "0",
		"GES_NG4_S": "0",
		"GES_NG5": "0",
		"GES_NG5_S": "0",
		"GES_NG6": "0",
		"GES_NG6_S": "0",
		"GES_NG7": "0",
		"GES_NG7_S": "0",
		"GES_NG8": "0",
		"GES_NG8_S": "0",
		"INS_1": "0",
		"INS_1_S": "0",
		"INS_2": "0",
		"INS_2_S": "0",
		"INS_3": "0",
		"INS_3_S": "0",
		"INS_4": "0",
		"INS_4_S": "0",
		"INS_5": "0",
		"INS_5_S": "0",
		"INS_6": "0",
		"INS_6_S": "0",
		"INS_7": "0",
		"INS_7_S": "0",
		"INS_8": "0",
		"INS_8_S": "0",
		"INS_9": "0",
		"INS_9_S": "0",
		"INS_10": "0",
		"INS_10_S": "0",
		"INS_11": "0",
		"INS_11_S": "0",
		"INS_12": "0",
		"INS_12_S": "0",
		"INS_13": "0",
		"INS_13_S": "0",
		"PRES_1": "0",
		"PRES_1_S": "0",
		"PRES_2": "0",
		"PRES_2_S": "0",
		"PRES_3": "0",
		"PRES_3_S": "0",
		"PRES_4": "0",
		"PRES_4_S": "0",
		"PRES_5": "0",
		"PRES_5_S": "0"
	}');

defined('JSON_R5_DEF') OR define('JSON_R5_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"RES_1": "0",
		"RES_1_S": "0",
		"RES_2": "0",
		"RES_2_S": "0",
		"RES_3": "0",
		"RES_3_S": "0",
		"RES_4": "0",
		"RES_4_S": "0",
		"TRS_1": "0",
		"TRS_1_S": "0",
		"TRS_2": "0",
		"TRS_2_S": "0",
		"TRS_3": "0",
		"TRS_3_S": "0",
		"TRS_4": "0",
		"TRS_4_S": "0",
		"TRS_5": "0",
		"TRS_5_S": "0",
		"MI_1": "0",
		"MI_1_S": "0",
		"MI_2": "0",
		"MI_2_S": "0",
		"MI_3": "0",
		"MI_3_S": "0",
		"MI_4": "0",
		"MI_4_S": "0",
		"MI_5": "0",
		"MI_5_S": "0",
		"MI_6": "0",
		"MI_6_S": "0",
		"MI_7": "0",
		"MI_7_S": "0"
	}');

defined('JSON_R6_DEF') OR define('JSON_R6_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"PEC": "0",
		"PEC_S": "0",
		"PPP_1": "0",
		"PPP_1_S": "0",
		"PPP_2": "0",
		"PPP_2_S": "0",
		"PPP_3": "0",
		"PPP_3_S": "0",
		"PPP_4": "0",
		"PPP_4_S": "0",
		"PPP_5": "0",
		"PPP_5_S": "0",
		"PPP_6": "0",
		"PPP_6_S": "0",
		"FER_1": "0",
		"FER_1_S": "0",
		"FER_2": "0",
		"FER_2_S": "0",
		"FER_3": "0",
		"FER_3_S": "0",
		"FER_4": "0",
		"FER_4_S": "0",
		"FER_5": "0",
		"FER_5_S": "0",
		"FER_6": "0",
		"FER_6_S": "0",
		"FER_7": "0",
		"FER_7_S": "0",
		"FER_8": "0",
		"FER_8_S": "0",
		"FER_9": "0",
		"FER_9_S": "0"
	}');

defined('JSON_R7_DEF') OR define('JSON_R7_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"RPA_1": "0",
		"RPA_1_S": "0",
		"RPA_2": "0",
		"RPA_2_S": "0",
		"RPA_3": "0",
		"RPA_3_S": "0",
		"RPA_4": "0",
		"RPA_4_S": "0",
		"RPA_5": "0",
		"RPA_5_S": "0",
		"RPA_6": "0",
		"RPA_6_S": "0",
		"RPA_7": "0",
		"RPA_7_S": "0",
		"NO_RIEGO": "0",
		"NO_RIEGO_S": "0",
		"CAR_1": "0",
		"CAR_1_S": "0",
		"CAR_2": "0",
		"CAR_2_S": "0",
		"CAR_3": "0",
		"CAR_3_S": "0",
		"CAR_4": "0",
		"CAR_4_S": "0",
		"DUA_1": "0",
		"DUA_1_S": "0",
		"DUA_2": "0",
		"DUA_2_S": "0",
		"DUA_3": "0",
		"DUA_3_S": "0",
		"DUA_4": "0",
		"DUA_4_S": "0",
		"DUA_5": "0",
		"DUA_5_S": "0",
		"PAC_1": "0",
		"PAC_1_S": "0",
		"PAC_2": "0",
		"PAC_2_S": "0",
		"PAC_3": "0",
		"PAC_3_S": "0"
	}');

defined('JSON_R8_DEF') OR define('JSON_R8_DEF', '{
		"SEXO": "-",
		"HECTAREA": "-",
		"SIN_UA": "0",
		"CON_UA": "0",
		"CON_UA_S": "0",
		"TH_0": "0",
		"TH_0_S": "0",
		"TH_1": "0",
		"TH_1_S": "0",
		"TH_2": "0",
		"TH_2_S": "0",
		"TH_3": "0",
		"TH_3_S": "0",
		"TH_4": "0",
		"TH_4_S": "0",
		"TH_5": "0",
		"TH_5_S": "0",
		"TH_6": "0",
		"TH_6_S": "0"
	}');