var urlBase = 'http://localhost:8060/midagri-censo/';

var data = {
    labels: ["Hasta 5.0", "De 5.1 a 10", "De 10.1 a 20", "De 20.1 a 50", "De 50.1 a 100", "De 100.1 a más"],
    datasets: [{
      label: 'Total',
      data: [10, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1,
      fill: false
    }]
  };

var options = {
    scales: {
      xAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };

var backgroundColor = [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',
        'rgba(255, 159, 64, 0.5)'
      ];
var borderColor = [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ];

var doughnutPieData = {
    datasets: [{
      data: [],
      backgroundColor: backgroundColor,
      borderColor: borderColor,
    }],
    labels: []
  };

var doughnutPieOptions = {
  responsive: true,
  animation: {
    animateScale: true,
    animateRotate: true
  }
};

$("#SelDepartamento").change(function() {
  let valDep = $("#SelDepartamento").val();
  $.ajax({
    type: "POST",
    url: `${urlBase}dashboard/provincias/${valDep}`,
    data: $(this).serialize(),
    success: function(res)
    {
      var jsonData = JSON.parse(res);
      var options = [];
      $('#SelProvincia').html('');
      $('#SelDistrito').html('');
      options.push('<option value="">Seleccione Provincia</option>');
      $.each(jsonData, function(i, item) {
        options.push($('<option/>',
        {
          value: item.COD_PROV, text: capitalizeFirstLetter(item.TXT_PROVINCIA)
        }));
      });
      $('#SelProvincia').append(options);   
      $('#SelDistrito').append('<option value="">Seleccione Distrito</option>');   

    }
  });
});

$("#SelProvincia").change(function() {
  let valDep = $("#SelDepartamento").val();
  let valProv = $("#SelProvincia").val();
  $.ajax({
    type: "POST",
    url: `${urlBase}dashboard/distritos/${valDep}/${valProv}`,
    data: $(this).serialize(),
    success: function(res)
    {
      var jsonData = JSON.parse(res);
      var options = [];
      $('#SelDistrito').html('');
      options.push('<option value="">Seleccione Distrito</option>');
      $.each(jsonData, function(i, item) {
        options.push($('<option/>',
        {
          value: item.COD_UBIGEO, text: capitalizeFirstLetter(item.TXT_DISTRITO)
        }));
      });
      $('#SelDistrito').append(options);   

    }
  });
});

$('#formFilter').submit(function() {
    let dist = $('#SelDistrito').val();
    if (dist) {
      return true;
    } else {
      Swal.fire({
        icon: 'warning',
        text: 'Seleccione departamento, provincia y distrito',
        confirmButtonColor: '#c82333'
      });
      return false;
    }
});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function exportReportToExcel(name) {
  $("#tableId").table2excel({
    filename: name,
    fileext: ".xls",
    preserveColors: true 
  });
}

function viewGraphics() {
  $("#tableIdDiv").hide();
  $("#graphicsIdDiv").show();
  $("#btnTable").show();
  $("#btnExcel").hide();
  $("#btnGraphics").hide();

  let d = JSON.parse(localStorage.data);

  if ($("#barChart").length) {
    data.datasets[0].data = [
    ((d.f0_5)?Number(d.f0_5.CON_UA):0) + ((d.m0_5)?Number(d.m0_5.CON_UA):0),
    ((d.f51_10)?Number(d.f51_10.CON_UA):0) + ((d.m51_10)?Number(d.m51_10.CON_UA):0),
    ((d.f101_20)?Number(d.f101_20.CON_UA):0) + ((d.m101_20)?Number(d.m101_20.CON_UA):0),
    ((d.f201_50)?Number(d.f201_50.CON_UA):0) + ((d.m201_50)?Number(d.m201_50.CON_UA):0),
    ((d.f501_100)?Number(d.f501_100.CON_UA):0) + ((d.m501_100)?Number(d.m501_100.CON_UA):0),
    ((d.f100)?Number(d.f100.CON_UA):0) + ((d.m100)?Number(d.m100.CON_UA):0)
    ];

    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'horizontalBar',
      data: data,
      options: options
    });
  }

  if ($("#pieChartR1").length) {
    doughnutPieData.datasets[0].data = [
    getDataReport(d, 'DP_MNAC'),
    getDataReport(d, 'DP_MEXT'),
    getDataReport(d, 'DP_AGRO'),
    getDataReport(d, 'DP_AUTOC'),
    getDataReport(d, 'DP_AUTOI'),
    getDataReport(d, 'DP_AANIM')];

    doughnutPieData.labels = ['Merc. Nacional', 'Merc. Exterior', 'Agroindustria', 'Autoconsumo', 'Autoinsumo', 'Alim. Animales'];

    var pieChartCanvas = $("#pieChartR1").get(0).getContext("2d");
    var pieChartR1 = new Chart(pieChartCanvas, {
      type: 'pie',
      data: doughnutPieData,
      options: doughnutPieOptions
    });
  }

  if ($("#doughnutChartR1").length) {
    let doughnutPieData2 = {
      datasets: [{
        data: [
          getDataReport(d, 'SI_GRAV'),
          getDataReport(d, 'SI_ASP'),
          getDataReport(d, 'SI_GOT'),
          getDataReport(d, 'SI_EXU'),
          getDataReport(d, 'SI_SECANO')],
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      }],
      labels: ['Riego>Gravedad', 'Riego>Aspersión', 'Riego>Goteo', 'Riego>Exudación', 'Bajo Secano']
    };

    var doughnutChartCanvas = $("#doughnutChartR1").get(0).getContext("2d");
    var doughnutChartR1 = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: doughnutPieData2,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChartR2").length) {
    let doughnutPieDataR2 = {
      datasets: [{
        data: [
          (getDataReport(d, 'RT_CTR') + getDataReport(d, 'RT_CTNR') + getDataReport(d, 'RT_STET') + getDataReport(d, 'RT_STNET')),
          getDataReport(d, 'RT_COM'),
          getDataReport(d, 'RT_ARR'),
          getDataReport(d, 'RT_POS'),
          getDataReport(d, 'RT_OTR')],
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      }],
      labels: ['En propiedad', 'Comunero', 'Arrendatario', 'Posesionario', 'Otro']
    };

    var pieChartCanvas = $("#pieChartR2").get(0).getContext("2d");
    var pieChartR2 = new Chart(pieChartCanvas, {
      type: 'pie',
      data: doughnutPieDataR2,
      options: doughnutPieOptions
    });
  }

  if ($("#doughnutChartR2").length) {
    let doughnutPieDataR21 = {
      datasets: [{
        data: [
          getDataReport(d, 'FA_HER'),
          getDataReport(d, 'FA_CV'),
          getDataReport(d, 'FA_ADJ'),
          getDataReport(d, 'FA_OTRA')],
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      }],
      labels: ['Herencia/Sucesión', 'Compra-Venta', 'Adjudicación', 'Otra']
    };

    var doughnutChartCanvas = $("#doughnutChartR2").get(0).getContext("2d");
    var doughnutChartR1 = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: doughnutPieDataR21,
      options: doughnutPieOptions
    });
  }

  if ($("#pieChartR3").length) {
    let doughnutPieDataR3 = {
      datasets: [{
        data: [
          getDataReport(d, 'PAC_SI'),
          getDataReport(d, 'PAC_NO')],
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      }],
      labels: ['SI', 'NO']
    };

    var pieChartCanvas = $("#pieChartR3").get(0).getContext("2d");
    var pieChartR3 = new Chart(pieChartCanvas, {
      type: 'pie',
      data: doughnutPieDataR3,
      options: doughnutPieOptions
    });
  }

  if ($("#doughnutChartR3").length) {
    let doughnutPieDataR31 = {
      datasets: [{
        data: [
          getDataReport(d, 'TB_AIA'),
          getDataReport(d, 'TB_AML'),
          getDataReport(d, 'TB_AME'),
          getDataReport(d, 'TB_OAT'),
          getDataReport(d, 'TB_ASF'),
          getDataReport(d, 'TB_OTR')],
        backgroundColor: backgroundColor,
        borderColor: borderColor,
      }],
      labels: ['Abs. Ins. Agri./Pecua.', 'Acc. Merc. Loc.', 'Acc. Merc. Ext.', 'Obt. Asist. Tec./Cap.', 'Acc. Serv. Finan.', 'Otros']
    };

    var doughnutChartCanvas = $("#doughnutChartR3").get(0).getContext("2d");
    var doughnutChartR3 = new Chart(doughnutChartCanvas, {
      type: 'doughnut',
      data: doughnutPieDataR31,
      options: doughnutPieOptions
    });
  }
}

function viewTable() {
  $("#tableIdDiv").show();
  $("#graphicsIdDiv").hide();
  $("#btnTable").hide();
  $("#btnExcel").show();
  $("#btnGraphics").show();
}

function getDataReport(d, col) {
  let data = ((d.f0_5)?Number(d.f0_5[col]):0) + ((d.m0_5)?Number(d.m0_5[col]):0) + 
             ((d.f51_10)?Number(d.f51_10[col]):0) + ((d.m51_10)?Number(d.m51_10[col]):0) +
             ((d.f101_20)?Number(d.f101_20[col]):0) + ((d.m101_20)?Number(d.m101_20[col]):0) +
             ((d.f201_50)?Number(d.f201_50[col]):0) + ((d.m201_50)?Number(d.m201_50[col]):0) +
             ((d.f501_100)?Number(d.f501_100[col]):0) + ((d.m501_100)?Number(d.m501_100[col]):0) +
             ((d.f100)?Number(d.f100[col]):0) + ((d.m100)?Number(d.m100[col]):0);
  return data;
}