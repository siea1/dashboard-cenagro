$(function() {

  'use strict';
  var data = {
    labels: ["Hasta 5.0", "De 5.1 a 10", "De 10.1 a 20", "De 20.1 a 50", "De 50.1 a 100", "De 100.1 a más"],
    datasets: [{
      label: '# of Votes',
      data: [10, 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1,
      fill: false
    }]
  };
  var multiLineData = {
    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
    datasets: [{
        label: 'Dataset 1',
        data: [12, 19, 3, 5, 2, 3],
        borderColor: [
          '#587ce4'
        ],
        borderWidth: 2,
        fill: false
      },
      {
        label: 'Dataset 2',
        data: [5, 23, 7, 12, 42, 23],
        borderColor: [
          '#ede190'
        ],
        borderWidth: 2,
        fill: false
      },
      {
        label: 'Dataset 3',
        data: [15, 10, 21, 32, 12, 33],
        borderColor: [
          '#f44252'
        ],
        borderWidth: 2,
        fill: false
      }
    ]
  };
  var options = {
    scales: {
      xAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };

  $("#btnSbmtDb").click(function() {
    let valDist = $("#SelDistrito").val();
    console.log('valDist ', valDist);  
    if (!valDist) {
      alert('Seleccione un distrito');
    } else {
      $.ajax({
        type: "POST",
        url: `${urlBase}dashboard/diagrams/1/${valDist}`,
        data: $(this).serialize(),
        success: function(res)
        {
          console.log('data ', data);
          var d = JSON.parse(res);
          data.datasets[0].data = [d.f0_5.CON_UA,8,8,8,8,9];
          
          console.log('dataaaaaa ', d);  

          if ($("#barChart").length) {
            var barChartCanvas = $("#barChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var barChart = new Chart(barChartCanvas, {
              type: 'horizontalBar',
              data: data,
              options: options
            });
          }

        }
      });
    }
  });

  

  



});